#include <iostream>
#include <forward_list>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <random>
#include <tuple>



#define tuple std::tuple<uint16_t,float>

typedef std::forward_list<tuple> fList;

class for_compare {
public :
    bool operator()(const tuple &a, const tuple &b){
        return (fabs(std::get<0>(a)/std::get<1>(a) - std::get<0>(b)/std::get<1>(b)) < std::numeric_limits<double>::epsilon());
    }
};

std::random_device rand_dev;
std::mt19937 rand_engine(rand_dev());

float RandomWeight () {
std::uniform_real_distribution<float> unifW(50, 120);
return (round(unifW(rand_dev)*10)/10);}

uint16_t RandomHeight () {
std::uniform_real_distribution<float> unifH(100, 200);
return static_cast<int>(std::trunc(unifH(rand_dev)));}


bool operator<(const tuple &a, const tuple &b) {
    for_compare c;
    if (c(a,b)) return (std::get<0>(a) < std::get<0>(b));
    return (std::get<0>(a)/std::get<1>(a) < std::get<0>(b)/std::get<1>(b));
}

void sort(fList &A){
    A.sort([](const tuple &a, const tuple &b){ return a < b; });
}

tuple HW_generate(){
    return std::make_tuple(RandomHeight(),RandomWeight());
}

void generating(fList &a, const int &list_size){
    for (int i = 0; i < list_size; i++){
        a.push_front(std::make_tuple(0,0));}
        generate(a.begin(), a.end(), HW_generate);
}


int counted(const int &A, const int &B, fList &a){
return count_if(a.begin(),a.end(),[A,B](const tuple &a){
                                         double _sqrt = sqrt(std::get<0>(a)*std::get<1>(a));
                                         return (_sqrt >= A && _sqrt <= B);});
}

double sr_arifmethic(const fList &a, int asize){
    double sr_ar = 0;
    for(const tuple &b : a) {
        sr_ar += std::get<0>(b) + std::get<1>(b);
    }
        return (sr_ar / asize);
}

void change_elements (const int &A, const int &B, fList &a, int asize){
double sr_ar = sr_arifmethic(a,asize);
for_each(a.begin(),a.end(),[A,B,sr_ar](tuple &c){
                                         if (sqrt(std::get<0>(c)*std::get<1>(c)) >= A && sqrt(std::get<0>(c)*std::get<1>(c)) <= B){
                                            std::get<0>(c) += sr_ar;
                                            std::get<1>(c) += sr_ar;
             }});
}

bool operator==(const tuple &a, const tuple &b){
    return (std::get<0>(a) == std::get<0>(b) && std::get<1>(a) == std::get<1>(b));
}

void ununique_delete (fList &a){
a.unique([](const tuple &a, const tuple &b){ return a == b; });

}

int main()
{
    int sizes = 1000000;
    fList A;
    generating(A, sizes);
    sort(A);
    ununique_delete(A);
    int D = 130, F = 160;
    std::cout << "COUNTED = "<< counted(D,F,A) << "\n\n";
    change_elements(D,F,A, sizes);
    return 0;
}
