#include <tuple>
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <ctime>
#include <iterator>
#include <cstdlib>      // std::rand, std::srand
#include <iomanip>

using namespace std;

typedef std::vector<std::tuple<uint16_t, float>> VEC;

/*bool operator<(const elem &a,const elem &b){
    elem c;
    if (c(a,b)) return (get<0>(a.HW) < get<0>(b.HW));
    return (get<0>(a.HW)/get<1>(a.HW) < get<0>(b.HW)/get<1>(b.HW));
}*/

std::random_device rand_dev;
std::mt19937 rand_engine(rand_dev());

float RandomWeight () {
std::uniform_real_distribution<float> unifW(30, 50);
return (std::trunc(unifW(rand_dev)));}

int RandomHeight () {
std::uniform_real_distribution<float> unifH(50, 200);
return static_cast<int>(std::trunc(unifH(rand_dev))); }
/*
void generate(VEC &A){
       for (elem &tmp : A){
       tmp.HW = make_tuple(RandomHeight(),RandomWeight());
       }
}*/

void sort(VEC &A){
    std::sort(A.begin(),A.end());
}


void out (VEC &a)
{
    for (auto &m : a)
    {
        std::cout <<" Height :"<<get<0>(m)<<"\t Weight :"<<get<1>(m)<<std::endl;
    }
}
/*
void deleting (VEC &A){
//    elem b;
//    std::unique(A.begin(),A.end(),b(A));

}*/

std::tuple<uint16_t, float> HW_generate(){
    std::tuple<uint16_t, float> typeidd;
    typeidd = make_tuple(RandomHeight(),RandomWeight());
    return typeidd;
}

void generating(VEC &a){
   // for (auto &m : a)
    //{
        generate(a.begin(), a.end(), HW_generate());
    //}
}


int main()
{
    uint16_t size = 15;
    VEC A(size);

    generating(A);

   // generate(A);
    out(A);
    /*sort(A);
    std::cout << "\n";
    out(A);
    std::random_device rd;
    std::mt19937 g(rd());
    shuffle(A.begin(),A.end(),g);
    deleting(A);
    std::cout << "\n";
    out(A);
    int D = 70, F = 80;
    std::cout << " COUNTED = "<<count_if(A.begin(),A.end(),[D,F](elem c){
                                         return (sqrt(get<0>(c.HW)*get<1>(c.HW))> D && sqrt(get<0>(c.HW)*get<1>(c.HW)> F));});
                                         std::cout << "\n";
    double sr_ar = 0;
    for(elem &b : A) {
        sr_ar += get<0>(b.HW) + get<1>(b.HW);
    }
    sr_ar /= VEC_SIZE;
    for_each(A.begin(),A.end(),[D,F,sr_ar](elem &c){
                                         if (sqrt(get<0>(c.HW)*get<1>(c.HW))> D && sqrt(get<0>(c.HW)*get<1>(c.HW)> F)){
                                            get<0>(c.HW) += sr_ar;
                                            get<1>(c.HW) += sr_ar;
             }});
                                         out(A);
//    sort(A.begin(), A.end());
   // double m =  ;*/
    return 0;
}
