#ifndef VECTORS_H_INCLUDED
#define VECTORS_H_INCLUDED

#include <tuple>
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <ctime>

using height = uint16_t;
using weight = float;
using tuple = std::tuple<height, weight>;
using VEC = std::vector<tuple>;

// ���������� �������� ()
class functor {
public :
    bool operator()(const tuple &a, const tuple &b)
    {
        return (fabs(std::get<height>(a)/std::get<weight>(a) - std::get<height>(b)/std::get<weight>(b)) < std::numeric_limits<double>::epsilon());
    }
};

// ���������
std::random_device rand_dev;
const unsigned seed = unsigned(std::time(nullptr));
std::mt19937_64 rand_engine(seed);

height RandomWeight ()
{
std::uniform_int_distribution<height> unifW(50, 120);
return (std::round(unifW(rand_dev)*10)/10);
}

uint16_t RandomHeight ()
{
std::uniform_real_distribution<float> unifH(100, 200);
return static_cast<int>(unifH(rand_dev));
}

// �������� <
bool operator<(const tuple &a, const tuple &b)
{
    functor c;
    if (c(a,b)) return (std::get<height>(a) < std::get<height>(b));
    return (std::get<height>(a)/std::get<weight>(a) < std::get<height>(b)/std::get<weight>(b));
}



// ���������� ��������� ��������
void sort(VEC &A)
{
    std::sort(A.begin(),A.end(),[](const tuple &a, const tuple &b){ return a<b; });
}

// ��������� ����� ����
tuple HW_generate()
{
    return std::make_tuple(RandomHeight(),RandomWeight());;
}

// ��������� �������� vector<tuple>
void generating(VEC &a, const int vec_size)
{
    for (uint32_t i = 0; i < vec_size; i++) a.push_back(std::make_tuple(0,0));
    generate(a.begin(), a.end(), HW_generate);
    for (tuple &c : a) std::cout <<std::get<0>(c)<<" "<<std::get<1>(c)<<std::endl;
}


// ������� ��������������� ��������� [A;B]
int counted(const int &A, const int &B, VEC &a)
{
return count_if(a.begin(),a.end(),[A,B](const tuple &a){
                                         double _sqrt = sqrt(std::get<height>(a)*std::get<weight>(a));
                                         return (_sqrt >= A && _sqrt <= B);});
}

// ������� �������� ��������������� ������� 4
double sr_arifmethic(const VEC &a)
{
    double sr_ar = 0;
    for(const tuple &b : a) {
        sr_ar += std::get<height>(b) + std::get<weight>(b);
    }
    return (sr_ar / a.size());
}

// ��������� ������� �������������� � �������� �� ��������� [A;B]
void change_elements (const int &A, const int &B, VEC &a)
{
double sr_ar = sr_arifmethic(a);
for_each(a.begin(),a.end(),[A,B,sr_ar](tuple &c){
                                         if (sqrt(std::get<height>(c)*std::get<weight>(c)) >= A && sqrt(std::get<height>(c)*std::get<weight>(c)) <= B){
                                            std::get<height>(c) += sr_ar;
                                            std::get<weight>(c) += sr_ar;
             }});
}
// �������� ���������
bool operator==(const tuple &a, const tuple &b)
{
    return (std::get<height>(a) == std::get<height>(b) && std::get<weight>(a) == std::get<weight>(b));
}

// �������� �������������
void ununique_delete (VEC &a)
{
VEC::iterator last = unique(a.begin(),a.end(),[](const tuple &a, const tuple &b){ return a == b; });
a.erase(last,a.end());
}


#endif // VECTORS_H_INCLUDED
