#include <tuple>
#include <vector>
#include "single_include/catch2/catch.hpp"
#include "vectors.h"
#define get std::get

using tuple = std::tuple<height,weight>;
using VEC = std::vector<tuple>;


TEST_CASE( "Testing vector<tuple> Catch2")
{
    tuple A;
    tuple B;
    SECTION("CHECKING VALUES GENERATING"){
    A = HW_generate();
    VEC C;
    generating(C,10);
    // �������� �� ������������ ��������� �������� �� ��������� ��� [50,120], ���� [100,200]
    CHECK(get<0>(A) >= 50);
    CHECK(get<0>(A) >= 100);
    CHECK(get<1>(A) >= 50);
    CHECK_FALSE(get<1>(A) >= 250); // fail
    CHECK_FALSE(get<0>(A) < 0); // fail
    CHECK_FALSE(get<1>(A) < 0); // fail
    }
    SECTION("CHECKING FUNCTOR () "){
    A = std::make_tuple(120,70);
    B = std::make_tuple(170,80);
    // �������� �� ������������ ��������� �������� �� ��������� ��� [50,120], ���� [100,200]
    functor c;
    CHECK_FALSE(c(A,B)); // fail
    get<0>(A) = 170;
    CHECK_FALSE(c(A,B)); // fail
    get<1>(A) = 80;
    CHECK(c(A,B));
    A = B;
    CHECK(c(A,B));
    B = std::make_tuple(100,50);
    CHECK_FALSE(c(A,B)); // fail
    }
    SECTION("CHECKING PREDICATE <"){
    A = std::make_tuple(150,80);
    B = std::make_tuple(140,90);
    CHECK_FALSE(A < B); //fail          A! = B => (150/80) > (140/90)
    CHECK(B < A);
    tuple C = A;
    CHECK_FALSE(C < A); // fail A = C => 150 = 150
    CHECK_FALSE(C < C); // fail C = C => 150 = 150
    get<0>(B) = 200;
    CHECK(A < B);
    get<1>(A) = 50;
    CHECK_FALSE(A < B); // fail
    }
    SECTION("CHECKING STD::SORT"){
    A = std::make_tuple(140,70);
    B = std::make_tuple(150,60);
    functor d;
    // A != B => 140 / 70 < 150 / 60 = > VEC[0] = A, VEC[1] = B;
    VEC C{A,B};
    sort(C);
    CHECK(d(C[0],A));
    CHECK(d(C[1],B));
    CHECK_FALSE(d(C[0],B)); // fail
    CHECK_FALSE(d(C[1],A)); // fail
    tuple E(std::make_tuple(150,50)); // 150 / 50 = 3 (E > A & E > B)
    C.push_back(E);
    sort(C);
    CHECK(d(C[0],A));
    CHECK(d(C[1],B));
    CHECK(d(C[2],E));
    }
    SECTION("CHECKING COUNTING VALUES IN [A,B] INTERVAL"){
    A = std::make_tuple(140,70); // sqrt(<0> * <1>) = 98.99
    B = std::make_tuple(120,50); // sqrt(<0> * <1>) = 77.45
    tuple C = std::make_tuple(180,50); // sqrt(<0> * <1>) = 94.86
    VEC D{A,B,C};
    int Ai = 93;
    int Bi = 99;
    CHECK(counted(Ai,Bi,D) == 2);
    CHECK_FALSE(counted(Ai,Bi,D) == 3);
    Ai = 70;
    Bi = 80;
    CHECK(counted(Ai,Bi,D) == 1);
    Ai = 79;
    CHECK(counted(Ai,Bi,D) == 0);
    CHECK_FALSE(counted(Ai,Bi,D) == 3);
    Ai = 70;
    Bi = 99;
    CHECK(counted(Ai,Bi,D) == 3);
    }
    SECTION("CHECKING VECTOR AVERAGE"){
    A = std::make_tuple(140,70);
    B = std::make_tuple(120,50);
    VEC C{A,B};
    // ((140 + 70) + (120+50))/2 =
    CHECK(sr_arifmethic(C) == 190);
    CHECK_FALSE(sr_arifmethic(C) == 200);
    tuple D = std::make_tuple(120,58);
    C.push_back(D);
    // sr_ar = 186
    CHECK(sr_arifmethic(C) == 186);
    CHECK_FALSE(sr_arifmethic(C) == 187);
    }
    SECTION("CHECKING CHANGE ELEMENTS (add average to element in [A,B])"){
    A = std::make_tuple(140,70); // sqrt(<0> * <1>) = 98.99
    B = std::make_tuple(120,50); // sqrt(<0> * <1>) = 77.45
    tuple D = std::make_tuple(120,58); // sqrt(<0> * <1>) = 83.43
    // sr_arifmetic = 186
    int Ai = 80;
    int Bi = 90;
    // only for  D
    VEC C{A,B,D};
    change_elements(Ai,Bi,C);
    CHECK(C[0] == A);
    CHECK_FALSE(C[2] == D);
    CHECK(C[1] == B);
    }
    SECTION("CHECKING UNUNIQUE ELEMENTS DELETING"){
    A = std::make_tuple(100,60);
    B = std::make_tuple(100,60); // !
    tuple C = std::make_tuple(120,50);
    tuple D = std::make_tuple(130,50); // !
    tuple E = std::make_tuple(130,50);
    VEC F{A,B,C,D,E};
    ununique_delete(F);
    CHECK_FALSE(F.size() == 5);
    CHECK_FALSE(F.size() == 4);
    CHECK(F.size() == 3);
    CHECK_FALSE(F[1] == B);
    CHECK(F[1] == C);
    CHECK(F[3] == E);
    }
}
