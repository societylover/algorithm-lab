#include "List.h"

// ------------------------- ��������� �����-�������� -------------------------
// �������� ���������� �� ��������
bool List::iterator::operator==(const iterator& it) const noexcept
{
    return (current == it.current);
}

// �������� ���������� �� �����������
bool List::iterator::operator!=(const iterator& it) const noexcept
{
    return !(*this == it); // ���������� ����� ==
}

// ������� � ���������� �������� � ������
List::iterator& List::iterator::operator++()
{
    if (current) current = current->next;                                // ��������� � ���������� ��������
    else throw std::out_of_range("����� �� ������� ������!");   // ����� �������� ���������� � ������ �� �������
    return *this;
}

// ������� � ����������� �������� � ������
List::iterator& List::iterator::operator--()
{
    if (current) current = current->prev;                                // ��������� � ����������� ��������
    else throw std::out_of_range("����� �� ������� ������!");   // ����� �������� ���������� � ������ �� �������
    return *this;
}

// ������� � ���������� �������� � ������ �����������
List::iterator List::iterator::operator++(int)
{
    iterator it (current);
    ++(*this);
    return it;
}

// ������� � ����������� �������� � ������ �����������
List::iterator List::iterator::operator--(int)
{
    iterator it (current);
    --(*this);
    return it;
}

// ��� �������� �� ������ (������) ��� �� 1
List::iterator& List::iterator::advance(const int &i)
{
    if (i == 0) return *this; // ���� �� 0 ����� ����������
    int counter = 0; // ����� ��� �������

    while (current && counter != i) // ����� �� ����� ���� ����� �� nullptr, ���� ����� ����������� ���������� ���
        {
            if (i > 0) { counter++; current = current->next;}
            else {counter--; current = current->prev;}
        }
    if (!current) throw std::out_of_range("���������� ���������� ���������!");   // ����� �������� ���������� � ������ �� �������
    return *this;
}

// �������� ������ ��������� �������� ������� ���������
List::iterator& List::iterator::operator=(const iterator& other)
{
    if (*this != other) current = other.current;
    return *this;
}

// ������������� ���������
List::reference List::iterator::operator*()
{
    return current->element; // ��������� �������� ����
}

// �������������� ������� � ������ ��������
const List::PNode List::iterator::operator&() const noexcept
{
    return current;
}

 // const- �������������
List::const_reference List::iterator::operator*() const
{
    return current->element; // ��������� �������� ����
}


// ����� �������� ���������� ����� ����������
List::size_type List::iterator::distance(List::iterator second) const noexcept
{
    size_type result = 0;
    iterator first(current);
    if (first == second) return 0; // ���� �����

    while (first != second)  // ���� �� ������ �� ������� ���������
        {
            result++; // ����������� ���������� �����������
            first++; // ������� ��������
        }
    return result;
}

// ------------------------- �����-��������� �� ������ ����������� ������ -------------------------
// ��� ������ ��������
void List::iterator::swap_element(iterator it1) noexcept
{
    std::swap(current->element, (&it1)->element);
}



// ����������
List::~List()
{
    for (Node * tmp = head; tmp != tail; delete tmp, tmp = tmp->next); // ������� �������� �� ������
    head = tail = nullptr; // �������� ��������� �� ������ � ����� ������
    list_size = 0; // �������� ������
}

// ������������� �������
List::List(const std::initializer_list<value_type>& t) noexcept
{
    list_size = t.size(); // ������� ������
    // ��������� �������� �������
    PNode tmp = new Node(*t.begin());
    tmp->prev = tail;
    tmp->next = nullptr;
    head = tail = tmp;
    // ��������� ����������� ��������
    for (size_type i = 1; i < list_size; i++)
    {
            PNode addition = new Node(*(t.begin()+i));
            addition->next = nullptr;
            addition->prev = tail;
            tail->next = addition;
            tail = addition;
    }
}


// ����������� �� ���������
 List::List()
 {
     head = new Node(computer_template);
     head->next = head->prev = nullptr;
     tail = head;
 }

// ����������� �� ������ � ������ ������� - ����������� �����������
List::List(const List& other) noexcept
{

    if (other.list_size != 0) // ���� ������ ���������������� �� ������, �� ��������� ��������
    {
        list_size = other.list_size;
        List a = other;
        PNode tmp = new Node(*a.begin()); // � �������� ���� �� ��������� �� ����������
        // ��������� �����
        tmp->prev = tail;
        tmp->next = nullptr;
        head = tail = tmp;
        iterator it = ++a.begin();// ��� ��������� � ��������

        // ��������� ��������, ������� ���� ����� ���������
        for(size_type i = 1; i < list_size; i++, it++)
        {
            PNode addition = new Node(*it); // ��� ������ �������� ������ ������
            // �������� ����� ������� �� �������
            addition->next = nullptr;
            addition->prev = tail;
            tail->next = addition;
            tail = addition;
        }
    }
    else *this = {}; // ����� �������������� ������
}

// ����������� ����������
List::List(List&& other) noexcept
{
    // ������� �������� ��� ������ ���������
    head = other.head;
    tail = other.tail;
    // ������� ������ � ������� ��� � r-value
    list_size = other.list_size;
    other.list_size = 0; // ������� ������, �.�. ������ �������� ���������
    // ������� ��������� � other, ����� �� ���� �������
    other.head = other.tail = nullptr;
}

// ������������ ��� r-value ��������� ��������
List& List::operator=(List&& other) noexcept
{
    clear();
    // ������� ��������
    head = other.head;
    tail = other.tail;
    // ������ �������� � ����������
    list_size = other.list_size;
    other.head = other.tail = nullptr;
    other.list_size = 0;
    return *this;
}


List& List::operator=(const List& other)
{
    clear();
    // ��������� ��� ��������
    PNode other_head = other.head;
    while (other_head != other.tail->next){
            push_back(other_head->element);
            other_head = other_head->next;}

    return *this;
}

// �������� �� ������ ������
List::iterator List::begin() noexcept
{
    iterator it(head); // �������������� �������� ���������� �� �������� �������
    return it;
}

// �������� �� ����� ������
List::iterator List::end() noexcept
{
    iterator it(tail); // �������������� �������� ���������� �� ��������� �������
    return it;
}

// ����������� �������� �� ������ ������
const List::iterator List::cbegin() noexcept
{
    iterator it(head); // �������������� �������� ���������� �� �������� �������
    return it;
}

// ����������� �������� �� ����� ������
const List::iterator List::cend() noexcept
{
    iterator it(tail); // �������������� �������� ���������� �� ��������� �������
    return it;
}

// ���������� ������ �� ���������� � ������ ���������� � ������
List::reference List::front()
{
    if (list_size == 0) throw std::length_error("������ ����!"); // ���� ������ ������ - ������� ������
    return (head->element);
}

// ���������� ������ �� ���������� � ��������� ���������� � ������
List::reference List::back()
{
    if (list_size == 0) throw std::length_error("������ ����!"); // ���� ������ ������ - ������� ������
    return (tail->element);
}

// �������� �� ������� ����������� ������
bool List::empty() const noexcept
{
    return (list_size == 0); // ���� �������� ������� ��������� �� nullptr - ������ ��� ������ ����
}

// ������ ���������� ��������� � ���������� ������
List::size_type List::size() const noexcept
{
    return list_size; // ������� ���������� ���������
}

// ������� �������� � ������
void List::push_front(const_reference &other)
{
    // �������� ����� ����
    PNode addition = new Node(other);
    // ��������� ���������� ������� nullptr
    addition->prev = nullptr;
    if(list_size != 0) addition->next = head; // �������� ������� ���� �����
    else delete head; // ����� ���� � ��� ������ ��� ��������� - ������ �������, ���������� � ������������ �� ���������
    head->prev = addition; // �������� ������� ������ � �����������
    head = addition; // �������� ������� - �����������
    if (list_size == 0) tail = head; // ���� �� ����� ������ ��� ���� - ������ = ���������� ��������
    list_size++; // �������� ������
}

// ������� �������� � ������ ��� ���������� ��������
void List::push_front(value_type && other)
{
    // ���������� ��� ������ const_reference &other
    push_front(other);
    other = computer_template; // ������� ���������
}

// ������ ������� � ������
void List::pop_front()
{
    if (list_size == 0)
        throw std::length_error("������ ����!"); // ���� ������ ������ - ������� ������
    list_size--; // ������� ������
    if (list_size == 0) // ���� ����� ������ = 0, ��
    {
        // ��������� �������� ������� � ������ ���������, ����� ���������� �������� 0
        *head = computer_template;
        head->next = head->prev = nullptr;
        tail = head;
    }
    else // ����� ������ ������� �� ������
    {
        PNode to_delete = head; // ��������� �������
        head = head->next;
        head->prev = nullptr;
        // �������� ����� � ������� �������
        delete to_delete;
    }
}

// �������� ������� � �����
void List::push_back(const_reference &other)
{
    // �������� ����� ����
    PNode addition = new Node(other);
    // ��������� ����� ����
    addition->next = nullptr;
    addition->prev = tail;
    if (list_size == 0) {
            delete head; // ���� ��������� �� ���� - ������ �����������
            addition->prev = nullptr; // ��� ��������� �������� ����������� ���!
            head = addition;
                            } // �������� ������� - �����������
    // ������ ����� � ����� �������
    tail->next = addition;
    tail = addition;
    // ������ ��������� ������� ���� ���������
    list_size++; // �������� ������
}

// �������� ������� � ����� (��� ����������)
void List::push_back(value_type && other)
{
    // ���������� ��� ������ const_reference &other
    push_back(other);
    other = {computer_template}; // ������� ���������
}

// ������� ������� � �����
void List::pop_back()
{
    if (list_size == 0) throw std::length_error("������ ����!"); // ���� ������ ������ - ������� ������
    list_size--; // ������� ������

    if (list_size == 0) // ���� ����� ������ = 0, ��
    {
        // ��������� �������� ������� � ������ ���������, ����� ���������� �������� 0
        *head = computer_template;
        head->next = head->prev = nullptr;
        tail = head;
    }
    else // ����� ������ ������� �� ������
    {
        PNode to_delete = tail; // ��������� �������
        tail = tail->prev;
        tail->next = nullptr;
        // �������� ����� � ������� �������
        delete to_delete;
    }
}

// ������� �������� ����� �������� ���������
List::iterator List::insert(iterator it, const_reference &other)
{
    // ���������� ������ �� ��� ��������� �������� � ���������� ����������� �������� � ����������� �� ��������� ���������
    PNode addition = new Node(other); // ����������� �������
    if (it == begin()) // ���� ��������� �� ������
    {
        // ������ � ��� �����
        addition->prev = nullptr;
        addition->next = head;
        head->prev = addition;
        head = addition;
    }
    else if (it == end()) // ���� ��������� �� ��������� �������, � �������� ����� ���
    {
        // ������ ������� � ���������� (� ��� ������ ����� ��������� �� "������")
        addition->prev = tail->prev;
        addition->next = tail;
        tail->prev->next = addition;
        tail->prev = addition;
    } else // ����� �� ��������� �� � "������" � �� � "�����", ������ ���-�� ����� ����
    {
        addition->next = &it;
        List::iterator tmp = it;
        addition->prev = &(--tmp);
        (&it)->prev->next = addition;
        (&it)->prev = addition;
    }
    if (list_size == 0) tail = head; // ���� �� ���� ���������, �� ����� ��������� �� ������
    list_size++; // �������� ������
    return iterator(addition);
}

// ������� �������� ����� �������� ��������� (��� ����������)
List::iterator List::insert(iterator it, value_type&& other)
{
    // ����������
    insert(it, other);
    // �������� ���������
    other = computer_template;
}

// �������� �������� �� ������� ��������� ��������
List::iterator List::erase(iterator it)
{
    PNode to_delete = &it;
    iterator next_pntr = begin();
    if (list_size == 0) throw std::length_error("������ ����!"); // ���� ������ ������ - ������� ������
    else if (it == begin()) // ���� �������� ������� �������� �������
    {
        // �������� �� ��, �� ������� �� �� ������������ ������� � ������
        if (list_size != 1) head = head->next; // ���� ��� -  ������ -  ��������� �������
        else { // ����� ������ - ����� ������� �� ��������� 0
            head = new Node(computer_template);
            head->next = nullptr;} // � ��������� ��������� - nullptr
        head->prev = nullptr;
        next_pntr = begin();
    } else if (it == end()) // ���� �������� ������� ��������� �������
    {
        tail = tail->prev;
        tail->next = nullptr;
        next_pntr = end();
    } else // ���� ����� �������� � ���������
    {
        // ������ �������� ����� � ������
        to_delete->prev->next = to_delete->next;
        to_delete->next->prev = to_delete->prev;
         next_pntr = iterator(to_delete->prev);
    }
    if (list_size == 1) tail = head;  // ���� ������ �������� � ���������� erase, �� ����� ����� ��������� �� ������
    delete to_delete;
    list_size--; // ������� ������
    return next_pntr;
}

// ����������� ����������
void List::clear()
{
    for (; list_size != 0; erase(begin())); // ������� �������� ����������
    list_size = 0; // ������� ������
}

// ����� �������� ����������
void List::swap(List& t) noexcept
{
    // �������� �������� ���������, ���������� ���������  � �������
    std::swap(head, t.head);
    std::swap(tail, t.tail);
    std::swap(list_size, t.list_size);
}

// ������� �������� � ������������� ����������, ������� ���� ������ !!""!"!"!
void List::unique() noexcept
{
        // ���� �� ��������� �� �������������� �������� � ������� ���������� ��������
        for (PNode tmp = head; tmp != tail->next; tmp = tmp->next) // ������� ����� ����, � �� ���������, ��� ���� ����� �� ��������� ������� ��������� � erase
        if (tmp->next && tmp->element == tmp->next->element)
        erase(iterator(tmp)); // �������������� ����� ��������, ����� �� ������ tmp = tmp->prev ����� �� ���������� �������
}

// ��������� ������������ ���������
void List::shuffle() noexcept
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count()); // chrono-seed
	static std::uniform_int_distribution<int> numbr(0, list_size-1); // ��� ��������� ���������� + ��� ���������
    for (iterator i = begin(); i != iterator(nullptr); i.swap_element(begin().advance(numbr(rng))), i++); // ������ ������� ��������
}
