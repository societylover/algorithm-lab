#ifndef LIST_H
#define LIST_H

#include <exception>
#include <algorithm>
#include <random> // ��� ��������� �����������
#include <chrono> // ��� ��������� �����������
#include "about.h"


// ������� ����������� ��� ����������
class functor
{
public:
    // >
    bool operator()(const computer &A, const ID &B_id)
    {
        return (std::atoi(std::get<0>(A).id) > std::atoi(B_id.id));
    }
};

class functor_ekz
{
public:
    // >
    bool operator()(const computer &A, const computer &B)
    {
        return (std::atoi(std::get<0>(A).id) > std::atoi(std::get<0>(B).id));
    }
};

// ������-������� ������
auto smaller = [](const computer &A, const ID &B_id)->bool{ return (std::atoi(std::get<0>(A).id) < std::atoi(B_id.id));};
auto smaller_ekz = [](const computer &A, const computer &B)->bool{ return (std::atoi(std::get<0>(A).id) < std::atoi(std::get<0>(B).id));};


class List
{
  // �������� ���������������
    public:
    using size_type = size_t;
    using value_type = computer;        		  //-- �������� ��� ��������� --
    using reference = value_type&;
    using const_reference = const value_type&;
  private:
    struct Node // ���� ������
    {
        value_type element; // �������� �������� ����
        Node *prev, *next;  // �������� �� ��������� � ���������� ��������
        Node(const value_type &A) noexcept : element(A) {};
    };
    typedef Node *PNode; // ��� ������������� ��� *
    PNode tail, head;    // ��������� �� ������ � ����� ������
    size_type list_size = 0;
  public:
     // -- �������� �����-�������� �
      class iterator
    {
        PNode current;
        public:
        // ������������ --
        explicit iterator(PNode t) noexcept : current(t) {};
        iterator() noexcept;
        // ���������� ���������
        iterator& operator=(const iterator& other);
        // ��������� ����������
        bool operator==(const iterator &it) const noexcept;
        bool operator!=(const iterator &it) const noexcept;
        // ����������� ���������
        iterator& operator++();              // ������ �������
        iterator& operator--();              // ����� �������
        iterator operator++(int);              // ������ ��������
        iterator operator--(int);              // ����� ��������
        iterator& advance(const int &i); // ��� ������������ ��������� �� ���������� ������� (�������)  ��� �� 1 . ���� ����� �������� ����� �� �������� � -, ����� �������������
        reference operator*();                // �������������
        const PNode operator&() const noexcept;     // �������������� ������� � ������ ��������
        const_reference operator*() const;                // const- �������������
        size_type distance(iterator second) const noexcept; // ������ ���������� ����� ���������� (������ ������� ������ ���� ������)
        void swap_element(iterator it1) noexcept;
    };  // -- ����� ��������� --

    /* ������������/����������/������������ */
    List();
    virtual ~List();
    List(const std::initializer_list<value_type> &t) noexcept;
    List(const List& other) noexcept;
    List(List&& other) noexcept;              // -- ����������� �������� --
    List& operator=(List&& other) noexcept;   // -- �������� ����������� --
    List& operator=(const List& other);
// ��������� ----------------
    iterator begin() noexcept;
    iterator end() noexcept;
    const iterator cbegin() noexcept;
    const iterator cend() noexcept;
// ������ � ��������� -------
    reference front();
    reference back();
// ������� ------------------
    bool empty () const noexcept;
    size_type size() const noexcept;
// ������������ ���������� --
    void push_front (const_reference &other);        // �������� � ������
    void push_front (value_type && other);          // �������� � ������ - ��������� ������ --
    void pop_front ();                        // ������� ������
    void push_back (const_reference &other);         // �������� � �����
    void push_back (value_type && other);           // �������� � ������ - ��������� ������ --
    void pop_back ();                         // ������� ���������
    iterator insert (iterator it, const_reference &other);  // �������� � ������� ���������
    iterator insert (iterator it, value_type&& other);      // �������� ��������� ������ --
    iterator erase (iterator it);                           // ������� ��������� (� �������)
    void clear ();                                // ������� ���
    void swap (List &t) noexcept;        // �������� � �������� �������
    void unique() noexcept;              // ������� ���������
    void shuffle() noexcept;           // �������� ����������� ��������
};


#endif // LIST_H
