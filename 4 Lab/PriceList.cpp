#include "PriceList.h"


// -------------------- ����� PRICE_LIST
// ������������

// ��������� ���������
PriceList::PriceList(PriceList&& other) noexcept
{
    // �������� ��������
    create_date = other.create_date;
    firm_name = other.firm_name;
    pricelist_List = other.pricelist_List;
    // ������� �������� ����������
    other.pricelist_List.clear();
    other.create_date = date_template;
    other.firm_name = "";
}

// �������� ������������ ��� ���������� ��������
PriceList& PriceList::operator=(PriceList&& other) noexcept   // -- �������� ����������� --
{
    // �������������� �������
    *this = {other};
    // ������ ������ � ������
    other.firm_name = "";
    other.create_date = date_template;
    other.pricelist_List.clear();
    return *this;
}


// �������� ������������� ��� ������� �����-�����
PriceList& PriceList::operator=(const PriceList& other)
{
    create_date = other.create_date;
    firm_name = other.firm_name;
    if (other.pricelist_List.size() != 0) pricelist_List = other.pricelist_List;
    return *this;
}

// ����������
PriceList::~PriceList()
{
    // ������� ������
    pricelist_List.clear();
    // ������� ����
    firm_name = "";
    create_date = date_template;
}

// ��� ��������� ID ��� ���� ������
inline int getID(const List::value_type& tmp)
{
    return std::atoi(std::get<id>(tmp).id);
}

// ��� ��������� MeM ��� ���� ������
inline int getMem(const List::value_type& tmp)
{
    return std::get<mem>(tmp) ;
}

// ��� ��������� hrd ��� ���� ������
inline int getHrd(const List::value_type& tmp)
{
    return std::get<hrd>(tmp) ;
}

// ��� ��������� vid ��� ���� ������
inline int getVid(const List::value_type& tmp)
{
    return std::get<vid>(tmp) ;
}


// ����������� ������� ��� ���������� �� ID � �����������-�����������
void sortByID(List::iterator begin, List::iterator toBegin, List::iterator toEnd) // - ������� ����������
{
    List::value_type pivot = *toBegin; // ����������� �������
    int id_int = getID(pivot);
    List::iterator l_hold = toBegin, r_hold = toEnd;
    while (begin.distance(toBegin) < begin.distance(toEnd)) // ���� ������� �� ���������
    {
        while (getID(*toEnd) >= id_int && (begin.distance(toBegin) < begin.distance(toEnd)))
            toEnd--; // �������� ������ ������� ���� ������� [right] ������ [pivot]
        if (toBegin != toEnd) // ���� ������� �� ����������
        {
            *toBegin = *toEnd;
            toBegin++; // �������� ����� ������� ������
        }
        while (getID(*toBegin) <= id_int && (begin.distance(toBegin) < begin.distance(toEnd)))
            toBegin++; // �������� ����� ������� ���� ������� [left] ������ [pivot]
        if (toBegin != toEnd) // ���� ������� �� ����������
        {
            *toEnd = *toBegin;
            toEnd--; // �������� ������ ������� ������
        }
    }
    *toBegin = pivot;
    List::iterator tmp = toBegin;
    toBegin = l_hold;
    toEnd   = r_hold;
    if (getID(*toBegin) < getID(*tmp)) // ���������� �������� ���������� ��� ����� � ������ ����� �������
        sortByID(begin, toBegin, --tmp);
    if (getID(*toEnd) > getID(*tmp))
        sortByID(begin, ++tmp, toEnd);
}

// ��������� ����������� ���������

void PriceList::shuffle() noexcept
{
    pricelist_List.shuffle();
}

// ������ ���������� ��� ������� � begin end
void PriceList::startIDsorting() noexcept
{
    sortByID(pricelist_List.begin(), pricelist_List.begin(), pricelist_List.end());

}

// ��������� �������� �� ��� ��������� � ������
List::value_type& PriceList::getElement(const List::size_type& it_counter) noexcept
{
    return *(pricelist_List.begin().advance(it_counter));
}

// �������� ����� � ������
List::value_type& PriceList::binarySearch(const ID& searched) noexcept
{
    List::iterator left = pricelist_List.begin(), right = pricelist_List.end();
    int t_fnd = std::atoi(searched.id);

    while (getID(*left) <= getID(*right) && getID(*right) >= t_fnd)
        {
            List::iterator mid = pricelist_List.begin().advance((pricelist_List.begin().distance(left) + pricelist_List.begin().distance(right))/2); // �������� ���������� �������
            // ����������� ������� ��������� ������
            if (getID(*mid) == t_fnd) return *mid;
            if (getID(*mid) < t_fnd) left = ++mid;
            if (getID(*mid) > t_fnd) right = --mid;
        }
    // �������������� �������
    return *pricelist_List.end(); // ������ ������, � ������ ���� �� �����
}

// ����������� ��� ���������� ����������������� ������, ���������� ������� ����� ������� ������ � �� ������� ���������
// ����� ���������� ������� �� ������ ������ � �� ������� ���������
// ����� ���������� ��������� �� ������� ������������� ����� ������
List::size_type CountSteps(List::iterator start, List::iterator toBegin, List::iterator toEnd)
{
    List::size_type i = 0, j = 0;
    List::iterator it = start;
    while (it != toBegin)
    {
        i++;
        it++;
    }
    it = start;
    while (it != toEnd)
    {
        j++;
        it++;
    }
    return (j-i);
}

// ���������������� ����� �� 8 ��������� ������
List::value_type& PriceList::interpolarSearch(const ID& searched) noexcept
{
    List::iterator mid = pricelist_List.begin(), low = pricelist_List.begin(), high = pricelist_List.end();
    int t_fnd = std::atoi(searched.id);
    while (smaller(*low, searched) && functor()(*high, searched)) {
        mid = low.advance((t_fnd - getID(*low)) * CountSteps(pricelist_List.begin(), low, high) / (getID(*high) - getID(*low)));

        if (smaller(*mid, searched))
            low = ++mid;
        else if (functor()(*mid, searched))
            high = --mid;
        else
            return *mid;
    }
    if (getID(*low) == t_fnd)
        return *low;
    if (getID(*high) == t_fnd)
        return *high;

    return *pricelist_List.end(); // Not found
}

// ����� ������ ������-���������� ��� �������� ������������� ���������
void PriceList::unique() noexcept
{
    pricelist_List.unique();
}

// ������ ���������� ���������
List::size_type PriceList::size() const noexcept
{
    return pricelist_List.size();
}

// ��������� ���� ������ �� ��� �� id ��� ������ ��� ������
PriceList PriceList::splitUpID(const BiggerLower &val, const ID &etalon)
{
    PriceList tmp("��������� ������ �� ID", create_date); // ������� ����� ������
    int t_fnd = std::stoi(etalon.id);
    if (val == bigger) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++(pricelist_List.end()); i++)
        {
            if (getID(*i) > t_fnd) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i); }
        }
    if (val == lower) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++pricelist_List.end(); )
        {
            if (getID(*i) < t_fnd) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i); }
            else i++;
        }

    return tmp;
}

// ��������� ���� ������ �� ��� �� mem ��� ������ ��� ������
PriceList PriceList::splitUpMEM(const BiggerLower &val, const CAPACITY_M &etalon)
{
    PriceList tmp("��������� ������ �� ������", create_date); // ������� ����� ������
    if (val == bigger) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++(pricelist_List.end()); i++)
        {
            if (std::get<mem>(*i) > etalon) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i);}
        }
    if (val == lower) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++(pricelist_List.end()); i++)
        {
            if (std::get<mem>(*i) < etalon) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i);}
        }
    return tmp;
}

// ��������� ���� ������ �� ��� �� hrd ��� ������ ��� ������
PriceList PriceList::splitUpHRD(const BiggerLower &val, const CAPACITY_H &etalon)
{
    PriceList tmp("��������� ������ �� �������� �����", create_date); // ������� ����� ������
    if (val == bigger) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++(pricelist_List.end()); i++)
        {
            if (std::get<hrd>(*i) > etalon) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i);}
        }
    if (val == lower) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++(pricelist_List.end()); i++)
        {
            if (std::get<hrd>(*i) < etalon) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i);}
        }
    return tmp;

}

// ��������� ���� ������ �� ��� �� mem ��� ������ ��� ������
PriceList PriceList::splitUpVID(const BiggerLower &val, const CAPACITY_C &etalon)
{
    PriceList tmp("��������� ������ �� �����������", create_date); // ������� ����� ������
    if (val == bigger) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++(pricelist_List.end()); i++)
        {
            if (std::get<vid>(*i) > etalon) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i);}
        }
    if (val == lower) // ���� ����� ������
    for (List::iterator i = pricelist_List.begin(); i != ++(pricelist_List.end()); i++)
        {
            if (std::get<vid>(*i) < etalon) { tmp.pricelist_List.push_back(*i); pricelist_List.erase(i);}
        }
    return tmp;

}

// ������� ��� ��������� ������
PriceList PriceList::splitUp(const WhatToTake &key, const BiggerLower &val, const List::value_type &etalon)
{
    if (key == id)  return splitUpID(val,  std::get<id>(etalon));
    if (key == mem) return splitUpMEM(val, std::get<mem>(etalon));
    if (key == hrd) return splitUpHRD(val, std::get<hrd>(etalon));
    return splitUpVID(val, std::get<vid>(etalon));
}

// ������� ���� ��������������� �������
// � ������ � this ����� ������������ �������� �� other ������
// � ���������� other ��������
void PriceList::mergeID(PriceList& other)
{
    if (this == &other) throw std::logic_error("������ ���������� ������ � ����� �����!");
    List::iterator it_main = pricelist_List.begin(), it_other = other.pricelist_List.begin();
    while (it_other != ++other.pricelist_List.end()) // ���� �� nullptr
    {
        // 1 �������� ������� - � �������� ������ ������� ��� ���������� �������� > ��� �����, �� ��������� ������� �� ������� ������ ����� ���
        if (getID(*it_main) >= getID(*it_other))
        {
            pricelist_List.insert(it_main,*it_other);
            it_main--;
            it_other++;

        } else // ����� ��������� ��� ������� � ������� ������ ������
        {
            if (it_main != pricelist_List.end())  it_main++; // �����, 2 �������� - ����������� ������� � ������� ������, ���� �� ����� �� ���������� ��������
            else { // 3 �������� - ������� � ������� ������ ������ � �� ����� �� ���������� ��������, ��������� ������� � �����, ��������� � ����. ������������ ��������, ����������� ��������� �� ����� �����, ��� ���������
                pricelist_List.push_back(*it_other);
                it_other++;
                it_main++;
            }
        }
    }
    other.pricelist_List.clear(); // ������� ������, ������� �������� � ��������
}

// ������� ���� ������� �� MEM
void PriceList::mergeMEM(PriceList& other)
{
    if (this == &other) throw std::logic_error("������ ���������� ������ � ����� �����!");
    List::iterator it_main = pricelist_List.begin(), it_other = other.pricelist_List.begin();
    while (it_other != ++other.pricelist_List.end()) // ���� �� nullptr
    {
        // 1 �������� ������� - � �������� ������ ������� ��� ���������� �������� > ��� �����, �� ��������� ������� �� ������� ������ ����� ���
        if (getMem(*it_main) >= getMem(*it_other))
        {
            pricelist_List.insert(it_main,*it_other);
            it_main--;
            it_other++;

        } else // ����� ��������� ��� ������� � ������� ������ ������
        {
            if (it_main != pricelist_List.end())  it_main++; // �����, 2 �������� - ����������� ������� � ������� ������, ���� �� ����� �� ���������� ��������
            else { // 3 �������� - ������� � ������� ������ ������ � �� ����� �� ���������� ��������, ��������� ������� � �����, ��������� � ����. ������������ ��������, ����������� ��������� �� ����� �����, ��� ���������
                pricelist_List.push_back(*it_other);
                it_other++;
                it_main++;
            }
        }
    }
    other.pricelist_List.clear(); // ������� ������, ������� �������� � ��������

}

// ������� ���� ������� �� ������ �������� �����
void PriceList::mergeHRD(PriceList& other)
{
    if (this == &other) throw std::logic_error("������ ���������� ������ � ����� �����!");
    List::iterator it_main = pricelist_List.begin(), it_other = other.pricelist_List.begin();
    while (it_other != ++other.pricelist_List.end()) // ���� �� nullptr
    {
        // 1 �������� ������� - � �������� ������ ������� ��� ���������� �������� > ��� �����, �� ��������� ������� �� ������� ������ ����� ���
        if (getHrd(*it_main) >= getHrd(*it_other))
        {
            pricelist_List.insert(it_main,*it_other);
            it_main--;
            it_other++;

        } else // ����� ��������� ��� ������� � ������� ������ ������
        {
            if (it_main != pricelist_List.end())  it_main++; // �����, 2 �������� - ����������� ������� � ������� ������, ���� �� ����� �� ���������� ��������
            else { // 3 �������� - ������� � ������� ������ ������ � �� ����� �� ���������� ��������, ��������� ������� � �����, ��������� � ����. ������������ ��������, ����������� ��������� �� ����� �����, ��� ���������
                pricelist_List.push_back(*it_other);
                it_other++;
                it_main++;
            }
        }
    }
    other.pricelist_List.clear(); // ������� ������, ������� �������� � ��������
}

// ������� ���� ������� �� ������ �����������
void PriceList::mergeVID(PriceList& other)
{
    if (this == &other) throw std::logic_error("������ ���������� ������ � ����� �����!");
    List::iterator it_main = pricelist_List.begin(), it_other = other.pricelist_List.begin();
    while (it_other != ++other.pricelist_List.end()) // ���� �� nullptr
    {
        // 1 �������� ������� - � �������� ������ ������� ��� ���������� �������� > ��� �����, �� ��������� ������� �� ������� ������ ����� ���
        if (getVid(*it_main) >= getVid(*it_other))
        {
            pricelist_List.insert(it_main,*it_other);
            it_main--;
            it_other++;

        } else // ����� ��������� ��� ������� � ������� ������ ������
        {
            if (it_main != pricelist_List.end())  it_main++; // �����, 2 �������� - ����������� ������� � ������� ������, ���� �� ����� �� ���������� ��������
            else { // 3 �������� - ������� � ������� ������ ������ � �� ����� �� ���������� ��������, ��������� ������� � �����, ��������� � ����. ������������ ��������, ����������� ��������� �� ����� �����, ��� ���������
                pricelist_List.push_back(*it_other);
                it_other++;
                it_main++;
            }
        }
    }
    other.pricelist_List.clear(); // ������� ������, ������� �������� � ��������
}

// ���������� �������� ��� ID
void PriceList::mergeSortID(List::size_type l, List::size_type r) noexcept
{
    if (l == r)
        return; // ������� ����������
    List::size_type mid = (l + r) / 2;
    mergeSortID(l, mid);
    mergeSortID(mid+1, r);
    List::size_type i = l;  // ������ ������� ����
    List::size_type j = mid + 1; // ������ ������� ����
    List tmp; // �������������� ������
    for (List::size_type step = 0; step < r - l + 1; step++) // ��� ���� ��������� ��������������� �������
    {
        // ���������� � ����������� ������������������ ������� �� ��������� ���� �����
        // ��� ������� ������� ���� ���� j > r
        if ((j > r)  || ((i <= mid) && (getID(*(pricelist_List.begin().advance(i))) < getID(*(pricelist_List.begin().advance(j))))))
        {
            tmp.push_back(*(pricelist_List.begin().advance(i)));
            i++;
        }
        else
        {
            tmp.push_back(*(pricelist_List.begin().advance(j)));
            j++;
        }
    }
    for (List::size_type step = 0; step < r - l + 1; step++)
    {
        *(pricelist_List.begin().advance(l+step)) = *(tmp.begin().advance(step));
    }
    tmp.clear(); // ������� �������������� ������
}

// ���������� ��������� ��� MEM
void PriceList::mergeSortMEM(List::size_type l, List::size_type r) noexcept
{
    if (l == r)
        return; // ������� ����������
    List::size_type mid = (l + r) / 2;
    mergeSortMEM(l, mid);
    mergeSortMEM(mid+1, r);
    List::size_type i = l;  // ������ ������� ����
    List::size_type j = mid + 1; // ������ ������� ����
    List tmp; // �������������� ������
    for (List::size_type step = 0; step < r - l + 1; step++) // ��� ���� ��������� ��������������� �������
    {
        // ���������� � ����������� ������������������ ������� �� ��������� ���� �����
        // ��� ������� ������� ���� ���� j > r
        if ((j > r)  || ((i <= mid) && (std::get<mem>(*(pricelist_List.begin().advance(i))) < std::get<mem>(*(pricelist_List.begin().advance(j))))))
        {
            tmp.push_back(*(pricelist_List.begin().advance(i)));
            i++;

        }
        else
        {
            tmp.push_back(*(pricelist_List.begin().advance(j)));
            j++;
        }
    }

    for (List::size_type step = 0; step < r - l + 1; step++)
    {
        *(pricelist_List.begin().advance(l+step)) = *(tmp.begin().advance(step));
    }
    tmp.clear(); // ������� �������������� ������
}

// ���������� �������� ��� HRD
void PriceList::mergeSortHRD(List::size_type l, List::size_type r) noexcept
{
    if (l == r)
        return; // ������� ����������
    List::size_type mid = (l + r) / 2;
    mergeSortHRD(l, mid);
    mergeSortHRD(mid+1, r);
    List::size_type i = l;  // ������ ������� ����
    List::size_type j = mid + 1; // ������ ������� ����
    List tmp; // �������������� ������
    for (List::size_type step = 0; step < r - l + 1; step++) // ��� ���� ��������� ��������������� �������
    {
        // ���������� � ����������� ������������������ ������� �� ��������� ���� �����
        // ��� ������� ������� ���� ���� j > r
        if ((j > r)  || ((i <= mid) && (std::get<hrd>(*(pricelist_List.begin().advance(i))) < std::get<hrd>(*(pricelist_List.begin().advance(j))))))
        {
            tmp.push_back(*(pricelist_List.begin().advance(i)));
            i++;
        }
        else
        {
            tmp.push_back(*(pricelist_List.begin().advance(j)));
            j++;
        }
    }

    for (List::size_type step = 0; step < r - l + 1; step++)
    {
        *(pricelist_List.begin().advance(l+step)) = *(tmp.begin().advance(step));
    }
    tmp.clear(); // ������� �������������� ������
}

// ���������� ��������� ��� VID
void PriceList::mergeSortVID(List::size_type l, List::size_type r) noexcept
{
    if (l == r)
        return; // ������� ����������
    List::size_type mid = (l + r) / 2;
    mergeSortVID(l, mid);
    mergeSortVID(mid+1, r);
    List::size_type i = l;  // ������ ������� ����
    List::size_type j = mid + 1; // ������ ������� ����
    List tmp; // �������������� ������
    for (List::size_type step = 0; step < r - l + 1; step++) // ��� ���� ��������� ��������������� �������
    {
        // ���������� � ����������� ������������������ ������� �� ��������� ���� �����
        // ��� ������� ������� ���� ���� j > r
        if ((j > r)  || ((i <= mid) && (std::get<vid>(*(pricelist_List.begin().advance(i))) <= std::get<vid>(*(pricelist_List.begin().advance(j))))))
        {
            tmp.push_back(*(pricelist_List.begin().advance(i)));
            i++;
        }
        else
        {
            tmp.push_back(*(pricelist_List.begin().advance(j)));
            j++;
        }
    }
    for (List::size_type step = 0; step < r - l + 1; step++)
    {
        *(pricelist_List.begin().advance(l+step)) = *(tmp.begin().advance(step));
    }
    tmp.clear(); // ������� �������������� ������
}

// ������� �� ������ �������� ������������ ������ ��������� ���������� ���������
void PriceList::sort(const WhatToTake &key) noexcept
{
    if (pricelist_List.size() != 0) // ���� � ������ ���� ��������, �� ����� �����������
    // � ����������� �� ����� �������� �� ������ �������� �������������� ����������
    switch (key)
    {
        case id: mergeSortID(0, pricelist_List.size()-1);
        break;
        case mem:  mergeSortMEM(0, pricelist_List.size()-1);
        break;
        case hrd: mergeSortHRD(0, pricelist_List.size()-1);
        break;
        default: mergeSortVID(0, pricelist_List.size()-1);
    }
}

// ������� �� ������ �������� ���������� ������� ��������
void PriceList::mergeType(const WhatToTake &key, PriceList &other) noexcept
{
    // � ����������� �� ����� �������� �� ������ �������� �������������� �������
    switch (key)
    {
        case id: mergeID(other);
        break;
        case mem:  mergeMEM(other);
        break;
        case hrd: mergeHRD(other);
        break;
        default: mergeVID(other);
    }
}

// ���������� ������ � ������
void PriceList::add(const List::value_type&  new_computer) noexcept
{
    pricelist_List.push_back(new_computer);
}

// ��������� ������
bool PriceList::change(const List::value_type&  new_computer) noexcept
{
    if (pricelist_List.size() == 0) return false; // ���� � ������ ��� ���������, �� �������� � ��� ������ ������
    List::iterator it = pricelist_List.begin();
    List::size_type to_cngID = getID(new_computer);
    while (it !=  pricelist_List.end() && getID(*it) != to_cngID) it++;// ���� ���������� �������
    // ���� ����� �������, �� ������
    if (getID(*it) == to_cngID)
    {
        *it = new_computer; // �������� ��������
        return true; // �������� �������
    }
    return false; // �� ���������� ����� � ������� �������
}


// �������� ��������
bool PriceList::to_delete(const ID& to_delete) noexcept
{
    if (pricelist_List.size() == 0) return false; // ��������� ��� - �� ��������� �������
    List::iterator it = pricelist_List.begin();
    List::size_type to_delID = std::atoi((to_delete).id);
     while (it != pricelist_List.end() && getID(*it) != to_delID) it++;// ���� ���������� �������
    // ���� ����� �������, �� ������
    if (getID(*it) == to_delID)
    {
        pricelist_List.erase(it); // ������� �������
        return true; // �������� �������
    }
    return false; // �� ���������� ����� � ������� �������
}


// ��������� ����� ������ ��� �������� �������� ����� ������ ��� ������ etalon
PriceList PriceList::newListID(const BiggerLower& val, const ID& etalon) noexcept
{
    PriceList tmp_list("��������� ������",create_date);
    List::iterator it = pricelist_List.begin();
    List::size_type etalonID = std::atoi(etalon.id);
    if (val == bigger) // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getID(*it) > etalonID) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    else // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getID(*it) < etalonID) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    return tmp_list;
}

// ��������� ����� ������ ��� �������� �������� ����� ������ ��� ������ etalon
PriceList PriceList::newListMEM(const BiggerLower& val, const CAPACITY_M& etalon) noexcept
{
    PriceList tmp_list("��������� ������",create_date);
    List::iterator it = pricelist_List.begin();
    if (val == bigger) // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getMem(*it) > etalon) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    else // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getMem(*it) < etalon) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    return tmp_list;
}

// ��������� ����� ������ ��� �������� �������� ����� ������ ��� ������ etalon
PriceList PriceList::newListHRD(const BiggerLower& val, const CAPACITY_H& etalon) noexcept
{
    PriceList tmp_list("��������� ������",create_date);
    List::iterator it = pricelist_List.begin();
    if (val == bigger) // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getHrd(*it) > etalon) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    else // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getHrd(*it) < etalon) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    return tmp_list;
}

// ��������� ����� ������ ��� �������� �������� ����� ������ ��� ������ etalon
PriceList PriceList::newListVID(const BiggerLower& val, const CAPACITY_C& etalon) noexcept
{
    PriceList tmp_list("��������� ������",create_date);
    List::iterator it = pricelist_List.begin();
    if (val == bigger) // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getVid(*it) > etalon) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    else // ���� ������
    {
        while (it != ++pricelist_List.end())
            {
                if (getVid(*it) < etalon) tmp_list.add(*it); // ������� ������� ���� �� ������, ��� ����
                it++;
            }
    }
    return tmp_list;
}

// �������� ����� ������ ������������, ��� ���������� �������� �� id mem hrd vid � ������ ��� ������
PriceList PriceList::getList(const WhatToTake& key, const BiggerLower& val, const List::value_type& etalon)
{
   if (pricelist_List.size() == 0) throw std::length_error("������ ����!");
   switch(key)
   {
    case id: return newListID(val, std::get<id>(etalon));
        break;
    case mem: return newListMEM(val, std::get<mem>(etalon));
        break;
    case hrd: return newListHRD(val, std::get<hrd>(etalon));
        break;
    default: return newListVID(val, std::get<vid>(etalon));

   }
}

