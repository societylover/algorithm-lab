#ifndef PRICELIST_H
#define PRICELIST_H
#include <List.h>

class PriceList
{
    // ��������, ���� �������� � ���������� � �������
    date create_date;
    NAME firm_name;
    CONTAIN computrs_in_available = 0;
    // ������ � ������������
    List pricelist_List;

    // �������� ������������ ��������� ����
    void date_check(const date &_date) const
    {
        if (std::get<day>(_date) < min_day || std::get<day>(_date) > max_day
           || std::get<month>(_date) < min_month || std::get<month>(_date) > max_month) throw std::logic_error("������������ ����!");
    }
    // ��������� ���� ������ �� ��� �� id ��� ������ ��� ������
    PriceList splitUpID(const BiggerLower &val, const ID &etalon);
    // ��������� ���� ������ �� ��� �� mem ��� ������ ��� ������
    PriceList splitUpMEM(const BiggerLower &val, const CAPACITY_M &etalon);
    // ��������� ���� ������ �� ��� �� hrd ��� ������ ��� ������
    PriceList splitUpHRD(const BiggerLower &val, const CAPACITY_H &etalon);
    // ��������� ���� ������ �� ��� �� mem ��� ������ ��� ������
    PriceList splitUpVID(const BiggerLower &val, const CAPACITY_C &etalon);

    // ���������� �������� ��� ID
    void mergeSortID(List::size_type l, List::size_type r) noexcept;
    // ���������� ��������� ��� MEM
    void mergeSortMEM(List::size_type l, List::size_type r) noexcept;
    // ���������� �������� ��� HRD
    void mergeSortHRD(List::size_type l, List::size_type r) noexcept;
    // ���������� ��������� ��� VID
    void mergeSortVID(List::size_type l, List::size_type r) noexcept;

    // ������� ���� ��������������� ��������
    void mergeID(PriceList& other);  // �� id
    void mergeMEM(PriceList& other); // �� mem
    void mergeHRD(PriceList& other); // �� hrd
    void mergeVID(PriceList& other); // �� vid

    // ��������� ������ ������ ��� ������ �������� ��������
    PriceList newListID(const BiggerLower &val,  const ID& etalon) noexcept;
    PriceList newListMEM(const BiggerLower &val, const CAPACITY_M& etalon) noexcept;
    PriceList newListHRD(const BiggerLower &val, const CAPACITY_H& etalon) noexcept;
    PriceList newListVID(const BiggerLower &val, const CAPACITY_C& etalon) noexcept;

    public:
    // ������������
    explicit PriceList(const NAME &_name, const date &_date) : firm_name(_name) // ����� � ������
    {
        date_check(_date); // �������� ����, ���� ����� �� �������� ��
        create_date = _date;
    };

    PriceList(const NAME &_name, const date &_date, const std::initializer_list<List::value_type> &t) : PriceList(_name, _date) // ����, ��� � ������
    {
        pricelist_List = List{t};
        computrs_in_available = pricelist_List.size();
    };

    PriceList(const PriceList& other) noexcept : create_date(other.create_date), firm_name(other.firm_name)  // ������ �����-������
    {
        pricelist_List = other.pricelist_List;
        computrs_in_available = pricelist_List.size();
    };

    PriceList(PriceList&& other) noexcept; // ��������� ����� ������
    PriceList& operator=(PriceList&& other) noexcept;   // -- �������� ����������� --
    PriceList& operator=(const PriceList& other);
    // ����������
    ~PriceList();

    // ��������� ����������� ���������
    void shuffle() noexcept;
    void unique() noexcept;
    List::size_type size() const noexcept;
    // �������� ���������
    void startIDsorting() noexcept; // ��� ������� ����������, �.�. ������ � ���������� ������ �� ���� ����������� ��������� begin end
    // ��������� ��������
    List::value_type& getElement(const List::size_type& it_counter) noexcept;
    // �������� ����� �� 8 ��������� ������
    List::value_type& binarySearch(const ID& searched) noexcept;
    // ���������������� ����� �� 8 ��������� ������
    List::value_type& interpolarSearch(const ID& searched) noexcept;
    // ��������� ���� ������ �� ��� � ����������� �� ����� � �������� - ����� �����
    PriceList splitUp(const WhatToTake &key, const BiggerLower &val, const List::value_type &etalon);
    void sort(const WhatToTake &key) noexcept; // ���������� ���������� �������� �� ��������
    // ����� ���� �������, �� �����, �� �������� ���� ����������
    void mergeType(const WhatToTake &key, PriceList &other) noexcept;

    // ���������� ������ � ������
    void add(const List::value_type &new_computer) noexcept;
    // ��������� ������ � ������
    bool change(const List::value_type &new_computer) noexcept;
    // �������� ������
    bool to_delete(const ID &to_delete) noexcept;

    // �������� ������, �������� id/mem/hrd/vid � ������ ��� ������
    PriceList getList(const WhatToTake &key, const BiggerLower &val, const List::value_type &etalon); // etalon �������� ����������� ���� ��� ������
};

#endif // PRICELIST_H
