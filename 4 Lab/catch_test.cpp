#define get_e(tmp, number) std::get<number>(tmp)

#include <random> // ��� mt19937
#include <chrono> // ��� mt19937
#include <list> // ��� ������� 2
#include <vector> // �.�. ��� list �� ������������ shuffle, �� ������ ����� �������������

#include "catch.hpp"
#include "PriceList.h"



// ��������� ID
std::string generateID()
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count()); // chrono-seed
	static std::uniform_int_distribution<int> numbr(65, 122); // � ascii � 65 �� 122 - ���� ���� � ��������
    std::string id = "12345678";
    for (uint8_t i = 0; i < 9; i++) id[i] = numbr(rng);
    return id;
}

// ��������� �������� ���������� ���������
List::value_type generateElement()
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count()); // chrono-seed
	static std::uniform_int_distribution<int> numbr(1, 1000); // � ascii � 65 �� 122 - ���� ���� � ��������
	List::value_type element(generateID().c_str(), "type", numbr(rng),numbr(rng),numbr(rng),numbr(rng),numbr(rng));
    return element;
}

TEST_CASE("������������ ������ PriceList ")
{
    SECTION("�������� � ������������� ��������� ������")
    {
        // ������ ����� � ����� ��������
        PriceList first("mvideo", std::make_tuple(1,1,2001));
        date date_s = std::make_tuple(3,4,2010);
        PriceList second("mvideo", date_s);
        PriceList third("eldorado", date_template);

        std::string name_frm = "svyaznoy";

        computer tmp1 (std::make_tuple("1","11",1,1,1,1,1));
        computer tmp2 (std::make_tuple("2","22",2,2,2,1,1));
        computer tmp3 (std::make_tuple("3","33",3,3,3,1,1));

        // ������, ����� �������� � ������� �������������
        PriceList fourth(name_frm, date_template, {tmp1, tmp2, tmp3});

        PriceList fifth(name_frm, date_template, {std::make_tuple("1","11",1,1,1,1,1), std::make_tuple("2","22",2,2,2,1,1), std::make_tuple("3","33",3,3,3,1,1)});
        PriceList sixth("technosila", std::make_tuple(10,10,2000), {std::make_tuple("1","11",1,1,1,1,1), std::make_tuple("2","22",2,2,2,1,1), std::make_tuple("3","33",3,3,3,1,1)});

        //������ ����������� &
         PriceList seventh(fifth);
        //������ ����������� &
         PriceList eight(seventh);

         //������ ����������� &&
         PriceList ninth(PriceList("technosila", std::make_tuple(10,10,2000), {std::make_tuple("77","11",1,1,1,1,1), std::make_tuple("2","22",2,2,2,1,1), std::make_tuple("3","33",3,3,3,1,1)}));
        //������ ����������� &&
         PriceList tenth(PriceList(name_frm, date_template, {std::make_tuple("1","11",1,1,1,1,1), std::make_tuple("2","22",2,2,2,1,1), std::make_tuple("3","33",3,3,3,1,1)}));
        // �������� =
         tenth = ninth;
         CHECK(std::strcmp(std::get<0>(ninth.getElement(0)).id, std::get<0>(tenth.getElement(0)).id) == 0); // �������� id � ���� = �������
    }
    SECTION("��������� �������� ���������� ���������� ����������")
    {
        List a;
        std::generate_n(a.begin(), 5, [&a](){ a.push_back(computer_template); return generateElement(); }); // ��������� ��������� �� 5 ���������
        CHECK(a.size() == 5); // ��������� ������� ��������� ���������

        List b;
        std::generate_n(b.begin(), 3, [&b](){ b.push_back(computer_template); return generateElement(); }); // ��������� ��������� �� 5 ���������
        CHECK(b.size() == 3); // ��������� ������� ��������� ���������

        List c;
        std::generate_n(c.begin(), 3, [&c](){ c.push_back(computer_template); return generateElement(); }); // ��������� ��������� �� 5 ���������
        CHECK(c.size() == 3); // ��������� ������� ��������� ���������

    }
    SECTION("������� 1. "){
    SECTION("���������� ������ �� 8-��������� ������")
    {
        PriceList tmp("firm_name", std::make_tuple(1,1,2000), { std::make_tuple("3","type",3,3,3,3,3), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("1","type",1,1,1,1,1), std::make_tuple("4","type",4,4,4,4,4)});
        CHECK(std::strcmp(get_e(tmp.getElement(0),0).id,"3") == 0); // �������� ��� � ������� �������� �� ���������� id = 3
        CHECK(std::strcmp(get_e(tmp.getElement(1),0).id,"2") == 0); // �������� ��� � ������� �������� �� ���������� id = 2
        CHECK(std::strcmp(get_e(tmp.getElement(2),0).id,"1") == 0); // �������� ��� � �������� �������� �� ���������� id = 1
        CHECK(std::strcmp(get_e(tmp.getElement(3),0).id,"4") == 0); // �������� ��� � ����������� �������� �� ���������� id = 4
        tmp.startIDsorting();
        CHECK(std::strcmp(get_e(tmp.getElement(0),0).id,"1") == 0); // �������� ��� � ������� �������� ����� ���������� id = 1
        CHECK(std::strcmp(get_e(tmp.getElement(1),0).id,"2") == 0); // �������� ��� � ������� �������� ����� ���������� id = 2
        CHECK(std::strcmp(get_e(tmp.getElement(2),0).id,"3") == 0); // �������� ��� � �������� �������� ����� ���������� id = 3
        CHECK(std::strcmp(get_e(tmp.getElement(3),0).id,"4") == 0); // �������� ��� � ����������� �������� ����� ���������� id = 4
    }
    SECTION("�������� ����������")
    {
        // ����� unique - ������� ����������� ���������, ����. 1 2 3 3 4 1 2 2 -> 1 2 3 4 1 2
        // ��� ����, ����� �������� �������� id ���������� ���������� ������� �������������, ����. 1 2 3 3 4 1 2 2 -> ���������� 1 1 2 2 2 3 3 4 -> 1 2 3 4
        List a({std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1)});
        a.unique();
        // � ���������� ��������� � id 3 2 3
        CHECK(a.size() == 3);

        List b({std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1)});
        b.unique();
        CHECK(b.size() == 1);

        List c({std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",4,1,1,1,1), std::make_tuple("3","type",5,1,1,1,1), std::make_tuple("3","type",6,1,1,1,1)});
        c.unique();
        CHECK(c.size() == 4);

        PriceList a1("mvdioe",date_template, {std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1)});
        a1.unique();
        CHECK(a1.size() == 3);

        PriceList b1("mvdioe",date_template, {std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",1,1,1,1,1)});
        b1.unique();
        CHECK(b1.size() == 1);

        PriceList c1("mvdioe",date_template, {std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("3","type",4,1,1,1,1), std::make_tuple("3","type",5,1,1,1,1), std::make_tuple("3","type",6,1,1,1,1)});
        c1.unique();
        CHECK(c1.size() == 4);
    }
    SECTION("�������� ����������� �������� - ����� ������-����������")
    {
        List a({std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",1,1,1,1,1), std::make_tuple("6","type",1,1,1,1,1)});
        a.shuffle();
        CHECK(std::strcmp(get_e(*a.begin(),0).id,"3") != 0); // ���� �����������, ��� ������� ��������� �� �����
        CHECK(std::strcmp(get_e(*(++a.begin()),0).id,"2") != 0); // ���� �����������, ��� ������� ��������� �� �����

        PriceList b("citilink", date_template,{std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",1,1,1,1,1), std::make_tuple("6","type",1,1,1,1,1)});
        b.shuffle();

        CHECK(std::strcmp(get_e(b.getElement(0),0).id, "3") != 0); // ���� �����������, ��� ������� ��������� �� �����
        CHECK(std::strcmp(get_e(b.getElement(1),0).id, "2") != 0); // ���� �����������, ��� ������� ��������� �� �����
    }
    SECTION("����������� �������� ��������� ������ 8-��������� ������")
    {
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",4,1,1,1,1), std::make_tuple("6","type",5,11,1,1,1)});

        // ��� ���������� ������ ���������� �������������
        a.startIDsorting();

        CHECK(get_e(a.binarySearch("3"), mem) == 1); // �������� ��� ������ ������� � ������� 3
        CHECK(get_e(a.binarySearch("2"), mem) == 2); // �������� ��� ������ ������� � ������� 2
        CHECK(get_e(a.binarySearch("5"), mem) == 4); // �������� ��� ������ ������� � ������� 5
        CHECK(get_e(a.binarySearch("6"), mem) == 5); // �������� ��� ��������� ������� � �������  6

        // �������� ��� ��� ������ �������� � id, �������� ���  � ������, ����� ����� ��������� ������� ������
        CHECK(get_e(a.binarySearch("7"), mem) == 5);
        CHECK(get_e(a.binarySearch("8"), mem) == 5);
        CHECK(get_e(a.binarySearch("9"), hrd) == 11);
    }
    SECTION("����������� �������� ����������������� ������ 8-��������� ������ ")
    {
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",4,1,1,1,1), std::make_tuple("6","type",5,11,1,1,1)});

        // ��� ���������� ������ ���������� �������������
        a.startIDsorting();

        CHECK(get_e(a.interpolarSearch("3"), mem) == 1); // �������� ��� ������ ������� � ������� 3
        CHECK(get_e(a.interpolarSearch("2"), mem) == 2); // �������� ��� ������ ������� � ������� 2
        CHECK(get_e(a.interpolarSearch("5"), mem) == 4); // �������� ��� ������ ������� � ������� 5
        CHECK(get_e(a.interpolarSearch("6"), mem) == 5); // �������� ��� ��������� ������� � �������  6

        // �������� ��� ��� ������ �������� � id, �������� ���  � ������, ����� ����� ��������� ������� ������
        CHECK(get_e(a.interpolarSearch("7"), mem) == 5);
        CHECK(get_e(a.interpolarSearch("8"), mem) == 5);
        CHECK(get_e(a.interpolarSearch("9"), hrd) == 11);
    }
    SECTION("� ������������ � ��������� (������-������ �������� ��������) ��������� ������ �� ��� ������ ")
    {
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",4,1,1,1,1), std::make_tuple("6","type",5,11,1,1,1)});

        SECTION("by ID"){
        // ������� �� ID
        List::value_type to_splitUp = std::make_tuple("3","type",1,5,6,2,3);

        // b ��� ������, � ������� �������� ��� ���������� ��������, ����� ���������� ������
        PriceList b = a.splitUp(id, bigger, to_splitUp);
        // ���������, ��� � b ������ �������� � id ������ ��� 3, � ������ � 5 � 6
        CHECK(b.size() == 2);
        // ���������, ��� � � �������� 2 ��������
        CHECK(a.size() == 2);

        // �������� ��� � b ��������� ������� � id 5 � 6
        CHECK(std::strcmp(get_e(b.getElement(0), 0).id, "5") == 0); // ������ ��������� ����� � id 5
        CHECK(std::strcmp(get_e(b.getElement(1), 0).id, "6") == 0); // � ������ � id 6

        // �������� ��� � � ���������� �������� � id 3 � 2
        CHECK(std::strcmp(get_e(a.getElement(0), 0).id, "3") == 0); // ������ ��������� ����� � id 3
        CHECK(std::strcmp(get_e(a.getElement(1), 0).id, "2") == 0); // � ������ � id 2
        }
        SECTION("by MEM"){
        PriceList c("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",4,1,1,1,1), std::make_tuple("6","type",5,11,1,1,1)});
        List::value_type to_splitUp = std::make_tuple("3","type",3,5,6,2,3);

        // � d ������� �������� � ������� ����� ������ ������ 3
        PriceList d = c.splitUp(mem, lower, to_splitUp);
        // ���������, ��� � c ������ �������� � ������� ������ 3, � ��� �������� c id 5 � 6
        CHECK(c.size() == 2);
        // ���������, ��� � � �������� 2 ��������
        CHECK(d.size() == 2);

        // �������� ��� � c ��������� ������� � id 5 � 6
        CHECK(std::strcmp(get_e(c.getElement(0), 0).id, "5") == 0); // ������ ��������� ����� � id 5
        CHECK(std::strcmp(get_e(c.getElement(1), 0).id, "6") == 0); // � ������ � id 6

        // �������� ��� � d ���������� �������� � id 3 � 2
        CHECK(std::strcmp(get_e(d.getElement(0), 0).id, "3") == 0); // ������ ��������� ����� � id 3
        CHECK(std::strcmp(get_e(d.getElement(1), 0).id, "2") == 0); // � ������ � id 2
        }
        SECTION("by HRD"){
        PriceList f("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",4,1,1,1,1), std::make_tuple("6","type",5,11,1,1,1)});
        List::value_type to_splitUp = std::make_tuple("3","type",3,5,6,2,3);

        // � j ������� �������� � ������� ����� �������� ����� ������ 5
        PriceList j = f.splitUp(hrd, lower, to_splitUp);
        // �������� ��� � f ������� 1 �������, � ������� ����� �������� ����� ������ 5
        CHECK(f.size() == 1);
        // ���������, ��� � j ��������, � ������� ����� �������� ����� ������ 5
        CHECK(j.size() == 3);

        // �������� ��� � f ��������� ������ � id 6
         CHECK(std::strcmp(get_e(f.getElement(0), 0).id, "6") == 0); // ������������ �������, � �������� hrd > 5

        // �������� ��� � j ���������� �������� � id 3 � 2 � 5
         CHECK(std::strcmp(get_e(j.getElement(0), 0).id, "3") == 0); // ������ ��������� ����� � id 3
         CHECK(std::strcmp(get_e(j.getElement(1), 0).id, "2") == 0); // � ������ � id 2
         CHECK(std::strcmp(get_e(j.getElement(2), 0).id, "5") == 0); // � ������ � id 5
        }
        SECTION("by VID"){
        PriceList f("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("5","type",4,1,1,1,1), std::make_tuple("6","type",5,11,1,1,1)});
        List::value_type to_splitUp = std::make_tuple("3","type",3,5,10,2,3);

        // � f ������� �������� � ������� ����� ����������� ������ 10, � ����� ���
        PriceList j = f.splitUp(vid, bigger, to_splitUp);
        // �������� ��� � f �������� ��� ��������
        CHECK(f.size() == 4);
        // ���������, ��� � j �������� �� ���������
        CHECK(j.size() == 0);

        // �������� ��� � f ��������� ������ � id  3 2 5 6
         CHECK(std::strcmp(get_e(f.getElement(0), 0).id, "3") == 0); // ������
         CHECK(std::strcmp(get_e(f.getElement(1), 0).id, "2") == 0); // ������
         CHECK(std::strcmp(get_e(f.getElement(2), 0).id, "5") == 0); // 3
         CHECK(std::strcmp(get_e(f.getElement(3), 0).id, "6") == 0); // 4
        }
    }
    SECTION("������������� ��� ������ �� ����� �� ��������. ����������� �������� ���������� ��������")
    {
        SECTION("by ID"){
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1),std::make_tuple("1","type",1,1,1,1,1), std::make_tuple("0","type",1,1,1,1,1), std::make_tuple("2","type",2,2,2,2,2), std::make_tuple("6","type",5,11,1,1,1), std::make_tuple("5","type",4,1,1,1,1)});

        // ������� �� ID
        List::value_type to_splitUp = std::make_tuple("3","type",1,5,6,2,3);

        // b ��� ������, � ������� �������� ��� ���������� ��������, ����� ���������� ������
        PriceList b = a.splitUp(id, bigger, to_splitUp);
        // ���������, ��� � b ������ �������� � id ������ ��� 3, � ������ � 5 � 6
        CHECK(b.size() == 2);
        // ���������, ��� � � �������� 2 ��������
        CHECK(a.size() == 4);

        // ���������� �������� �� id
        a.sort(id);
        b.sort(id);

        // �������� �������������� ������
        CHECK(std::strcmp(get_e(b.getElement(0), 0).id, "5") == 0); // ������ ��������� ����� � id 5
        CHECK(std::strcmp(get_e(b.getElement(1), 0).id, "6") == 0); // � ������ � id 6

        // �������� ��� ������� ����� ���������� �� id 0 1 2 3
        CHECK(std::strcmp(get_e(a.getElement(0), 0).id, "0") == 0); // ������ ��������� ����� � id 0
        CHECK(std::strcmp(get_e(a.getElement(1), 0).id, "1") == 0); // � ������ � id 1
        CHECK(std::strcmp(get_e(a.getElement(2), 0).id, "2") == 0); // � ������ � id 2
        CHECK(std::strcmp(get_e(a.getElement(3), 0).id, "3") == 0); // � ������ � id 2
    }
    SECTION("by MEM"){
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,1,1,1,1),std::make_tuple("1","type",4,1,1,1,1), std::make_tuple("0","type",2,2,1,1,1), std::make_tuple("2","type",3,2,2,2,2), std::make_tuple("6","type",6,11,1,1,1), std::make_tuple("5","type",7,1,1,1,1)});

        // ������� �� ID
        List::value_type to_splitUp = std::make_tuple("3","type",5,5,6,2,3);

        // b ��� ������, � ������� �������� ��� ���������� ��������, ����� ���������� ������
        PriceList b = a.splitUp(mem, bigger, to_splitUp);
        // ���������, ��� � b ������ �������� � mem ������ ��� 5, � ������ � 5 � 6
        CHECK(b.size() == 2);
        // ���������, ��� � � �������� 2 ��������
        CHECK(a.size() == 4);

        // ���������� �������� �� mem
        a.sort(mem);
        b.sort(mem);

        // �������� �������������� ������
        // ������ ������������ �� mem, ������� ������� id 6 5
        CHECK(std::strcmp(get_e(b.getElement(0), 0).id, "6") == 0); // ������ ��������� ����� � id 6
        CHECK(std::strcmp(get_e(b.getElement(1), 0).id, "5") == 0); // � ������ � id 5

        // ������ ������������ �� mem, ������� ������� id 3 0 2 1
        CHECK(std::strcmp(get_e(a.getElement(0), 0).id, "3") == 0); // ������ ��������� ����� � id 3
        CHECK(std::strcmp(get_e(a.getElement(1), 0).id, "0") == 0); // � ������ � id 0
        CHECK(std::strcmp(get_e(a.getElement(2), 0).id, "2") == 0); // � ������ � id 2
        CHECK(std::strcmp(get_e(a.getElement(3), 0).id, "1") == 0); // � ������ � id 1
    }
    SECTION("by HRD"){
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,2,1,1,1),std::make_tuple("1","type",4,0,1,1,1), std::make_tuple("0","type",2,1,1,1,1), std::make_tuple("2","type",3,0,2,2,2), std::make_tuple("6","type",6,11,1,1,1), std::make_tuple("5","type",7,22,1,1,1)});

        // ������� �� ID
        List::value_type to_splitUp = std::make_tuple("3","type",5,3,6,2,3);

        // b ��� ������, � ������� �������� ��� ���������� ��������, ����� ���������� ������
        PriceList b = a.splitUp(hrd, lower, to_splitUp);
        // ���������, ��� � b ������ �������� � ������� ������ ��� �������� � hrd < 3
        CHECK(b.size() == 4);
        // ���������, ��� � � �������� 2 ��������
        CHECK(a.size() == 2);

        // ���������� �������� �� id
        a.sort(hrd);
        b.sort(hrd);

        // �������� �������������� ������
        // ������ ������������ �� hrd, ������� ������� id 2 1 0 3
        CHECK(std::strcmp(get_e(b.getElement(0), 0).id, "2") == 0);
        CHECK(std::strcmp(get_e(b.getElement(1), 0).id, "1") == 0);
        CHECK(std::strcmp(get_e(b.getElement(2), 0).id, "0") == 0);
        CHECK(std::strcmp(get_e(b.getElement(3), 0).id, "3") == 0);

        // ������ ������������ �� hrd, ������� ������� id 6 5
        CHECK(std::strcmp(get_e(a.getElement(0), 0).id, "6") == 0); // ������ ��������� ����� � id 6
        CHECK(std::strcmp(get_e(a.getElement(1), 0).id, "5") == 0); // � ������ � id 5
    }
    SECTION("by VID"){
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,2,1,1,1),std::make_tuple("1","type",4,0,1,1,1), std::make_tuple("0","type",2,1,1,1,1), std::make_tuple("2","type",3,0,2,2,2), std::make_tuple("6","type",6,11,1,1,1), std::make_tuple("5","type",7,22,1,1,1)});

        // ������� �� ID
        List::value_type to_splitUp = std::make_tuple("3","type",5,3,6,2,3);

        // b ��� ������, � ������� �������� ��� ���������� ��������, ����� ���������� ������
        PriceList b = a.splitUp(vid, bigger, to_splitUp);
        // ���������, ��� � b ������ �������� � ������� ������ ��� �������� � vid > 6
        CHECK(b.size() == 0);
        // ���������, ��� � � �������� ��� ��������
        CHECK(a.size() == 6);

        // ���������� �������� �� id
        a.sort(vid);

        b.sort(vid);

        // �������� �������������� ������

        // ������ ������������ �� vid, ������� ������� id 3 1 0 6 5 2
        CHECK(std::strcmp(get_e(a.getElement(0), 0).id, "3") == 0);
        CHECK(std::strcmp(get_e(a.getElement(1), 0).id, "1") == 0);
        CHECK(std::strcmp(get_e(a.getElement(2), 0).id, "0") == 0);
        CHECK(std::strcmp(get_e(a.getElement(3), 0).id, "6") == 0);
        CHECK(std::strcmp(get_e(a.getElement(4), 0).id, "5") == 0);
        CHECK(std::strcmp(get_e(a.getElement(5), 0).id, "2") == 0);
    }
    }
    SECTION("����������� �������� ������� ���� ��������������� �������")
    {
        PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,2,1,1,1),std::make_tuple("1","type",4,0,1,1,1), std::make_tuple("0","type",2,1,1,1,1), std::make_tuple("2","type",3,0,2,2,2), std::make_tuple("6","type",6,11,1,1,1), std::make_tuple("5","type",7,22,1,1,1)});
        PriceList b("mvideo", date_template, { std::make_tuple("66","type",1,2,1,1,1),std::make_tuple("55","type",4,0,1,1,1)});

        // ���������� a � � �� id, ��� ������� �� mem hrd vid
        a.sort(id);
        b.sort(id);
        // ������� ������� � � �
        a.mergeType(id, b); // �� id

        // � � ������� ��� �������� b
        CHECK(a.size() == 8);
        // � � �� �������� ���������
        CHECK(b.size() == 0);

        // �������� ������� ������
        CHECK(std::strcmp(get_e(a.getElement(0), 0).id, "0") == 0);
        CHECK(std::strcmp(get_e(a.getElement(1), 0).id, "1") == 0);
        CHECK(std::strcmp(get_e(a.getElement(2), 0).id, "2") == 0);
        CHECK(std::strcmp(get_e(a.getElement(3), 0).id, "3") == 0);
        CHECK(std::strcmp(get_e(a.getElement(4), 0).id, "5") == 0);
        CHECK(std::strcmp(get_e(a.getElement(5), 0).id, "6") == 0);
        CHECK(std::strcmp(get_e(a.getElement(6), 0).id, "55") == 0);
        CHECK(std::strcmp(get_e(a.getElement(7), 0).id, "66") == 0);
    }
    SECTION("���������� ��������� � ������")
    {
     PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,2,1,1,1),std::make_tuple("1","type",4,0,1,1,1), std::make_tuple("0","type",2,1,1,1,1), std::make_tuple("2","type",3,0,2,2,2), std::make_tuple("6","type",6,11,1,1,1), std::make_tuple("5","type",7,22,1,1,1)});
     a.add(std::make_tuple("66","type",1,2,1,1,1));
     CHECK(a.size() == 7); // ����� ��������� ������ ����� 7

     a.add(std::make_tuple("77","type",1,2,1,1,1));
     CHECK(a.size() == 8); // ����� ��������� ������ ����� 7

     // �������� ����������� ���������
     CHECK(std::strcmp(get_e(a.getElement(6), 0).id, "66") == 0);
     CHECK(std::strcmp(get_e(a.getElement(7), 0).id, "77") == 0);
    }
    SECTION("�������� �������� �� ������")
    {
     PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,2,1,1,1),std::make_tuple("1","type",4,0,1,1,1), std::make_tuple("0","type",2,1,1,1,1), std::make_tuple("2","type",3,0,2,2,2), std::make_tuple("6","type",6,11,1,1,1), std::make_tuple("5","type",7,22,1,1,1)});
     CHECK(a.to_delete("3")); // �������� ��� �������� �������
     // �������� ������ �������
     CHECK(a.size() == 5);
     CHECK_FALSE(a.to_delete("101")); // �������� ��� ������� ����� ������� ������, ��� ���
     CHECK(a.size() == 5); // ������ ���������� �� ������

    }
    SECTION("������ �������� � ������")
    {
     PriceList a("mvideo", date_template, { std::make_tuple("3","type",1,2,1,1,1),std::make_tuple("1","type",4,0,1,1,1), std::make_tuple("0","type",2,1,1,1,1), std::make_tuple("2","type",3,0,2,2,2), std::make_tuple("6","type",6,11,1,1,1), std::make_tuple("5","type",7,22,1,1,1)});
     CHECK(a.change(std::make_tuple("3","type",99,2,1,1,1))); // �������� ��� ������� �������� �������
     // �������� ������ �������
     CHECK(a.size() == 6);
     CHECK(get_e(a.getElement(0), mem) == 99); // �������� ��� ������ ������� � mem ������ 99
     CHECK_FALSE(a.change(std::make_tuple("101","type",99,2,1,1,1))); // �������� ��� ������� ����� ������� ������, ��� ���
     CHECK(a.size() == 6); // ������ ���������� �� ������
    }
    }
    SECTION("������� 2.")
    {
        SECTION("���������� generate_n")
        {
            std::list<List::value_type> a(5, computer_template);
            std::generate_n(a.begin(), 5, generateElement);
            std::generate(a.begin(), a.end(), generateElement);

            CHECK(a.size() != 4);
            CHECK(a.size() == 5);
            CHECK(*a.begin() != *a.end());
        }
        SECTION("���������� �� 8 ��������� ������")
        {
            std::list<List::value_type> a = { std::make_tuple("3","type",1,2,1,1,1), std::make_tuple("2","type",1,2,1,1,1), std::make_tuple("6","type",1,2,1,1,1), std::make_tuple("1","type",1,2,1,1,1)};

            a.sort(functor_ekz());

            CHECK(a.size() == 4);
            // �������� ��� ������ ���������� � ������� ��������

            CHECK(std::strcmp(get_e(*a.begin(), 0).id, "6") == 0);
            CHECK(std::strcmp(get_e(*(++a.begin()), 0).id, "3") == 0);
            CHECK(std::strcmp(get_e(*(++(++a.begin())), 0).id, "2") == 0);
            CHECK(std::strcmp(get_e(*(--a.end()), 0).id, "1") == 0);

            // � ������� �����������
            a.sort(smaller_ekz);
            CHECK(a.size() == 4);
            // �������� ��� ������ ���������� � ������� �����������

            CHECK(std::strcmp(get_e(*a.begin(), 0).id, "1") == 0);
            CHECK(std::strcmp(get_e(*(++a.begin()), 0).id, "2") == 0);
            CHECK(std::strcmp(get_e(*(++(++a.begin())), 0).id, "3") == 0);
            CHECK(std::strcmp(get_e(*(--a.end()), 0).id, "6") == 0);
        }
        SECTION("�������� ����������")
        {
            std::list<List::value_type> a = { std::make_tuple("3","type",1,2,1,1,1), std::make_tuple("3","type",1,2,1,1,1), std::make_tuple("3","type",1,2,1,1,1), std::make_tuple("1","type",1,2,1,1,1)};


            // ������ ������ ������������� ��������
            a.unique();

            // ������ ������ 2
            CHECK(a.size() == 2);

            CHECK(std::strcmp(get_e(*a.begin(), 0).id, "3") == 0);
            CHECK(std::strcmp(get_e(*(++a.begin()), 0).id, "1") == 0);
        }
        SECTION("������������ ��������� ���������")
        {
            std::list<List::value_type> a = { std::make_tuple("77","type",1,2,1,1,1), std::make_tuple("2","type",1,2,1,1,1), std::make_tuple("1","type",1,2,1,1,1), std::make_tuple("6","type",1,2,1,1,1)};

            std::vector <List::value_type> tmp_a (a.begin(), a.end());
            // ������������ ���������
            std::shuffle(tmp_a.begin(), tmp_a.end(), std::mt19937{ std::random_device{}() });
            a = std::list<List::value_type>(tmp_a.begin(), tmp_a.end());

            CHECK(a.size() == 4);

            CHECK_FALSE(std::strcmp(get_e(*a.begin(), 0).id, "77") == 0); // ��� ��������� ������������ �������� ��� ��� ����� �� ������, ������� 50/50
            CHECK(std::strcmp(get_e(*(++a.begin()), 0).id, "2") == 0);
        }
        SECTION("����� �� id")
        {
            std::list<List::value_type> a = { std::make_tuple("1","type",1,2,1,1,1), std::make_tuple("2","type",66,2,1,1,1), std::make_tuple("3","type",1,2,1,1,1), std::make_tuple("4","type",1,2,1,1,1)};

            computer fnd = std::make_tuple("1","type",1,2,1,1,1);
            CHECK(std::binary_search(a.begin(), a.end(), fnd, smaller_ekz)); // ����� �������� � ������
        }

    }
}
