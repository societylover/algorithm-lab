#include "price_list.h"

void price_list::date_check(const date &created) const
    {
        // ��������� ���� �� ������������, � ������ ���� > 0 � < 32, ����� > 0 � < 13
        uint8_t day = std::stoi({created, 0, 2});
        if (day < min_day_month || day > max_day) throw std::range_error("������������ ���� (����)!");
        uint8_t month = std::stoi({created, 2, 2});
        if (month < min_day_month || month > max_month) throw std::range_error("������������ ���� (�����)!");
    }

// �������� ID �� ������������
void price_list::IDAlreadyIn(const value_type& tmp)
{
    if(std::count_if(allInfo.begin(), allInfo.end(), [tmp](value_type &a){
    return std::get<id>(a) == std::get<id>(tmp);}) != 0) throw std::invalid_argument("ID ��� ������� � �����-�����!");
}



price_list::price_list(const name& f_name, const date& created) noexcept
{
    date_check(created); // ��������� ����
    firm_name = f_name;
    strcpy(create, created); // ������ ����
}

price_list::price_list(const name& f_name, const date& created, const std::initializer_list<value_type>& t) : allInfo(t)
{
    date_check(created); // �������� ����
    firm_name = f_name;
    strcpy(create, created); // ������ ����
}

void price_list::add(const value_type &a) noexcept
{
    allInfo.ID_check(std::get<id>(a)); // �������� ���������� ID
    IDAlreadyIn(a); // �������� �� ������������
    allInfo.push_back(a); // ���� �� ���� ���������� - ���������
}

void price_list::change(const size_type& i, const value_type &tmp) noexcept
{
    allInfo.ID_check(std::get<id>(tmp)); // �������� ���������� ID
    IDAlreadyIn(tmp); // �������� �� ������������
    allInfo.at(i) = tmp; // ������ c �����������
}

size_type price_list::in_active() const noexcept
{
    return allInfo.size();
}

value_type& price_list::get(const size_type &i) const noexcept
{
    return allInfo.at(i); // ��������� �������� � �����������
}

void price_list::delete_var(const size_type& i) noexcept
{
    allInfo.erase(i); // ������� �������
}

// ���������� �� ID
void price_list::ID_sort(const search &how) noexcept
{
    size_type fst,scnd, min; // �������� ��������� ����
    for (size_type i = 0; i < allInfo.size() - 1; i++)
    {
        min = i; // ���������� ������ �������� ��������
        // ���� ����������� ������� ����� ��������� �� ����� i-���
        for (size_type j = i + 1; j < allInfo.size(); j++)  // ��� ��������� ��������� ����� i-���
        {
            fst = std::stoi(std::get<id>(allInfo[j]));
            scnd = std::stoi(std::get<id>(allInfo[min]));
            if (how == UPPER) // ���� � ������� �����������
                if (fst < scnd) // ���� ������� ������ ������������,
                min = j;       // ���������� ��� ������ � min
            if (how == LOWER) // ���� � ������� ��������
                if (fst > scnd)
                min = j;
        }
        std::swap(allInfo[i],allInfo[min]); // ����� �������� �������� � ����������� ������
    }
}

// ���������� �� ������, �������� �����, �����������
void price_list::MEM_HRD_VID_sort(const WhatToTake &key_sort, const search &how) noexcept
{
    size_type fst,scnd, min; // �������� ��������� ����
    for (size_type i = 0; i < allInfo.size() - 1; i++)
    {
        min = i; // ���������� ������ �������� ��������
        // ���� ����������� ������� ����� ��������� �� ����� i-���
        for (size_type j = i + 1; j < allInfo.size(); j++)  // ��� ��������� ��������� ����� i-���
        {
            if (key_sort == mem){
            fst = std::get<mem>(allInfo[j]);
            scnd = std::get<mem>(allInfo[min]);}
            if (key_sort == hrd) {
            fst = std::get<hrd>(allInfo[j]);
            scnd = std::get<hrd>(allInfo[min]);}
            if (key_sort == vid){
            fst = std::get<vid>(allInfo[j]);
            scnd = std::get<vid>(allInfo[min]);}
            if (how == UPPER) // ���� � ������� �����������
                if (fst < scnd) // ���� ������� ������ ������������,
                min = j;       // ���������� ��� ������ � min
            if (how == LOWER) // ���� � ������� ��������
                if (fst > scnd)
                min = j;
        }
        std::swap(allInfo[i],allInfo[min]); // ����� �������� �������� � ����������� ������
    }
}

// ���������� �������
void price_list::sort(const WhatToTake& key_sort, const search &how) noexcept
{
    // � ����������� �� ���� ����� �������� ����� ��� ���������� �� �����
    if (key_sort == id) ID_sort(how);
    else MEM_HRD_VID_sort(key_sort, how);
}

size_type price_list::linearSearchIDUnsorted() const noexcept
{
    ID searchedID = std::get<id>(allInfo[allInfo.size()-1]); // �������� ������� �������� �������

    // �������� ����� �� ID
    for(size_type i = 0; i < allInfo.size(); i++)
            if (std::get<id>(allInfo[i]) == searchedID) return i;

    return BARIER_ANYWAY;
}

size_type price_list::linearSearchMEMUnsorted() const noexcept
{
    CAPACITY_MEM searchedMEM = std::get<mem>(allInfo[allInfo.size()-1]); // �������� ������� �������� �������

    // �������� ����� �� MEM
    for(size_type i = 0; i < allInfo.size(); i++)
            if (std::get<mem>(allInfo[i]) == searchedMEM) return i;

    return BARIER_ANYWAY;
}

size_type price_list::linearSearchHRDUnsorted() const noexcept
{
    CAPACITY_HRD searchedHRD = std::get<hrd>(allInfo[allInfo.size()-1]); // �������� ������� �������� �������

    // �������� ����� �� MEM
    for(size_type i = 0; i < allInfo.size(); i++)
            if (std::get<hrd>(allInfo[i]) == searchedHRD) return i;

    return BARIER_ANYWAY;
}

size_type price_list::linearSearchVIDUnsorted() const noexcept
{
    CAPACITY_VID searchedVID = std::get<vid>(allInfo[allInfo.size()-1]); // �������� ������� �������� �������

    // �������� ����� �� MEM
    for(size_type i = 0; i < allInfo.size(); i++)
            if (std::get<vid>(allInfo[i]) == searchedVID) return i;

    return BARIER_ANYWAY;
}

// ����� ������� ������ - ������� ��������������� ��������
size_type price_list::linearSearchUnsortedBarier(const WhatToTake &key) const noexcept
{
    // ��������������� ����� �������� ����� ����� � ��������������� ����������
    if (key == id) return linearSearchIDUnsorted();
    if (key == mem) return linearSearchMEMUnsorted();
    if (key == hrd) return linearSearchHRDUnsorted();
    return linearSearchVIDUnsorted();
}

// �������� ������ � �������� � c������������ �������
size_type price_list::linearSearchIDSorted(const search &how_sorted) const noexcept
{
    ID searchedID = std::get<id>(allInfo[in_active()-1]);
    if (how_sorted == UPPER) { if (std::stoi(std::get<id>(allInfo[in_active()-2])) < std::stoi(searchedID)) return in_active()-1; }// ����� �������� ������ �� ������� ����� �������������
    // ���� ���������� �� ����������� � ������� ����� �������� < ��������, �� ������ �������� ����� - ���!
    if (how_sorted == LOWER) {  if (std::stoi(std::get<id>(allInfo[in_active()-2])) > std::stoi(searchedID)) return in_active()-1; } // ����������� ��������, ������ ��������� �� ��������
    // ���� ������� ����� �������� ������, ��� �� ����, �� ������ ������ ���

    for (size_type i = 0; i < in_active(); i++)
        if (searchedID == std::get<id>(allInfo[i])) return i;

    return BARIER_ANYWAY;
}

size_type price_list::linearSearchMEMSorted(const search &how_sorted) const noexcept
{
    CAPACITY_MEM searchedMEM = std::get<mem>(allInfo[in_active()-1]);
    if (how_sorted == UPPER) { if (std::get<mem>(allInfo[in_active()-2]) < searchedMEM) return in_active()-1; }// ����� �������� ������ �� ������� ����� �������������
    // ���� ���������� �� ����������� � ������� ����� �������� < ��������, �� ������ �������� ����� - ���!
    if (how_sorted == LOWER) { if (std::get<mem>(allInfo[in_active()-2]) > searchedMEM) return in_active()-1; } // ����������� ��������, ������ ��������� �� ��������
    // ���� ������� ����� �������� ������, ��� �� ����, �� ������ ������ ���

    for (size_type i = 0; i < in_active(); i++)
        if (searchedMEM == std::get<mem>(allInfo[i])) return i;

    return BARIER_ANYWAY;
}


size_type price_list::linearSearchHRDSorted(const search &how_sorted) const noexcept
{
    CAPACITY_HRD searchedHRD = std::get<hrd>(allInfo[in_active()-1]);
    if (how_sorted == UPPER) { if (std::get<hrd>(allInfo[in_active()-2]) < searchedHRD) return in_active()-1; }// ����� �������� ������ �� ������� ����� �������������
    // ���� ���������� �� ����������� � ������� ����� �������� < ��������, �� ������ �������� ����� - ���!
    if (how_sorted == LOWER) { if (std::get<hrd>(allInfo[in_active()-2]) > searchedHRD) return in_active()-1; } // ����������� ��������, ������ ��������� �� ��������
    // ���� ������� ����� �������� ������, ��� �� ����, �� ������ ������ ���

    for (size_type i = 0; i < in_active(); i++)
        if (searchedHRD == std::get<hrd>(allInfo[i])) return i;

    return BARIER_ANYWAY;
}


size_type price_list::linearSearchVIDSorted(const search &how_sorted) const noexcept
{
    CAPACITY_VID searchedVID = std::get<vid>(allInfo[in_active()-1]);
    if (how_sorted == UPPER) { if (std::get<vid>(allInfo[in_active()-2]) < searchedVID) return in_active()-1; }// ����� �������� ������ �� ������� ����� �������������
    // ���� ���������� �� ����������� � ������� ����� �������� < ��������, �� ������ �������� ����� - ���!
    if (how_sorted == LOWER) { if (std::get<vid>(allInfo[in_active()-2]) > searchedVID) return in_active()-1; } // ����������� ��������, ������ ��������� �� ��������
    // ���� ������� ����� �������� ������, ��� �� ����, �� ������ ������ ���

    for (size_type i = 0; i < in_active(); i++)
        if (searchedVID == std::get<vid>(allInfo[i])) return i;

    return BARIER_ANYWAY;
}

// ����� ������ ������ �� �����
size_type price_list::linearSearchSortedBarier(const WhatToTake &key, const search &how_sorted) const noexcept
{
    if (key == id) return linearSearchIDSorted(how_sorted);
    if (key == mem) return linearSearchMEMSorted(how_sorted);
    if (key == hrd) return linearSearchHRDSorted(how_sorted);
    return linearSearchVIDSorted(how_sorted);
}

size_type price_list::binarySearchMEM(const CAPACITY_MEM& to_fnd) const noexcept
{
    size_type left = 0, right = in_active()-1, mid;
    while (left <= right)
        {
            mid = (right + left) / 2;
            if (std::get<mem>(allInfo[mid]) == to_fnd) return mid;
            if (std::get<mem>(allInfo[mid]) < to_fnd) left = mid + 1;
            else right = mid - 1;
        }
    return in_active(); // ������ ������ - ����� ��������, ��� ��� �������� � ������, ������ = �������
}

size_type price_list::binarySearchHRD(const CAPACITY_HRD& to_fnd) const noexcept
{
    size_type left = 0, right = in_active()-1, mid;
    while (left <= right)
        {
            mid = (right + left) / 2;
            if (std::get<hrd>(allInfo[mid]) == to_fnd) return mid;
            if (std::get<hrd>(allInfo[mid]) < to_fnd) left = mid + 1;
            else right = mid - 1;
        }
    return in_active(); // ������ ������ - ����� ��������, ��� ��� �������� � ������, ������ = �������
}

size_type price_list::binarySearchVID(const CAPACITY_VID& to_fnd) const noexcept
{
    size_type left = 0, right = in_active()-1, mid;
    while (left <= right)
        {
            mid = (right + left) / 2;
            if (std::get<vid>(allInfo[mid]) == to_fnd) return mid;
            if (std::get<vid>(allInfo[mid]) <  to_fnd) left = mid + 1;
            else right = mid - 1;
        }
    return in_active(); // ������ ������ - ����� ��������, ��� ��� �������� � ������, ������ = �������
}


size_type price_list::interpolarSearchMEM(const CAPACITY_MEM& to_fnd) const noexcept
{
    size_type mid, low = 0, high = in_active() - 1;
    while (std::get<mem>(allInfo[low]) < to_fnd && std::get<mem>(allInfo[high]) > to_fnd) {
        mid = low + ((to_fnd - std::get<mem>(allInfo[low])) * (high - low)) / (std::get<mem>(allInfo[high]) - std::get<mem>(allInfo[low]));

        if (std::get<mem>(allInfo[mid]) < to_fnd)
            low = mid + 1;
        else if (std::get<mem>(allInfo[mid]) > to_fnd)
            high = mid - 1;
        else
            return mid;
    }

    if (std::get<mem>(allInfo[low]) == to_fnd)
        return low;
    if (std::get<mem>(allInfo[high]) == to_fnd)
        return high;

    return in_active(); // Not found
}

size_type price_list::interpolarSearchHRD(const CAPACITY_HRD& to_fnd) const noexcept
{
    size_type mid, low = 0, high = in_active() - 1;
    while (std::get<hrd>(allInfo[low]) < to_fnd && std::get<hrd>(allInfo[high]) > to_fnd) {
        mid = low + ((to_fnd - std::get<hrd>(allInfo[low])) * (high - low)) / (std::get<hrd>(allInfo[high]) - std::get<hrd>(allInfo[low]));

        if (std::get<hrd>(allInfo[mid]) < to_fnd)
            low = mid + 1;
        else if (std::get<hrd>(allInfo[mid]) > to_fnd)
            high = mid - 1;
        else
            return mid;
    }

    if (std::get<hrd>(allInfo[low]) == to_fnd)
        return low;
    if (std::get<hrd>(allInfo[high]) == to_fnd)
        return high;

    return in_active(); // Not found
}

size_type price_list::interpolarSearchVID(const CAPACITY_VID& to_fnd) const noexcept
{
    size_type mid, low = 0, high = in_active() - 1;
    while (std::get<vid>(allInfo[low]) < to_fnd && std::get<vid>(allInfo[high]) > to_fnd) {
        mid = low + ((to_fnd - std::get<vid>(allInfo[low])) * (high - low)) / (std::get<vid>(allInfo[high]) - std::get<vid>(allInfo[low]));

        if (std::get<vid>(allInfo[mid]) < to_fnd)
            low = mid + 1;
        else if (std::get<vid>(allInfo[mid]) > to_fnd)
            high = mid - 1;
        else
            return mid;
    }

    if (std::get<vid>(allInfo[low]) == to_fnd)
        return low;
    if (std::get<vid>(allInfo[high]) == to_fnd)
        return high;

    return in_active(); // Not found
}

size_type price_list::binarySearchID( const ID& to_fnd) const noexcept
{
    size_type left = 0, right = in_active()-1, mid;
    int t_fnd = std::stoi(to_fnd);
    while (left <= right)
        {
            mid = (right + left) / 2;
            if (std::stoi(std::get<id>(allInfo[mid])) == t_fnd) return mid;
            if (std::stoi(std::get<id>(allInfo[mid])) < t_fnd) left = mid + 1;
            else right = mid - 1;
        }
    return in_active(); // ������ ������ - ����� ��������, ��� ��� �������� � ������, ������ = �������
}

size_type price_list::binarySearchMEM_HRD_VID(const WhatToTake &key, const CAPACITY_MEM& to_fnd) const noexcept // �������� ����� �� ���������� � �������� �� ID
{
    if (key == mem)
        binarySearchMEM(to_fnd);
    if (key == hrd)
        binarySearchHRD(to_fnd);
    return binarySearchVID(to_fnd);
}

size_type price_list::interPolarSearchID(const ID& to_fnd) const noexcept // ������������� ����� �� ���������� � ��������
{
    size_type mid, low = 0, high = in_active() - 1;
    int t_fnd = std::stoi(to_fnd);
    while (std::stoi(std::get<id>(allInfo[low])) < t_fnd && std::stoi(std::get<id>(allInfo[high])) > t_fnd) {
        mid = low + ((t_fnd - std::stoi(std::get<id>(allInfo[low]))) * (high - low)) / (std::stoi(std::get<id>(allInfo[high])) - std::stoi(std::get<id>(allInfo[low])));

        if (std::stoi(std::get<id>(allInfo[mid])) < t_fnd)
            low = mid + 1;
        else if (std::stoi(std::get<id>(allInfo[mid])) > t_fnd)
            high = mid - 1;
        else
            return mid;
    }

    if (std::stoi(std::get<id>(allInfo[low])) == t_fnd)
        return low;
    if (std::stoi(std::get<id>(allInfo[high])) == t_fnd)
        return high;

    return in_active(); // Not found
}

size_type price_list::interPolarSearchMEM_HRD_VID(const WhatToTake &key, const CAPACITY_MEM& to_fnd) const noexcept // ������������� ����� �� ���������� � �������� �� MEM HRD VID
{
    if (key == mem) interpolarSearchMEM(to_fnd);
    if (key == hrd) interpolarSearchHRD(to_fnd);
    return interpolarSearchVID(to_fnd);
}

