#ifndef COMPUTER_H_INCLUDED
#define COMPUTER_H_INCLUDED

#include <tuple>
#include <string>

// �������� using � ������� ����������

enum WhatToTake{id = 0, type, mem, hrd, vid, prc, cont};

// ��� ������
using search = bool;

const bool UPPER = true;
const bool LOWER = false;

// ��������� ��� �������� ����
const uint8_t min_day_month = 1;
const uint8_t max_day = 31;
const uint8_t max_month = 12;

// ��������� ��� �����������
const float CAP_COEF = 2;

// ��� ������� ������, ��� ���������� warning'��
const size_t BARIER_ANYWAY = 1;

// �������� tuple
using ID =   std::string; // ID �������� � ������
using TYPE = std::string; // ��� ����������
using CAPACITY_MEM = uint16_t; // ����� ������
using CAPACITY_HRD = uint16_t; // ����� �������� �����
using CAPACITY_VID = uint16_t; // ����� ������ ����������
using PRICE = uint32_t; // ���� ����������
using AVAILABLE = uint16_t; // ���������� � �������

using name = std::string; // �������� �����
using date = char[9]; // ���� �������� ����� ��������

// ������� ����������
using comp = std::tuple <ID,
                         TYPE,
                         CAPACITY_MEM,
                         CAPACITY_HRD,
                         CAPACITY_VID,
                         PRICE,
                         AVAILABLE>;


#endif // COMPUTER_H_INCLUDED
