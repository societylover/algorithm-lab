#include "Container.h"
#include <vector>


Container::Container(const Container::size_type& n, const value_type &t) noexcept
{
    values = new comp[n]; // ������������ ������ ������� n
    v_size = n;  // ����� ������
    v_capacity = n; // ����� ����������� � ������ = ���������� �������
    for(size_type i = 0; i < n; values[i] = t, i++); // ���������� ��������
}
Container::Container(const std::initializer_list<value_type>& t) noexcept
{
    v_size = v_capacity = t.size(); // ����� ������
    values = new comp[v_size];// ������������ ������ ������� v_size
    size_type i = 0; // ��� ������ � values
    std::for_each(t.begin(), t.end(), [&i, this](const comp &a) // this ��� ID_CHECK
    {
        ID_check(std::get<0>(a)); // ���� �������� �������� ���������� ���, ����� ����������
        values[i] = a;
        i++;
    });
}

Container::Container(const Container& other)
{
    *this = other;
}

Container::Container(Container&& other) noexcept
{
    *this = other;
}

Container& Container::operator=(const Container& other)
{
    v_size = other.size();
    v_capacity = other.capacity();
    delete [] values;
    values = new comp[v_capacity];
    std::move(other.cbegin(), other.cend(), values);
    return *this;
}

Container& Container::operator=(Container&& other) noexcept
{
    if (v_size != 0) delete [] values; // ������ ������ ������
    v_size = other.size(); // �������� ������
    v_capacity = other.capacity(); // �������� �����������
    // �������� � �������� ������
    values = new comp[v_capacity];
    std::move(other.begin(), other.end(), values); // �������� ��������
    return *this;
}

Container::~Container() noexcept
{
    delete [] values;
    v_capacity = v_size = 0; // �������� ���
}

Container::size_type Container::size() const noexcept
{
    return v_size;
}

bool Container::empty() const noexcept
{
    return (v_size == 0);
}

Container::size_type Container::capacity() const noexcept
{
    return v_capacity;
}

Container::iterator Container::begin() noexcept
{
    return values;
}

Container::const_iterator Container::cbegin() const noexcept
{
    return values;
}

Container::iterator Container::end() noexcept
{
    return values + v_size;
}

Container::const_iterator Container::cend() const noexcept
{
    return values + v_size;
}


Container::reference Container::operator[](const size_type &i) noexcept
{
    return values[i];
}

Container::reference Container::at(const size_type &i) const
{
    if (i >= v_size) throw std::out_of_range("������ >= �������!");
    return values[i];
}

Container::const_reference Container::operator[](const size_type &i) const noexcept
{
    return values[i];
}

Container::const_reference Container::front() noexcept
{
    return values[0];
}

Container::const_reference Container::back() const noexcept
{
    return values[v_size-1];
}

Container::reference Container::back() noexcept
{
    return values[v_size-1];
}

void Container::push_back(const Container::value_type& v) noexcept
{
    if (v_size < v_capacity) // ���� ����������� �����������, �� ����� �� ����������� ������
        {
            values[v_size] = v;
            v_size++;
        }
    else  // ����� ����������� ������
        {
            if (v_capacity != 0) v_capacity *= CAP_COEF; // ����� �����������
            else v_capacity = 1; // ���� ��� ������
            comp * tmp = new comp[v_capacity];
            std::move(begin(), end(), tmp); // ���������� ������
            tmp[v_size] = v; // �������� ������
            v_size++;  // ���������� ������
            delete [] values;
            values = tmp; // ��������� ����� ������
        }
      //  std::cout << v_size <<"- in push"<<std::endl;
}

void Container::pop_back() noexcept
{
    if (v_size != 0) v_size--;
}

void Container::push_front(const Container::value_type& v) noexcept
{
    ID_check(std::get<id>(v)); // �������� ID
    if(v_size < v_capacity) // ���� ����������� �����������, �� ����� �� ����������� ������
    {
        values[v_size] = v;
        v_size++;
    }
    else // ����� ����������� ������
    {
        v_capacity *= CAP_COEF;
        comp * tmp = new comp[v_capacity];
        std::move(begin(), end(), tmp+1); // ���������� ������
        tmp[0] = v;  // ��������� �������
        v_size++;// ���������� ������
        delete [] values;
        values = tmp; // ��������� ����� ������
    }
}

void Container::pop_front() noexcept
{
    if (v_size != 0) v_size--;
    for(size_type i = 0; i < v_size; values[i] = values[i+1], i++); // ��������� �������� ������� ������ �������
}

void Container::insert(const Container::size_type &idx, const value_type& v)
{
    if (idx >= v_size) throw std::out_of_range("������ >= �������!");
    ID_check(std::get<id>(v));
    if (v_size < v_capacity) // ���� ���� �����
    {
        // ������� �������� ����� idx
        for(size_type i = v_size; i > idx; values[i] = values[i-1] ,i--); // ������������ ��������
        values[idx] = v;
        v_size++;
    }
    else // ���� ������ ����� ���������������
    {
        v_capacity *= CAP_COEF; // ����������� �����������
        comp * tmp = new comp[v_capacity];
        // �������� ������
        for (size_type i = v_size; i > idx; tmp[i] = values[i-1], i--);
        tmp[idx] = v;
        for (size_type i = 0; i < idx; tmp[i] = values[i], i++);
        // �������� ����� ������
        delete [] values;
        v_size++;
        values = tmp;
    }
}

void Container::insert(const Container::iterator &it, const value_type& v)
{
    // ���������� ������� �� ��������� � �������� ����� insert (size_type)
    iterator iter_begin = begin();
    size_type i_iter = 0;
    while(iter_begin != it)
    {
        iter_begin++;
        i_iter++;
    }
    insert(i_iter, v); // ��������� ����� insert(size_type)
}

void Container::erase(const Container::size_type &idx)
{
    if (idx >= v_size) throw std::out_of_range("����� �� �������!");
    if (idx != v_size - 1) /* ������ �������� ��� ����, ����� �� �������� �� �������� */
        for (size_type i = idx; i != v_size; values[i] = values[i+1], i++); // ������� �������� ����� �������
    v_size--;
}

void Container::erase(const Container::iterator &it)
{
    for (iterator i = it; i != end(); *i = *(i+1), i++); // ������� �������� ����� �������
    v_size--;
}

void Container::clear() noexcept
{
    // ������� ������
    delete [] values;
    values = NULL;
    // �������� ������
    v_size = v_capacity = 0;
}

void Container::swap(Container& rhs)
{
    // ����������� �� ��������� �������
    comp * a = new comp[v_capacity];
    comp * b = new comp[rhs.v_capacity];
    std::copy(begin(), end(), a);
    std::copy(rhs.begin(), rhs.end(), b);
    delete [] values; //������� ������
    delete [] rhs.values;
    // ���������� �����
    values = b;
    rhs.values = a;
    // ����� ������� � �����������
    std::swap(rhs.v_capacity, v_capacity);
    std::swap(rhs.v_size, v_size);
}
