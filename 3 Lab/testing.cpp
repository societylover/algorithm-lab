#include <sstream>
#include <vector>

#include "catch.hpp"
#include "price_list.h"

ID randID() // ��������� ID
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count()); // chrono-seed
	static std::uniform_int_distribution<int> numbr(0, 9);
	std::stringstream tmp;
	uint8_t i = 0;
	while (i < 8)
	{
		tmp << (numbr(rng)); // ���������� �� �������
		i++;
	}
	return tmp.str();
}

CAPACITY_HRD randMEM_HARD_VID() // �������� � ��� +- ����� � using ������ � ���� ��, ������� ���������
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count()); // chrono-seed
	static std::uniform_int_distribution<int> numbr(100, 2000);
	return numbr(rng);
}

PRICE rndPrice() // ��������� ��������� ����
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count()); // chrono-seed
	static std::uniform_int_distribution<int> numbr(10000, 200000);
    return numbr(rng);
}

AVAILABLE rndAvailable() // ��������� ��������� ����
{
    static std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count()); // chrono-seed
	static std::uniform_int_distribution<int> numbr(0, 20);
    return numbr(rng);
}

// ��������� ���������� �������� ���������
value_type randomValue()
{
    value_type tmp;
    tmp = std::make_tuple(randID(),"abcde",randMEM_HARD_VID(),randMEM_HARD_VID(),randMEM_HARD_VID(),rndPrice(), rndAvailable());
    return tmp;
}

size_type linearSearch(const std::vector <value_type> &A, const ID& to_fndID, const CAPACITY_MEM &cp_m, const CAPACITY_HRD &cp_h, const CAPACITY_VID &cp_v, const WhatToTake &how)
{
    if (how == UPPER)
    {
        for(size_type i = 0; i < A.size(); i++)
        {
            if (std::stoi(std::get<id>(A[i])) > std::stoi(to_fndID) && std::get<mem>(A[i]) > cp_m && std::get<hrd>(A[i]) > cp_h && std::get<vid>(A[i]) > cp_v)
                return i;
        }
    }
    else
    {
        for(size_type i = 0; i < A.size(); i++)
        {
            if (std::stoi(std::get<id>(A[i])) < std::stoi(to_fndID) && std::get<mem>(A[i]) < cp_m && std::get<hrd>(A[i]) < cp_h && std::get<vid>(A[i]) < cp_v)
                return i;
        }
    }
    return A.size();
}

void sort(std::vector <value_type> &A, const WhatToTake& key_sort)
{
    size_type min; // ��� ������ ������������ �������� � ��� ������
        for (size_type i = 0; i < A.size() - 1; i++)
        {
            size_type fst,scnd; // �������� ��������� ����
            min = i; // ���������� ������ �������� ��������
            // ���� ����������� ������� ����� ��������� �� ����� i-���
        for (size_type j = i + 1; j < A.size(); j++)  // ��� ��������� ��������� ����� i-���
        {
            // ������ ���������� ? ���� �� ID, �� ������� ��� ��������� ��� ���������� ��������� ��� � �����
            if (key_sort == id)
            {
                fst = std::stoi(std::get<id>(A[j]));
                scnd = std::stoi(std::get<id>(A[min]));
            }
            else // ����� �� ����� �����
            {
                // ������������ std::get<key_sort>(allinfo[j]) ������, �.�. key_sort - is not constant expression
                if (key_sort == mem){
                fst = std::get<mem>(A[j]);
                scnd = std::get<mem>(A[min]);}
                if (key_sort == hrd){
                fst = std::get<hrd>(A[j]);
                scnd = std::get<hrd>(A[min]);}
                if (key_sort == vid){
                fst = std::get<vid>(A[j]);
                scnd = std::get<vid>(A[min]);}
            } // ���� ������� ������ ������������,
             if (fst < scnd) // ���� ������� ������ ������������,
                min = j;       // ���������� ��� ������ � min
        }
        std::swap(A[i],A[min]); // ����� �������� �������� � ����������� ������
        }
}

size_type binarySearch(const std::vector <value_type> &A, const ID& to_fndID, const CAPACITY_MEM &cp_m, const CAPACITY_HRD &cp_h, const CAPACITY_VID &cp_v, const WhatToTake &how)
{
    size_type left = 0, right = A.size();
    if (how == UPPER){
    while (left <= right)
        {
            size_type mid = (right+left) / 2;
            if (std::stoi(std::get<id>(A[mid])) > std::stoi(to_fndID) && std::get<mem>(A[mid]) > cp_m && std::get<hrd>(A[mid]) > cp_h && std::get<vid>(A[mid]) > cp_v)
                    return mid;
            else if (std::stoi(std::get<id>(A[mid])) == std::stoi(to_fndID) && std::get<mem>(A[mid]) == cp_m && std::get<hrd>(A[mid]) == cp_h && std::get<vid>(A[mid]) == cp_v)
                {
                left = mid + 1;
            } else right = mid - 1;
        }}
    else {
        while (left <= right)
        {
            size_type mid = (right+left) / 2;
            if (std::stoi(std::get<id>(A[mid])) < std::stoi(to_fndID) && std::get<mem>(A[mid]) < cp_m && std::get<hrd>(A[mid]) < cp_h && std::get<vid>(A[mid]) < cp_v)
                    return mid;
            else if (std::stoi(std::get<id>(A[mid])) == std::stoi(to_fndID) && std::get<mem>(A[mid]) == cp_m && std::get<hrd>(A[mid]) == cp_h && std::get<vid>(A[mid]) == cp_v)
                {
            right = mid - 1;
            } else left = mid + 1;
        }
    }
    return A.size();
}

size_type interpolarSearch(const std::vector <value_type> &A, const ID& to_fndID, const CAPACITY_MEM &cp_m, const CAPACITY_HRD &cp_h, const CAPACITY_VID &cp_v, const WhatToTake &how)
{
    size_type mid, left = 0, right = A.size()-1;
    while (true)
    {
        mid = left + ((std::stoi(to_fndID) - std::stoi(std::get<id>(A[left])))*(right-left))/(std::stoi(std::get<id>(A[right]))-std::stoi(std::get<id>(A[left])));
        if (std::stoi(std::get<id>(A[mid])) < std::stoi(to_fndID))
            left = mid + 1;
        else if (std::stoi(std::get<id>(A[mid])) > std::stoi(to_fndID))
            right = mid - 1;
        else
            return mid;
    }
    //if (std::stoi(std::get<id>(A[left])) == std::stoi(to_find))
     //   return left;
   // if (std::stoi(std::get<id>(A[right])) == std::stoi(to_find))
      //  return right;

    return A.size(); // ���� �� �������
}

TEST_CASE("������������ ������ PriceList ")
{
    SECTION("�������� ��������� ������")
    {
        price_list b("eldorado","02032010");
        price_list c("citilink","10102015");

        price_list d("megafon", "01012020", {
                     std::make_tuple("12345678","type1",1,2,3,4,5),
                     std::make_tuple("11111111","type2",4,3,2,1,0),
                     std::make_tuple("10101010","type3",1,1,1,1,1),
                     }); // c initializer_list
        price_list e("svyaznoi", "14052010", {
                     std::make_tuple("12345678","type1",1,2,3,4,5),
                     std::make_tuple("11111111","type2",4,3,2,1,0),
                     std::make_tuple("10101010","type3",1,1,1,1,1),
                     }); // c initializer_list

        CHECK(std::get<id>(d.get(0)) == "12345678");
        CHECK(std::get<type>(d.get(1)) == "type2");
        CHECK(std::get<id>(e.get(1)) == "11111111");
        CHECK(std::get<mem>(e.get(2)) == 1);
    }

    SECTION("���������� ��������")
    {
        price_list a("m.video", "01022020");
        a.add(std::make_tuple("12345678","type1",1,2,3,4,5));
        a.add(std::make_tuple("11111111","type2",1,2,3,4,5));
        a.add(std::make_tuple("55555555","type3",1,2,3,4,5));
        a.add(std::make_tuple("11111","type2",1,2,3,4,5));
        a.add(std::make_tuple("22323","type2",1,2,3,4,5));

        CHECK(std::get<id>(a.get(0)) == "12345678");
        CHECK(std::get<type>(a.get(1)) == "type2");
        CHECK(std::get<id>(a.get(2)) == "55555555");
        CHECK(std::get<vid>(a.get(3)) == 3);
        CHECK(std::get<prc>(a.get(4)) == 4);
    }

    SECTION("������ ��������")
    {
        price_list b("eldorado","01012020");
        b.add(std::make_tuple("22323","type2",1,2,3,4,5));
        b.change(0, std::make_tuple("1111","type1",6,6,6,6,6));

        CHECK(std::get<id>(b.get(0)) == "1111");
        CHECK(std::get<type>(b.get(0)) == "type1");
        CHECK(std::get<mem>(b.get(0)) == 6);
        CHECK(std::get<hrd>(b.get(0)) == 6);
        CHECK(std::get<vid>(b.get(0)) == 6);
        CHECK(std::get<prc>(b.get(0)) == 6);
        CHECK(std::get<cont>(b.get(0)) == 6);
    }
    SECTION("�������� ��������")
    {
        price_list c("media markt","05052020");
        c.add(std::make_tuple("22323","type2",1,2,3,4,5));
        c.add(std::make_tuple("11111","type3",1,2,3,4,5));

        CHECK(c.in_active() == 2);
        c.delete_var(1);
        CHECK(c.in_active() == 1);
        c.delete_var(0);
        CHECK(c.in_active() == 0);
    }
    SECTION("�������� ������� ������� �������")
    {
        price_list a("m.video","01012010");

        // ��������� ��������� ��������
        a.add(randomValue());
        a.add(randomValue());
        a.add(randomValue());
        a.add(randomValue());

        SECTION("1. ����������� �������� ����������������� ������ � �������� �� ���������������� ����������")
        {
            // ������� ������ � �����-����
            a.add(std::make_tuple("10101","type1",1,2,3,4,5));

            // ����� ID = 10101, �.�. ������ � ����� - ������� ������� ������� � ������ ����������
            CHECK(a.linearSearchUnsortedBarier(id) == 4); // ��������������� ������ ID �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchUnsortedBarier(id) != 4);

            // ����� �� ������, ����� ��, ������ 100 �������� �� ��������������
            CHECK(a.linearSearchUnsortedBarier(mem) == 4);
            CHECK_FALSE(a.linearSearchUnsortedBarier(mem) != 4);

            // ����� �� �����, ����� ��, ������ 100 �������� �� ��������������
            CHECK(a.linearSearchUnsortedBarier(hrd) == 4);
            CHECK_FALSE(a.linearSearchUnsortedBarier(hrd) != 4);

            // ����� �� �����������, ����� ��, ������ 100 �������� �� ��������������
            CHECK(a.linearSearchUnsortedBarier(vid) == 4);
            CHECK_FALSE(a.linearSearchUnsortedBarier(vid) != 4);
            CHECK_FALSE(a.linearSearchUnsortedBarier(vid) == 0);
            CHECK_FALSE(a.linearSearchUnsortedBarier(vid) == 1);
            CHECK_FALSE(a.linearSearchUnsortedBarier(vid) == 2);
            CHECK_FALSE(a.linearSearchUnsortedBarier(vid) == 3);
        }

        SECTION("2. ����������� �������� ����������������� ������ � �������� �� �������������� ����������")
        {
            a.sort(id, UPPER); // �� ����������� ID
            // ������� ������ � �����-����
            a.add(std::make_tuple("99999999","type1",1,2,3,4,5));

            // ����� ID = 99999999, �.�. ������ � ����� - ������� ������� ������� � ������ ����������
            CHECK(a.linearSearchSortedBarier(id, UPPER) == 4); // ��������������� ������ ID �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(id, UPPER) != 4);

            // ������ ������ � �������������
            a.delete_var(4);

            a.sort(id, LOWER);
            // ������� ������ � �����-����
            a.add(std::make_tuple("1","type1",1,2,3,4,5));

            CHECK(a.linearSearchSortedBarier(id, LOWER) == 4); // ��������������� ������ ID �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(id, LOWER) != 4);

            // ������ ������ � �������������
            a.delete_var(4);

            a.sort(mem, UPPER); // �� ����������� ������
            // ������� ������ � �����-����
            a.add(std::make_tuple("90999","type1",2500,2,3,4,5)); // ������� � 2500 ����������� ������

            CHECK(a.linearSearchSortedBarier(mem, UPPER) == 4); // ��������������� ������ mem �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(mem, UPPER) != 4);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, UPPER) == 0);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, UPPER) == 1);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, UPPER) == 2);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, UPPER) == 3);

            // ������ ������ � �������������
            a.delete_var(4);

            a.sort(mem, LOWER); // �� ����������� ������
            // ������� ������ � �����-����
            a.add(std::make_tuple("90999","type1",70,2,3,4,5)); // ������� � 70 ����������� ������

            CHECK(a.linearSearchSortedBarier(mem, LOWER) == 4); // ��������������� ������ mem �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(mem, LOWER) != 4);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, LOWER) == 0);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, LOWER) == 1);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, LOWER) == 2);
            CHECK_FALSE(a.linearSearchSortedBarier(mem, LOWER) == 3);

            // ������ ������ � �������������
            a.delete_var(4);

            a.sort(hrd, UPPER); // �� ����������� �����
            // ������� ������ � �����-����
            a.add(std::make_tuple("90999","type1",1,2500,3,4,5)); // ������� � 2500 ���� ������

            CHECK(a.linearSearchSortedBarier(hrd, UPPER) == 4); // ��������������� ������ hrd �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, UPPER) != 4);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, UPPER) == 0);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, UPPER) == 1);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, UPPER) == 2);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, UPPER) == 3);

            // ������ ������ � �������������
            a.delete_var(4);

            a.sort(hrd, LOWER); // �� ����������� ������
            // ������� ������ � �����-����
            a.add(std::make_tuple("90999","type1",1,70,3,4,5)); // ������� � 70 �������� ������

            CHECK(a.linearSearchSortedBarier(hrd, LOWER) == 4); // ��������������� ������ hrd �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, LOWER) != 4);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, LOWER) == 0);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, LOWER) == 1);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, LOWER) == 2);
            CHECK_FALSE(a.linearSearchSortedBarier(hrd, LOWER) == 3);

             // ������ ������ � �������������
            a.delete_var(4);

            a.sort(vid, UPPER); // �� ����������� �����
            // ������� ������ � �����-����
            a.add(std::make_tuple("90999","type1",1,2,3000,4,5)); // ������� � 3000 ����� ������

            CHECK(a.linearSearchSortedBarier(vid, UPPER) == 4); // ��������������� ������ vid �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(vid, UPPER) != 4);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, UPPER) == 0);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, UPPER) == 1);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, UPPER) == 2);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, UPPER) == 3);

             // ������ ������ � �������������
            a.delete_var(4);

            a.sort(vid, LOWER); // �� ����������� ������
            // ������� ������ � �����-����
            a.add(std::make_tuple("90999","type1",1,2,70,4,5)); // ������� � 70 ����� ������

            CHECK(a.linearSearchSortedBarier(vid, LOWER) == 4); // ��������������� ������ vid �� �����, ������� ������� ������� ���������
            CHECK_FALSE(a.linearSearchSortedBarier(vid, LOWER) != 4);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, LOWER) == 0);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, LOWER) == 1);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, LOWER) == 2);
            CHECK_FALSE(a.linearSearchSortedBarier(vid, LOWER) == 3);
        }
        SECTION("3. ����������� �������� ����� � ���������������� ����� �� �������������� ����������")
        {
            a.sort(id, UPPER); // �� ����������� ID
            CHECK(a.binarySearchID("99999999") == 4); // ����� ������� ID �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.binarySearchID("99999999") == 0);
            CHECK_FALSE(a.binarySearchID("99999999") == 1);
            CHECK_FALSE(a.binarySearchID("99999999") == 2);
            CHECK_FALSE(a.binarySearchID("99999999") == 3);

            a.sort(mem, UPPER); // �� ����������� mem
            CHECK(a.binarySearchMEM_HRD_VID(mem, 3000) == 4); // ����� ������� mem �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(mem,3000) == 0);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(mem,3000) == 1);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(mem,3000) == 2);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(mem,3000) == 3);

            a.sort(hrd, UPPER); // �� ����������� hrd
            CHECK(a.binarySearchMEM_HRD_VID(hrd, 2500) == 4); // ����� ������� hrd �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(hrd, 2500) == 0);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(hrd, 2500) == 1);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(hrd, 2500) == 2);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(hrd, 2500) == 3);

            a.sort(vid, UPPER); // �� ����������� vid
            CHECK(a.binarySearchMEM_HRD_VID(vid, 2500) == 4); // ����� ������� vid �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(vid, 2500) == 0);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(vid, 2500) == 1);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(vid, 2500) == 2);
            CHECK_FALSE(a.binarySearchMEM_HRD_VID(vid, 2500) == 3);

            // ���������������� �����
            a.sort(id, UPPER); // �� ����������� ID
            CHECK(a.interPolarSearchID("99999999") == 4); // ����� ������� ID �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.interPolarSearchID("99999999") == 0);
            CHECK_FALSE(a.interPolarSearchID("99999999") == 1);
            CHECK_FALSE(a.interPolarSearchID("99999999") == 2);
            CHECK_FALSE(a.interPolarSearchID("99999999") == 3);

            a.sort(mem, UPPER); // �� ����������� mem
            CHECK(a.interPolarSearchMEM_HRD_VID(mem, 3000) == 4); // ����� ������� mem �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(mem,3000) == 0);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(mem,3000) == 1);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(mem,3000) == 2);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(mem,3000) == 3);

            a.sort(hrd, UPPER); // �� ����������� hrd
            CHECK(a.interPolarSearchMEM_HRD_VID(hrd, 2500) == 4); // ����� ������� hrd �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(hrd, 2500) == 0);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(hrd, 2500) == 1);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(hrd, 2500) == 2);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(hrd, 2500) == 3);

            a.sort(vid, UPPER); // �� ����������� vid
            CHECK(a.interPolarSearchMEM_HRD_VID(vid, 2500) == 4); // ����� ������� vid �� �������������, ������ ����������� ����� ������ ����������
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(vid, 2500) == 0);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(vid, 2500) == 1);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(vid, 2500) == 2);
            CHECK_FALSE(a.interPolarSearchMEM_HRD_VID(vid, 2500) == 3);
        }
        SECTION("������ 2 ������ �������������� ������-���������� ������������ ����������� ��������� vector.")
        {
            //��������� ��������� ���������� �������.
            std::vector <value_type> a;
            a.push_back(randomValue());
            a.push_back(randomValue());
            a.push_back(randomValue());
            a.push_back(randomValue());
            SECTION("1. ����������� �������� ����������������� ������ � �������� �� ���������������� ����������")
            {
                // ��������� ������
                a.push_back(std::make_tuple("101010","type1",1,2,3,4,5));



            }


        }
    }
}


