#ifndef PRICE_LIST_H
#define PRICE_LIST_H

#include <iostream>

#include "Container.h"

using value_type = Container::value_type;
using size_type =  Container::size_type;

class price_list
{
    Container allInfo; // ��� ��������� � �������
    date create; // ���� ��������
    name firm_name; // ��� �����
    // ���������� ������� - v_size �� Container
    // �������� �� ������������ ��������� ����
    void date_check(const date &created) const;

    // �������� ID �� ������������
    void IDAlreadyIn(const value_type& tmp);
    // ���������� �� �����, ��� how - boolean, true(UPPER) = � ������� �����������, false (LOWER) = � ������� ��������
    void ID_sort(const search &how) noexcept;
    void MEM_HRD_VID_sort(const WhatToTake &key_sort, const search &how) noexcept; // ���� ����� ��

    // �������� ������ � �������� � ��������������� �������
    size_type linearSearchIDUnsorted() const noexcept;
    size_type linearSearchMEMUnsorted() const noexcept;
    size_type linearSearchHRDUnsorted() const noexcept;
    size_type linearSearchVIDUnsorted() const noexcept;

    // �������� ������ � �������� � c������������ �������
    size_type linearSearchIDSorted(const search &how_sorted) const noexcept;
    size_type linearSearchMEMSorted(const search &how_sorted) const noexcept;
    size_type linearSearchHRDSorted(const search &how_sorted) const noexcept;
    size_type linearSearchVIDSorted(const search &how_sorted) const noexcept;

    // �������� ����� �� �����
    size_type binarySearchMEM(const CAPACITY_MEM& to_fnd) const noexcept;
    size_type binarySearchHRD(const CAPACITY_HRD& to_fnd) const noexcept;
    size_type binarySearchVID(const CAPACITY_VID& to_fnd) const noexcept;

    // ������������� ������ �� �����
    size_type interpolarSearchMEM(const CAPACITY_MEM& to_fnd) const noexcept;
    size_type interpolarSearchHRD(const CAPACITY_HRD& to_fnd) const noexcept;
    size_type interpolarSearchVID(const CAPACITY_VID& to_fnd) const noexcept;

    public:
    // �����������
    explicit price_list(const name &f_name, const date &created) noexcept;
    price_list(const name &f_name, const date &created, const std::initializer_list<value_type>& t);

    // ������ ����������, ���������, ��������
    void add(const value_type &a) noexcept;
    void change(const size_type &i, const value_type &tmp) noexcept;
    void delete_var(const size_type &i) noexcept;

    // ��������� ��������
    value_type& get(const size_type &i) const noexcept;

    // ��� ��������� � �������
    size_type in_active() const noexcept;

    // ���������� �������
    void sort(const WhatToTake& key_sort, const search &how) noexcept;

    // �����, ������� ����� �������� ��������������� ����� �� ���������������� ����������
    size_type linearSearchUnsortedBarier(const WhatToTake &key) const noexcept;
    //                                                     �� �������������� ����������
    size_type linearSearchSortedBarier(const WhatToTake &key, const search &how_sorted) const noexcept;

    size_type binarySearchID(const ID& to_fnd) const noexcept; // �������� ����� �� ���������� � �������� �� ID
    size_type binarySearchMEM_HRD_VID(const WhatToTake &key, const CAPACITY_MEM& to_fnd) const noexcept; // �������� ����� �� ���������� � �������� �� MEM HRD VID

    size_type interPolarSearchID(const ID& to_fnd) const noexcept; // ������������� ����� �� ���������� � ��������
    size_type interPolarSearchMEM_HRD_VID(const WhatToTake &key, const CAPACITY_MEM& to_fnd) const noexcept; // ������������� ����� �� ���������� � �������� �� MEM HRD VID

};

#endif // PRICE_LIST_H
