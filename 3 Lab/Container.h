#ifndef Container_H
#define Container_H

#include <exception> // ��� throw
#include <cstring> // strcpy

#include <algorithm> // std::algo's

#include "COMPUTER.h" // using � tuple-�������

class Container
{
    comp * values = NULL; // ����������� ��������� �� ������
    size_t v_size = 0;  // ���������� ���������
    size_t v_capacity = 0; // �����������

    public:
    // �������� ID
    void ID_check(const ID &ids) const
    {
        if (ids.length() > 8) throw std::range_error("������� ID!");
    }
  // -- ���� --
    using size_type = size_t;
    using value_type = comp;        		  //-- �������� ��� ��������� --
    using iterator = value_type*;             // -- ��� ������ ��������� --
    using const_iterator = const value_type*; // -- ����� ��������� �� ��������� --
    using reference = value_type& ;
    using const_reference = const value_type& ;
  // -- ������������ � ������������ --
    Container(){}; // �� ��������� ��� ���������� ��� ��� � ��������
    explicit Container(/*const name &f_name, const date &created, */ const size_type& n,const value_type &t = value_type()) noexcept;
    Container(/*const name& f_name, const date& created, */const std::initializer_list<value_type> &t) noexcept;
    Container(const Container& other);
    Container(Container&& other) noexcept;              // -- ����������� �������� --
    Container& operator=(const Container& other);
    Container& operator=(Container&& other) noexcept;   // -- �������� ����������� --
    virtual ~Container() noexcept;
  // -- ������� --
    size_type size() const noexcept;             // ������� ���������� ���������
    bool empty() const noexcept;                 // ���� �� ��������
    size_type capacity() const noexcept;         // ������������� ������
  // -- ��������� -- ��� ������ ��������� --
  // -- �������� � ����������� - ��� ����������� �������� � ����������� --
  // -- ������ �� ������������ �������� ����������� --
    iterator begin() noexcept;
    const_iterator cbegin() const noexcept;
    iterator end() noexcept;
    const_iterator cend() const noexcept;
  // -- ������ � ��������� --
    reference operator[](const size_type &i) noexcept;
    reference at(const size_type &i) const; // ������������ �������� �� ����� �� ������� - throw
    const_reference operator[](const size_type &i) const noexcept;
    reference front() const noexcept;
    const_reference front() noexcept;
    reference back() noexcept;
    const_reference back() const noexcept;
  // -- ������-������������
    void push_back(const value_type& v) noexcept;             // -- �������� ������� � ����� --
    void pop_back() noexcept;                        // ������� ��������� �������
    void push_front(const value_type& v) noexcept;            // -- �������� ������� � ������ --
    void pop_front() noexcept;                       // ������� ������ �������
    void insert(const size_type &idx, const value_type& v);			 // -- �������� ������� ����� ��������� � �������� idx
    void insert(const iterator &it, const value_type& v);			 	 // -- �������� ������� ����� ��������� it
    void erase(const size_type &idx); 			     // -- ������� ������� idx
    void erase(const iterator &it);	 			 // -- ������� ������� it
    void clear() noexcept;    	     		 // �������� ������
    void swap(Container& rhs);              // -- ����� �������� - ���� ����������� --
};

#endif // Container_H
