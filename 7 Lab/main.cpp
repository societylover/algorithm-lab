 #define CATCH_CONFIG_RUNNER
 #include "catch.hpp"

#include "PriceList.h"

int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "Russian");			// -- ������� ������� --
    std::cout << "������������ 7, ������� 7. �������� �.�., ����� - 21"<<std::endl;
    std::cout << "7.	�����-���� ������������ ����� �������� � ���� ������ ������� \n"
    "����������� �����������. ���� ������� ������ �������� ����������������� ����� \n"
    "(8 ��������), ��� ����������, ����� ������, ����� ������� �����, ����� ������ \n"
    "����������, ���� ���������� � �������� �������� � ���������� �����������, ���������\n"
    "� �������. �����-���� �������� �������� �����, ���� ��������, � ���������� �������. \n"
    "� ������ PriceList ����������� ������ ����������, ��������� � �������� ������ � ������.\n" <<std::endl;
    std::cout << "�������: \n"
    " �� ���� ��������� ����������� ��������� ���������: \n"
    " � ��� �������� ������ ������ � ������� ������ � ��������� ����; \n"
    " ����������� ������ ����� � ����������� � ����������� ����; \n"
    " � ����� ������������� � ������������ �����; \n"
    " � ����������� ���� ��������; "<<std::endl;

  int result = Catch::Session().run( argc, argv );	// -- ���� ������� Catch --
  system("pause");			// -- ��� ��������� ����������� --
  return result;
}

