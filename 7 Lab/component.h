#ifndef COMPONENT_H_INCLUDED
#define COMPONENT_H_INCLUDED

#include <tuple>
#include <cstring>
#include <algorithm>
#include <iostream>

// �������� ������� std::tuple, ����������� using-������ �������

// ���������, � ������� ���������� ������ � id, � ������ ��� ���������� ����������� ������������� char[] � tuple � ����������� ��������� = ��� char*, ������� �� ����������
struct ID_ARRAY
{
    char id[9];
    ID_ARRAY(const char B[9]){ std::strcpy(id, B);}; // �����������
    ID_ARRAY& operator=(const ID_ARRAY &B) // ��� ����������� ���������� ��������
    {
       std::strcpy(id, B.id);
       return *this;
    }
    // ��������� ��������� ����������� ��� ��������� ��������
    bool operator==(const ID_ARRAY &B) const
    {
        return (std::strcmp(id, B.id) == 0);
    }

    bool operator!=(const ID_ARRAY &B) const
    {
        return !(*this == B);
    }
};

using ID   = ID_ARRAY;
using TYPE = std::string;
using CAPACITY_M =  unsigned short;    // ����� ������
using CAPACITY_H =  unsigned short;    // ����� �������� �����
using CAPACITY_C =  unsigned short;    // ����� ������ ����������
// 3 ������, ������� �������� ������ � ���� �� ��� ����, ����� � tuple ����� ���� �� ��������
using PRICE =     unsigned short;    // ���� � �.�
using AVAILABLE = unsigned short;    // ���������� � �������
using DAY = uint8_t;    // ����
using MONTH = uint8_t;  // �����
using YEAR = uint16_t;  // ���

using NAME = std::string;   // �������� �����
using CONTAIN = unsigned;   // ���������� ������� � �������

// ������ � ������� � ����������
using computer = std::tuple <   TYPE,
                                CAPACITY_M,
                                CAPACITY_H,
                                CAPACITY_C,
                                PRICE,
                                AVAILABLE>;

// ��� ���������� �� ���������
const computer computer_template(std::make_tuple("0",0,0,0,0,0));

// ������ � �����
using DATE = std::tuple <       DAY,    // 1 +
                                MONTH,  // 1 +
                                YEAR>;  // 2  = 4
// ��������� ����
const DAY max_day = 31, min_day = 1;
const MONTH max_month = 12, min_month = 1;

// ��� ��������� �� 0 � 1, � ���� � �����
enum date_indexex {day = 0, month = 1};

// ��� ��������� ����
const DATE date_template(std::make_tuple(1,1,2001));

#endif // COMPONENT_H_INCLUDED
