#include "RBTree.h"
#include <stack>
#include <algorithm>

#define key_val(element) std::get<0>(element->value)

RBTree::RBTree(const RBTree& other)
{
    std::stack<PNode> stack; // ��� ������ ���������
    PNode current = other.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            insert(current->value); // ���������� �������� � ������
            current = current->left;
        }
        current = stack.top()->right;
        stack.pop();
    }
}

RBTree::PNode RBTree::node_t::uncle() const
{
    if (!parent|| !parent->parent)  return nullptr;
    if (parent->isOnLeft())  return parent->parent->right;
    else return parent->parent->left;
}

bool RBTree::node_t::isOnLeft() const
{
    return (this == parent->left);
}

RBTree::PNode RBTree::node_t::sibling() const
{
    if (!parent) return nullptr;
    if (isOnLeft())  return parent->right;
    return parent->left;
}

void RBTree::node_t::move_down(PNode nParent)
{
    if (parent) {
      if (isOnLeft()) {
        parent->left = nParent;
      } else {
        parent->right = nParent;
      }
    }
    nParent->parent = parent;
    parent = nParent;
}

bool RBTree::node_t::has_red_child() const
{
    return (left && left->cvet == red) || (right && right->cvet == red);
}

void RBTree::leftRotate(PNode x)
{
    PNode nParent = x->right;

    if (x == root) root = nParent;

    x->move_down(nParent);

    x->right = nParent->left;

    if (nParent->left)  nParent->left->parent = x;

    nParent->left = x;
}

void RBTree::rightRotate(PNode x)
{
    PNode nParent = x->left;

    if (x == root) root = nParent;

    x->move_down(nParent);

    x->left = nParent->right;

    if (nParent->right)   nParent->right->parent = x;

    nParent->right = x;
}

void RBTree::fixRedRed(PNode x)
{
    if (x == root) {
      x->cvet = black;
      return;
    }

    PNode parent = x->parent, grandparent = parent->parent,
         uncle = x->uncle();

    if (parent->cvet != black) {
      if (uncle != nullptr && uncle->cvet == red) {

        parent->cvet = uncle->cvet = black;
        grandparent->cvet = red;
        fixRedRed(grandparent);
      } else {

        if (parent->isOnLeft()) {
          if (x->isOnLeft()) {

            std::swap(parent->cvet, grandparent->cvet);
          } else {
            leftRotate(parent);
            std::swap(x->cvet, grandparent->cvet);
          }

          rightRotate(grandparent);
        } else {
          if (x->isOnLeft()) {

            rightRotate(parent);
            std::swap(x->cvet, grandparent->cvet);
          } else {
              std::swap(parent->cvet, grandparent->cvet);
          }

          leftRotate(grandparent);
        }
      }
    }
}

RBTree::PNode RBTree::successor(PNode x)
{
    PNode temp = x;

    while (temp->left) temp = temp->left;

    return temp;
}

RBTree::PNode RBTree::tree_replace(PNode x)
{
    if (x->left && x->right)  return successor(x->right);

    if (x->left == nullptr && x->right == nullptr) return nullptr;

    if (x->left != nullptr)  return x->left;
    else return x->right;
}

void RBTree::fixDoubleBlack(PNode x)
{
    if (x == root) return;

    PNode sibling = x->sibling(), parent = x->parent;
    if (sibling == NULL)
    {
        fixDoubleBlack(parent);
    }
    else
    {
        if (sibling->cvet == red)
        {
            parent->cvet = red;
            sibling->cvet = black;
            if (sibling->isOnLeft()) rightRotate(parent);
            else leftRotate(parent);

            fixDoubleBlack(x);
        }
        else
        {
            if (sibling->has_red_child())
            {
                if (sibling->left != nullptr && sibling->left->cvet == red)
                {
                    if (sibling->isOnLeft())
                    {
                        sibling->left->cvet = sibling->cvet;
                        sibling->cvet = parent->cvet;
                        rightRotate(parent);
                    }
                    else
                    {
                        sibling->left->cvet = parent->cvet;
                        rightRotate(sibling);
                        leftRotate(parent);
                    }
                }
                else
                {
                    if (sibling->isOnLeft())
                    {
                        sibling->right->cvet = parent->cvet;
                        leftRotate(sibling);
                        rightRotate(parent);
                    }
                    else
                    {
                        sibling->right->cvet = sibling->cvet;
                        sibling->cvet = parent->cvet;
                        leftRotate(parent);
                    }
                }
                parent->cvet = black;
            }
            else
            {
                sibling->cvet = red;
                if (parent->cvet == black)
                    fixDoubleBlack(parent);
                else
                    parent->cvet = black;
            }
        }
    }
}

void RBTree::deleteNode(PNode v)
{
    PNode u = tree_replace(v);

    bool uvBlack = ((u == nullptr || u->cvet == black) && (v->cvet == black));
    PNode parent = v->parent;

    if (u == nullptr) {

      if (v == root) root = nullptr;
      else {
        if (uvBlack)fixDoubleBlack(v);
        else if (v->sibling() != nullptr) v->sibling()->cvet = red;
        if (v->isOnLeft()) parent->left = nullptr;
        else parent->right = nullptr;
      }
      delete v;
      _size--;
      return ;
    }
    if (v->left == nullptr && v->right == nullptr) {
      if (v == root) {

        v->value = u->value;
        v->left = v->right = nullptr;
        _size--;
        delete u;
      } else {

        if (v->isOnLeft()) parent->left = u;
        else parent->right = u;
        _size--;
        delete v;
        u->parent = parent;
        if (uvBlack) fixDoubleBlack(u);
        else u->cvet = black;
      }
      return ;
    }
    std::swap(u->value, v->value);
    deleteNode(u);
}

RBTree::PNode RBTree::search(key_t n)
{
    PNode temp = root;
    while (temp != nullptr) {
      if (n < key_val(temp)) {
        if (temp->left == nullptr)
          break;
        else temp = temp->left;
      } else if (n == key_val(temp)) {
        break;
      } else {
        if (temp->right == nullptr)
          break;
        else temp = temp->right;
      }
    }
    return temp;
  }


bool RBTree::insert_node(data_t n)
{
    PNode newNode = new node_t(n);
    if (root == NULL)
    {
        newNode->cvet = black;
        root = newNode;
    }
    else
    {
        PNode temp = search(std::get<0>(n));

        if (key_val(temp) == std::get<0>(n))
        {
            return false;
        }

        newNode->parent = temp;

        if (std::get<0>(n) < key_val(temp)) temp->left = newNode;
        else temp->right = newNode;

        fixRedRed(newNode);
    }
    _size++;
    return true;
}

RBTree::~RBTree()
{
    clear();
}

RBTree::RBTree(const data_t& d)
{
    insert_node(d);
}

bool RBTree::insert(const key_t& key, const value_type& v)
{
    data_t val = std::make_tuple(key,v);
    return insert(val);
}


RBTree::RBTree(RBTree&& other) noexcept
{
    std::swap(root, other.root);
    std::swap(_size, other._size);
}

RBTree::RBTree(const std::initializer_list<data_t> &t)
{
    for(auto i = t.begin(); i != t.end(); i++) insert(*i);
}

RBTree& RBTree::operator=(RBTree&& other) noexcept
{
    clear();
    std::swap(root,  other.root);
    std::swap(_size, other._size);
    return *this;
}

RBTree& RBTree::operator=(const RBTree& other)
{
    if (&other != this)
    {
    std::stack<PNode> stack; // ��� ������ ���������
    PNode current = other.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            insert(current->value); // ���������� �������� � ������
            current = current->left;
        }
        current = stack.top()->right;
        stack.pop();
    }
    }
    return *this;
}

bool RBTree::empty() const noexcept
{
    return !(root);
}

size_t RBTree::count() const noexcept
{
    return _size;
}

// ����������� ���������� ������ ������
size_t RBTree::recursive_height(const node_t *tmp) const noexcept
{
    return tmp == nullptr ? 0 : (1 + std::max(recursive_height(tmp->left), recursive_height(tmp->right)));
}

size_t RBTree::height() const noexcept
{
    return recursive_height(root);
}

RBTree::data_t RBTree::find(const key_t& key) const
{
    if (!root) throw std::logic_error("���������� ����� ������� � ������ ������!");
    bool finded = false;
    PNode current = root;
    while (current && !finded)
    {
        if (key_val(current) > key) current = current->left;
        else if (key_val(current) < key) current = current->right;
        else finded = true;
    }
    if (current) return current->value;
    return root->value;
}

bool RBTree::insert(const data_t& d)
{
    return insert_node(d);
}

bool RBTree::replace(const key_t& key, const value_type& v)
{
    PNode value = search(key);
    if (key_val(value) == key)
    {
        value->value = std::make_tuple(key, v);
        return true;
    }
    return false;
}

bool RBTree::erase(const key_t& key)
{
    size_t i = _size;
    PNode fnd = search(key);
    if (fnd) deleteNode(fnd);
    return (i == _size);
}

void RBTree::clear()
{
    while (_size != 0) erase(key_val(root));
}

void RBTree::swap(RBTree& t) noexcept
{
    std::swap(root, t.root);
    std::swap(_size, t._size);
}


// ����������� ��������, ������ ������ ���������� ������
void RBTree::concat(RBTree &t)
{
    if(t.root == root) throw std::logic_error("����������� �������� ���������� � ����� �����!");// ���� �� t != �������� ���������, �� ����������
    while (t.root) // ���� � t ���� �������� ����� �� ����� � �������
        {
            insert(t.root->value);
            t.erase(std::get<0>(t.root->value));
        }
    // �� ������ this �� ������ ���������� + �������� t, � t ����
}

// ����� ������������ ��������
RBTree::data_t find_min(const RBTree &A)
{
    // ����������� ������ � �������� ������ ������ � ������ ����� ����
    if (A.count() == 0) throw std::length_error("������ ������, ����������� ������� �����������!"); // ���� ��������� � ������ ���
    RBTree::PNode current = A.root;
    while (current->left)  current = current->left; // ��������� �����, ���� ����� ���� �������
    return current->value; // ������ ��������� �������
}
// ����� �������� � ������������ ������
RBTree::data_t find_max(const RBTree &A)
{
// ����������� ������ � �������� ������ ������ � ������ ������ ����
    if(A.count() == 0) throw std::length_error("������ ������, ������������ ������� �����������!"); // ���� ��������� � ������ ���
    RBTree::PNode current = A.root;
    while (current->right)  current = current->right; // ��������� �����, ���� ����� ���� �������
    return current->value; // ������ ��������� �������

}

#define get(num, element) std::get<num>(element)

#include <sstream>

std::string elementValue(const RBTree::data_t &element)
{
    std::stringstream A;
      A << std::get<0>(element)            << "\n"
      << std::get<0>(get(1, element))    << "\n"
      << std::get<1>(get(1, element))    << "\n"
      << std::get<2>(get(1, element))    << "\n"
      << std::get<3>(get(1, element))    << "\n"
      << std::get<4>(get(1, element))    << "\n"
      << std::get<5>(get(1, element))    << "\n\n";
    return A.str();
}
#include <fstream>
// ����� �������� ��������� ������ ������
void PreOrderRecurse(std::ofstream &write, RBTree::PNode val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        write << elementValue(val->value);
        PreOrderRecurse(write, val->left);
        PreOrderRecurse(write, val->right);
        }
}

// ����� �������� ��������� ������ ������
void InOrderRecurse(std::ofstream &write, RBTree::PNode val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        InOrderRecurse(write, val->left);
        write << elementValue(val->value);
        InOrderRecurse(write, val->right);
        }
}

// ����� �������� ��������� ������ ������
void PostOrderRecurse(std::ofstream &write, RBTree::PNode val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        PostOrderRecurse(write, val->left);
        PostOrderRecurse(write, val->right);
        write << elementValue(val->value);
        }
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WritePreOrderRecurse(const std::string &filename, const RBTree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open()) PreOrderRecurse(toWrite, A.root);
    toWrite.close();
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WriteInOrderRecurse(const std::string &filename, const RBTree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open())  InOrderRecurse(toWrite, A.root);
    toWrite.close();
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WritePostOrderRecurse(const std::string &filename, const RBTree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open()) PostOrderRecurse(toWrite, A.root);
    toWrite.close();
}


 void PreOrderCycle(const std::string &filename, const RBTree &A) noexcept
 {
    if (A.count() != 0){
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){ // ���� ������� ������ ������

    std::stack<RBTree::PNode> stack; // ��� ������ ���������
    RBTree::PNode current = A.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            toWrite << elementValue(current->value);
            current = current->left;
        }
        current = stack.top()->right;
        stack.pop();
    }}
    toWrite.close();
 }}


 void InOrderCycle(const std::string &filename, const RBTree &A)noexcept
 {
    if (A.count() != 0){
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){
    std::stack<RBTree::PNode> stack;
    RBTree::PNode current = A.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            current = current->left;
        }
        toWrite << elementValue(stack.top()->value);
        current = stack.top()->right;
        stack.pop();
    }}}
 }

 void PostOrderCycle(const std::string &filename, const RBTree &A)noexcept
 {
    if (A.count() != 0){ // ���� ���� ��� ��������
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){ // ���� �������� ��������
    std::stack<RBTree::PNode> stack, stackR; // ��� ������ ���������
    RBTree::PNode current = A.root;
    stack.push(current);
    while (!stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        current = stack.top();
        stack.pop();
        stackR.push(current);
        if (current->left)  stack.push(current->left);
        if (current->right) stack.push(current->right);
    }
    while(!stackR.empty()) // ����, ������� ���������� � ������� ��������
        {
            toWrite << elementValue(stackR.top()->value); // ������ ������ � ����
            stackR.pop();
        }
      }
   }
}

