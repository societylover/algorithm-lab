#ifndef RBTREE_H
#define RBTREE_H
#include "component.h"



class RBTree
{
  public:
  enum color { red = 0, black = 1 };
  enum direction { lft = 0, rght = 1 }; // ��� ���������� ���������� � ������ �������
  using key_t   = uint64_t;
  using value_type = computer;
  using data_t  = std::tuple<key_t, value_type>;
  private:
    struct node_t
    {
        data_t value;
        color cvet = red;  // ������� ��� ������ ����
        node_t * left = nullptr, * right = nullptr, * parent = nullptr;
        node_t() = default;
        node_t(const data_t &val, const color &cvt = red) : value(val), cvet(red) {}
        node_t * uncle() const;
        bool isOnLeft() const;
        node_t * sibling() const;
        void move_down(node_t * nParent);
        bool has_red_child() const;
    };
  public: using PNode = node_t * ;
  private:
    PNode root = nullptr;
    size_t _size = 0;
    size_t recursive_height(const node_t *tmp) const noexcept; // ��� �������� ������ ������
    // ��� ���������� �������� �� �������������� ������ ������ ������� ������
    void leftRotate(PNode x);
    void rightRotate(PNode x);
    void fixRedRed(PNode x); // root ������ ������
    PNode successor(PNode x); // ����� ���� � x
    PNode tree_replace(PNode x);
    void fixDoubleBlack(PNode x);
    // ������� ����
    void deleteNode(PNode v);
    // ����� ����
    PNode search(key_t n);
    // �������� ����
    bool insert_node(data_t n);
  public:
    RBTree() = default;
    ~RBTree();
    RBTree(const data_t& d);
    RBTree(const std::initializer_list<data_t> &t);
    RBTree(const RBTree& other);
    RBTree(RBTree&& other) noexcept;                // -- ����������� �������� --
    RBTree& operator=(RBTree&& other) noexcept;     // -- �������� ����������� --
    RBTree& operator=(const RBTree& other);
// ���������� ------------------
    bool empty() const noexcept;
    size_t count() const noexcept;              // -- ����������� ��������� --
    size_t height() const noexcept;             // -- ������ ������ --
// ����� -----------------------
    data_t find(const key_t& key) const;
    // ����� �������� � ����������� ������
    friend data_t find_min(const RBTree &A);
// ����� �������� � ������������ ������
    friend data_t find_max(const RBTree &A);
// ������������ ���������� --
// �������� --
    bool insert(const key_t& key, const value_type& v);
    bool insert(const data_t& d);
// �������� --
    bool replace(const key_t& key, const value_type& v);
// ������� --
    bool erase (const key_t& key);              // ������� ���������
    void clear ();                              // ������� ���
// ����� --
    void swap (RBTree &t) noexcept;        // �������� � �������� �������

    // ����������� ��������
    void concat(RBTree &t);
    // ����������� ������
    friend void PreOrderRecurse(std::ofstream &write, PNode val)noexcept;
    friend void InOrderRecurse(std::ofstream &write, PNode val)noexcept;
    friend void PostOrderRecurse(std::ofstream &write, PNode val)noexcept;
    // ������������ ������
    friend void PreOrderCycle(const std::string &fileName, const RBTree &A)noexcept;
    friend void InOrderCycle(const std::string &fileName, const RBTree &A)noexcept;
    friend void PostOrderCycle(const std::string &fileName, const RBTree &A)noexcept;
    // ������� � ������� ������ ��������� �� ������


    friend void WritePreOrderRecurse(const std::string &fileName, const RBTree &A) noexcept;
    friend void WriteInOrderRecurse(const std::string &fileName, const RBTree &A) noexcept;
    friend void WritePostOrderRecurse(const std::string &fileName, const RBTree &A) noexcept;

};

#endif // RBTREE_H
