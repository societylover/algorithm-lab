#include "PriceList.h"

// ������������
// ��� ������ ������� PriceList l-value
PriceList::PriceList(const PriceList& other)  : firmName(other.firmName), create_date(other.create_date),  models_in(other.models_in), Container(other.Container)
{

}

// ��� ������ PriceList r-value
PriceList::PriceList(PriceList&& other) : firmName(other.firmName), create_date(other.create_date), Container(other.Container), models_in(other.models_in)
{
    // ������ �������� r-value
    other.firmName = "";
    other.create_date = date_template;
    other.models_in = 0;
    other.Container.clear();
}

// ��������� ������������
// ������������ ��� r-value
PriceList& PriceList::operator=(PriceList&& other)
{
    firmName = other.firmName;
    create_date = other.create_date;

    Container = other.Container;
    models_in = other.models_in;

    // ������ r-value ��������
    other.firmName = "";
    create_date = date_template;
    other.Container.clear();
    models_in = 0;

    return *this;
}

// ������������ ��� r-value
PriceList& PriceList::operator=(const PriceList& other)
{

    firmName = other.firmName;
    create_date = other.create_date;
    Container = other.Container;
    models_in = other.models_in;

    return *this;
}

// ����������
PriceList::~PriceList()
{
    firmName = "";
    create_date = date_template;
    models_in = 0;
    Container.clear();

}

// ������ ������ PriceList
// ���������� ��������� ��� r-value
void PriceList::add(const hash_t::key_t &key, const computer& to_insert) noexcept
{
    Container.insert(key, to_insert);
    models_in = Container.count();
}

// ���������� ��������� ��� l-value
void PriceList::add(const hash_t::key_t &key, computer&& to_insert) noexcept
{
    Container.insert(key, to_insert); // ��������� �������
    models_in = Container.count();    // ��������� ���������� � �������
    to_insert = computer_template;
}

// ���������� ������� � ����� �����
size_t count(const PriceList &A) noexcept
{
    return A.models_in;
}


// �������� ���������
bool PriceList::remove(const hash_t::key_t& to_delete) noexcept
{
    bool erased = false;
    if (Container.erase(to_delete)) // ������� �������
    {
        models_in = Container.count(); // ��������� ���������� � �������
        erased = !erased;
    }
    return erased;
}

// ��������� ���������
bool PriceList::change(const hash_t::key_t& id_change, const computer& new_value)
{
    return (Container.replace(id_change, new_value)); // ���������� ����� ��������
}
