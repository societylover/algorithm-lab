#ifndef HASH_H
#define HASH_H

#include "component.h"

const size_t table_size = 257; // 1024
const size_t x_gorner_divide = 23;
const size_t x_gorner_highlight = 256;

bool chain_method = true;
bool quadrat_rehash = false;

const uint32_t mask = 255; // ��� ��� ��������� 8 ���, 1023 ��� ��������� 10 ���

class hash_t
{ public:
  using key_t   = uint64_t;
  using value_type = computer;
  using data_t  = std::tuple<key_t, value_type>;
  private:
    struct node_t
    {
        key_t key;        // ������ � ������������
        value_type value; // ���� � ��� ������
        node_t * next = nullptr; // chain
        node_t(const value_type &val) : value(val) {}
        node_t() = default;
    };
  public: using PNode = node_t *;
  private:
    node_t * head = nullptr; // ��������� �� ������
    size_t _count = 0;
    size_t count_hash_function_by_divide(const key_t &key); // ������� �������� ��� �������
    size_t count_hash_function_gorner_divide(const key_t &key); // ������� �������� ��� �������
    size_t count_hash_function_gorner_highlight(const key_t &key); // ������� �������� ��� �������

    PNode find_pointer(const key_t &key);

  public:
    hash_t();
    ~hash_t();
    hash_t(const data_t& d);
// ���������� ------------------
    bool empty() const noexcept;
    size_t count() const noexcept;              // -- ����������� ��������� --
// ����� -----------------------
    data_t find(const key_t& key) ;
// ������������ ���������� --
// �������� --
    bool insert(const key_t& key, const value_type& v);
    bool insert(const data_t& d);
// �������� --
    bool replace(const key_t& key, const value_type& v);
// ������� --
    bool erase (const key_t& key);              // ������� ���������
    void clear ();                              // ������� ���
// ����� --
    void swap (hash_t &t) noexcept;        // �������� � �������� �������
};



#endif // HASH_H
