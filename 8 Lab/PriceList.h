    #ifndef PRICELIST_H
    #define PRICELIST_H

    #include "hash_t.h"

    class PriceList
    {

       NAME firmName;
       DATE create_date;
       hash_t Container;
       CONTAIN models_in = 0;

       public :

       PriceList (const DATE &c_date, const NAME &f_name) noexcept : firmName(f_name), create_date(c_date), Container() {};
       PriceList (const DATE &c_date, const NAME &f_name, const hash_t &Elements) noexcept : firmName(f_name), create_date(c_date), Container(Elements), models_in(Elements.count()) {};

       PriceList(const PriceList &other);

       PriceList(PriceList && other);

       PriceList& operator=(PriceList &&other);

       PriceList& operator=(const PriceList &other);

       ~PriceList();

       void add(const hash_t::key_t &key, const computer &to_insert) noexcept;

       void add(const hash_t::key_t &key, computer && to_insert) noexcept;

       friend size_t count(const PriceList &A) noexcept;

       bool remove(const hash_t::key_t& to_delete) noexcept;

       bool change(const hash_t::key_t& id_change, const computer &new_value);

       friend void WriteInFile(const PriceList &pr_list, const std::string &fileName) noexcept;
    };

    #endif // PRICELIST_H
