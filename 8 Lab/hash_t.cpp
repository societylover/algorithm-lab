#include "hash_t.h"

#include <stack>
#include <vector>
#include <algorithm>

size_t hash_t::get_gorner_result(uint64_t key)
{
    std::vector <uint64_t> values;

    while (key != 0)
    {
        values.push_back(key % 10);
        key /= 10;
    }
    std::reverse(values.begin(), values.end());
    for (size_t i = 1; i < values.size(); i++) values[i] += values[i - 1] * x_gorner_divide;
    return values[values.size()-1];
}

// ������� �������� ��� ������� ��� �������� �� ������
size_t hash_t::count_hash_function_by_divide(const key_t& key)
{
    return key % table_size;
}

// ������� �������� ��� ������� �� ������� � ������� �� ������ �������
size_t hash_t::count_hash_function_gorner_divide(const key_t& key)
{
    return count_hash_function_by_divide(get_gorner_result(key));
}

// ������� �������� ��� ������� �� ������� � ��������� ����
size_t hash_t::count_hash_function_gorner_highlight(const key_t& key)
{
    return (get_gorner_result(key) & mask);
}

hash_t::~hash_t()
{
    clear();
}

hash_t& hash_t::operator=(const hash_t &other)
{
     for (size_t i = 0; i < table_size; i++)
     {
         PNode current = &other.head[i];
         head[i].key = current->key;
         head[i].value = current->value;
         while (current->next != nullptr)
         {
             insert(current->key, current->value);
             current = current->next;
         }
     }
     return *this;
}

size_t hash_t::line_count(const key_t& key) noexcept
{
    PNode tmp = find_pointer(key);
    size_t i = 0;
    if (tmp->key != table_size + 1)
    {
        if (chain_method)
        {
           while (tmp != nullptr) { i++; tmp = tmp->next; }
        } else if (quadrat_rehash) i = 1;
    }
    return i;
}


hash_t::hash_t(const hash_t& d)
{
    head = new node_t [table_size];
    for (size_t i = 0; i < table_size; i++)
     {
         PNode current = &d.head[i];
         head[i].key = current->key;
         head[i].value = current->value;
         while (current->next != nullptr)
         {
             insert(current->key, current->value);
             current = current->next;
         }
     }
}

hash_t::hash_t()
{
    head = new node_t [table_size];
    for (size_t i = 0; i < table_size; i++) head[i].key = table_size + 1;

}

hash_t::hash_t(const std::initializer_list<data_t> &t)
{
    head = new node_t [table_size];
    for (size_t i = 0; i < table_size; i++) head[i].key = table_size + 1;
    std::for_each(t.begin(), t.end(),[this](data_t val){insert(val);});
}

hash_t::hash_t(const data_t& d)
{
    head = new node_t [table_size];
    for (size_t i = 0; i < table_size; i++) head[i].key = table_size+1;
    insert(d);
}

bool hash_t::empty() const noexcept
{
    return (head == nullptr);
}

size_t hash_t::count() const noexcept
{
    return _count;
}

size_t count_step(const uint64_t &key, const uint8_t &step, uint8_t &try_count)
{
    size_t result = key + step * try_count;
    try_count++;
    return result;
}

hash_t::PNode hash_t::find_pointer(const key_t &key)
{
    size_t index_of = count_hash_function_gorner_highlight(key);
    if (head != nullptr){
    if (chain_method)
    {
      if (head[index_of].key != table_size + 1) return &head[index_of];
    } else if (quadrat_rehash)
    {
        for (size_t i = 0; i < table_size; i++) {
        size_t index = (index_of + i*i) % table_size;
        if (head[index].key == key)
        return &head[index_of];
        }
    }
    }
    return nullptr;
}

hash_t::data_t hash_t::find(const key_t& key)
{
    PNode result = find_pointer(key);
    if (result != nullptr)  return std::make_tuple(key, result->value);
    else return std::make_tuple(table_size + 1, computer_template);
}

bool hash_t::insert(const key_t& key, const value_type& v)
{
    size_t result = count_hash_function_gorner_highlight(key);
    // ����� ����� - project 1: ������� �� ������� �� ������ �������
    // project 2: ������ x = 23 ������� �� ������ �������
    // project 3: ������ x = 256 ��������� 8 ��� 10 ��� (�� ������� �������)
    if (chain_method)
    {
    if (head[result].key != table_size + 1) // ���� �� ����� nullptr, ��� ���-�� ����
    {
        PNode current = &head[result];
        while (current->next != nullptr) current = current->next;

        current->next = new node_t(v);
        current->next->key = key;
    } else
         { head[result].value = v;
           head[result].key = result; }
    }
    else if (quadrat_rehash)
    {
       for (size_t i = 0; i < table_size; i++) {
        size_t index = (result + i*i) % table_size;
        if (head[index].key == table_size + 1) {
            head[index].value = v;
            head[index].key = key;
            _count++;
            return true;
       }
    }
    return false; // �� ������� ��� ������������ �����������
    }
    _count++;
    return true;
}

bool hash_t::insert(const data_t& d)
{
    return (insert(std::get<0>(d), std::get<1>(d)));
}

bool hash_t::replace(const key_t& key, const value_type& v)
{
    PNode fnd_ = find_pointer(key);
    if (fnd_ != nullptr)
    {
        fnd_->value = v;
        fnd_->key = key; ///!
        return true;
    }
    return false;
}

bool hash_t::erase(const key_t& key)
{
    PNode fnd_ = find_pointer(key);
    if (fnd_ != nullptr)
    {
        if (chain_method)
        {
            size_t index = count_hash_function_gorner_highlight(key);
            if (head[index].next == nullptr)
            {
                head[index].key = table_size + 1;
            } else
            {
                PNode to_delete = head[index].next;
                std::swap(head[index].value, head[index].next->value);
                std::swap(head[index].next, head[index].next->next);
                delete to_delete;
            }
        }
        else if (quadrat_rehash) fnd_->key = table_size + 1;
        _count--;
        return true;
    }
    return false;
}


void hash_t::clear()
{
    if (head != nullptr){
    for (size_t i = 0; i < table_size; i++)
    {
        PNode current = head[i].next;
        if (current){
        std::stack<PNode> values;
        while (current != nullptr)
        {
            values.push(current);
            current = current->next;
        }
        while (!values.empty()){
        delete values.top();
        values.pop();
        }
    }
    }
    delete [] head;
    head = nullptr;
    _count = 0;}
}

void hash_t::swap(hash_t& t) noexcept
{
    std::swap(head, t.head);
    std::swap(_count, t._count);
}

