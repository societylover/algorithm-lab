#include <sstream>

#include "catch.hpp"
#include "PriceList.h"


TEST_CASE("������������ ������ PriceList ")
{
    SECTION("�������� � ������������� ��������� ������")
    {
        // ���������� � ����������� ���� � ��������
        PriceList a(date_template, "mvideo");
        PriceList b(std::make_tuple(1,1,2000), "dns");
        // ����������� � ����������� ����, ��� � ������
        hash_t e ({std::make_tuple(3, std::make_tuple("1",2,3,4,5,6)), std::make_tuple(2, std::make_tuple("3",3,3,3,3,3)), std::make_tuple(4, std::make_tuple("3",3,3,3,3,3))});
        hash_t cd ({std::make_tuple(3, std::make_tuple("1",2,3,4,5,6)), std::make_tuple(2, std::make_tuple("3",3,3,3,3,3)), std::make_tuple(4, std::make_tuple("3",3,3,3,3,3))});
        e = cd;
        hash_t abc(cd);
        PriceList d(date_template, "dns", e);
        PriceList c(d);
        c = a;
    }
    SECTION("������ PriceList")
    {
        SECTION("���������� �������� � PriceList")
        {
             PriceList a(date_template, "mvideo");
             // ��������� ��������
             a.add(1, std::make_tuple("6",2,3,4,5,6)); // &&
             a.add(3, std::make_tuple("8",1,2,3,4,5)); // &&
             CHECK(count(a) == 2);
             computer c = std::make_tuple("8",1,1,1,1,1);
             a.add(4, c); // &
             CHECK(count(a) == 3);
        }
        SECTION("��������� �������� � PriceList")
        {
             PriceList a(date_template, "mvideo");
             // ��������� ��������
             a.add(1, std::make_tuple("6",2,3,4,5,6)); // &&
             a.add(3, std::make_tuple("8",1,2,3,4,5)); // &&
             CHECK(count(a) == 2);
             computer c = std::make_tuple("99",1,1,1,1,1);
             CHECK(a.change(3, c)); // ������� ������� � ������ 3
             CHECK_FALSE(a.change(4, c)); // ���������� �������� �������������� �������
        }
        SECTION("�������� ������ � ������")
        {
            PriceList a(date_template, "mvideo");
             // ��������� ��������
             a.add(1, std::make_tuple("6",2,3,4,5,6)); // &&
             a.add(3, std::make_tuple("8",1,2,3,4,5)); // &&
             CHECK(count(a) == 2);
             computer c = std::make_tuple("99",1,1,1,1,1);
             CHECK(a.remove(3)); // ������ ������� � ������ 3
             CHECK_FALSE(a.remove(4)); // ���������� ������� �������������� �������
        }
    }
}

