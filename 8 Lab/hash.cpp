#include "hash.h"

#include <vector>
#include <algorithm>

size_t get_gorner_result(uint64_t key)
{
    std::vector <uint64_t> values;

    while (key != 0)
    {
        values.push_back(key % 10);
        key /= 10;
    }
    std::reverse(values.begin(), values.end());
    for (size_t i = 1; i < values.size(); i++) values[i] += values[i - 1] * x_gorner_divide;
    return values[values.size()-1];
}

// ������� �������� ��� ������� ��� �������� �� ������
size_t hash_t::count_hash_function_by_divide(const key_t& key)
{
    return key % table_size;
}

// ������� �������� ��� ������� �� ������� � ������� �� ������ �������
size_t hash_t::count_hash_function_gorner_divide(const key_t& key)
{
    return count_hash_function_by_divide(get_gorner_result(key));
}

// ������� �������� ��� ������� �� ������� � ��������� ����
size_t hash_t::count_hash_function_gorner_highlight(const key_t& key)
{
    return (get_gorner_result(key) & mask);
}

hash_t::~hash_t()
{
    clear();
}

hash_t::hash_t()
{
    head = new node_t [table_size];
    for (size_t i = 0; i < table_size; i++) head[i].key = table_size+1;
}

hash_t::hash_t(const data_t& d)
{
    head = new node_t [table_size];
    for (size_t i = 0; i < table_size; i++) head[i].key = table_size+1;
    insert(d);
}

bool hash_t::empty() const noexcept
{
    return (head == nullptr);
}

size_t hash_t::count() const noexcept
{
    return _count;
}

size_t count_step(const uint64_t &key, const uint8_t &step, uint8_t &try_count)
{
    size_t result = key + step * try_count;
    try_count++;
    return result;
}

hash_t::PNode hash_t::find_pointer(const key_t &key)
{
    size_t index_of = count_hash_function_by_divide(key);
    if (head != nullptr){
    if (chain_method)
    {
      if (&head[index_of] != nullptr) return &head[index_of];
    } else if (quadrat_rehash)
    {
        for (size_t i = 0; i < table_size; i++) {
        size_t index = (index_of + i*i) % table_size;
        if (head[index].key == key)
        return &head[index_of];
        }
    }
    }
    return nullptr;
}

hash_t::data_t hash_t::find(const key_t& key)
{
    return std::make_tuple(key, find_pointer(key)->value);
}

bool hash_t::insert(const key_t& key, const value_type& v)
{
    size_t result = count_hash_function_by_divide(key);
    // ����� ����� - project 1: ������� �� ������� �� ������ �������
    // project 2: ������ x = 23 ������� �� ������ �������
    // project 3: ������ x = 256 ��������� 8 ��� 10 ��� (�� ������� �������)
    if (chain_method)
    {
    if (&head[result] != nullptr) // ���� �� ����� nullptr, ��� ���-�� ����
    {
        PNode current = &head[result];
        while (current->next != nullptr) current = current->next;
        current->next = new node_t(v);
    } else head[result].value = v;
    }
    else if (quadrat_rehash)
    {
       for (size_t i = 0; i < table_size; i++) {
        size_t index = (result + i*i) % table_size;
        if (head[index].key = table_size + 1) {
            head[index].value = v;
            head[index].key = result;
            _count++;
            return true;
        }
    }
    return false; // �� ������� ��� ������������ �����������
    }
    _count++;
    return true;
}

bool hash_t::insert(const data_t& d)
{
    return (insert(std::get<0>(d), std::get<1>(d)));
}

bool hash_t::replace(const key_t& key, const value_type& v)
{
    PNode fnd_ = find_pointer(key);
    if (fnd_ != nullptr)
    {
        fnd_->value = v;
        fnd_->key = key; ///!
        return true;
    }
    return false;
}

bool hash_t::erase(const key_t& key)
{
    PNode fnd_ = find_pointer(key);
    if (fnd_ != nullptr)
    {
        if (chain_method)
        {
          PNode to_delete = fnd_;
          size_t index = count_hash_function_by_divide(key);
          head[index] = *head[index].next;
          delete to_delete;
        }
        else if (quadrat_rehash)
        {
        fnd_->value = computer_template;
        fnd_->key = 0;
        }
        _count--;
        return true;
    }
    return false;
}

#include <stack>

void hash_t::clear()
{
    for (size_t i = 0; i < table_size; i++)
    {
        PNode current = &head[i];
        if (current != nullptr){
        std::stack<PNode> values;
        while (current->next != nullptr)
        {
            values.push(current);
            current = current->next;
        }
        while (!values.empty())
        delete values.top();
        values.pop();
        }
    }
    delete [] head;
    head = nullptr;
    _count = 0;
}

void hash_t::swap(hash_t& t) noexcept
{
    std::swap(head, t.head);
    std::swap(_count, t._count);
}
