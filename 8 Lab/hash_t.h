#ifndef HASH_T_H
#define HASH_T_H

#include "component.h"

const uint32_t table_size = 257; // 1024 257
const size_t x_gorner_divide = 23;
const size_t x_gorner_highlight = 256;


const uint32_t mask = 255; // 255 ��� ��� ��������� 8 ���, 1023 ��� ��������� 10 ���

class hash_t
{ public:
  bool chain_method = false, quadrat_rehash = true;
  using key_t   = uint64_t;
  using value_type = computer;
  using data_t  = std::tuple<key_t, value_type>;
  private:
    struct node_t
    {
        key_t key;        // ������ � ������������
        value_type value; // ���� � ��� ������
        node_t * next = nullptr; // chain
        node_t(const value_type &val) : value(val) {}
        node_t() : next(nullptr) {}
    };
  public: using PNode = node_t *;
  private:
    node_t * head = nullptr; // ��������� �� ������
    size_t _count = 0;
    size_t count_hash_function_by_divide(const key_t &key); // ������� �������� ��� �������
    size_t count_hash_function_gorner_divide(const key_t &key); // ������� �������� ��� �������
    size_t count_hash_function_gorner_highlight(const key_t &key); // ������� �������� ��� �������
    size_t get_gorner_result(uint64_t key);
    PNode find_pointer(const key_t &key);

  public:
    hash_t();
    ~hash_t();
    hash_t(const hash_t& d);
    hash_t& operator=(const hash_t &other);
    hash_t(const data_t& d);
    hash_t(const std::initializer_list<data_t> &t);
// ���������� ------------------
    bool empty() const noexcept;
    size_t count() const noexcept;              // -- ����������� ��������� --
    size_t line_count(const key_t& key) noexcept;
// ����� -----------------------
    data_t find(const key_t& key) ;
// ������������ ���������� --
// �������� --
    bool insert(const key_t& key, const value_type& v);
    bool insert(const data_t& d);
// �������� --
    bool replace(const key_t& key, const value_type& v);
// ������� --
    bool erase (const key_t& key);              // ������� ���������
    void clear ();                              // ������� ���
// ����� --
    void swap (hash_t &t) noexcept;        // �������� � �������� �������
};

#endif // HASH_T_H
