#include <sstream>

#include "catch.hpp"
#include "PriceList.h"

std::string readFromFile(const std::string &flName)
{
    std::string A;
    std::ifstream B(flName);
    getline(B,A,'\0');
    B.close();
    return A;
}

TEST_CASE("������������ ������ PriceList ")
{
    SECTION("�������� � ������������� ��������� ������")
    {
        // ���������� � ����������� ���� � ��������
        PriceList a(date_template, "mvideo");
        PriceList b(std::make_tuple(1,1,2000), "dns");
        // ����������� � ����������� ����, ��� � ������
        tree e ({std::make_tuple("1", std::make_tuple("1",2,3,4,5,6)), std::make_tuple("2", std::make_tuple("3",3,3,3,3,3)), std::make_tuple("4", std::make_tuple("3",3,3,3,3,3))});
        PriceList d(date_template, "dns", e);
        PriceList c(d);
        PriceList f = d;
        f = a;
    }
    SECTION("��� �������� ������ ������")
    {
        // ��������� �������� ����� �� ���������� � ����������
        std::string fileName = "test1.txt";
        std::string fileName2 = "test2.txt";
        tree a({std::make_tuple("1", std::make_tuple("1",2,3,4,5,6)),
                std::make_tuple("2", std::make_tuple("3",3,3,3,3,3)),
                std::make_tuple("4", std::make_tuple("3",3,3,3,3,3)),
                std::make_tuple("5", std::make_tuple("6",7,8,9,1,10))});
        // ������ ������� ������ ������ - ��������� � ���������
        WritePreOrderRecurse(fileName, a);
        PreOrderCycle(fileName2, a);
        // ��������� ����
        CHECK(readFromFile(fileName) == readFromFile(fileName2));
        // ������ ������� ������ ������
        WriteInOrderRecurse(fileName, a);
        InOrderCycle(fileName2, a);
        CHECK(readFromFile(fileName) == readFromFile(fileName2));
        // ������ ������� ������ ������
        WritePostOrderRecurse(fileName, a);
        PostOrderCycle(fileName2, a);
        CHECK(readFromFile(fileName) == readFromFile(fileName2));
    }
    SECTION("����� ������������ � ������������� �����")
    {
        // ��������� �������� ����� �� ���������� � ����������
        tree a({std::make_tuple("7", std::make_tuple("6",2,3,4,5,6)),
                std::make_tuple("2", std::make_tuple("3",3,3,3,3,3)),
                std::make_tuple("4", std::make_tuple("77",3,3,3,3,3)),
                std::make_tuple("8", std::make_tuple("5",7,8,9,1,10))});
        CHECK(strcmp(std::get<0>(find_min(a)).id,"2") == 0);
        CHECK(strcmp(std::get<0>(find_max(a)).id,"8") == 0);
        a.insert(std::make_tuple("9", std::make_tuple("6",2,3,4,5,6)));
        CHECK(strcmp(std::get<0>(find_max(a)).id,"9") == 0);
        a.insert(std::make_tuple("11", std::make_tuple("6",2,3,4,5,6)));
        CHECK(strcmp(std::get<0>(find_max(a)).id,"11") == 0);
        a.insert(std::make_tuple("1", std::make_tuple("6",2,3,4,5,6)));
        CHECK(strcmp(std::get<0>(find_min(a)).id,"1") == 0);
    }
    SECTION("����������� ��������")
    {
        // ��������� �������� ����� �� ���������� � ����������
        tree a({std::make_tuple("7", std::make_tuple("6",2,3,4,5,6)),
                std::make_tuple("2", std::make_tuple("3",3,3,3,3,3)),
                std::make_tuple("4", std::make_tuple("77",3,3,3,3,3)),
                std::make_tuple("8", std::make_tuple("5",7,8,9,1,10))});

        tree c({std::make_tuple("1", std::make_tuple("6",2,3,4,5,6)),
                std::make_tuple("5", std::make_tuple("3",3,3,3,3,3)),
                std::make_tuple("3", std::make_tuple("77",3,3,3,3,3)),
                std::make_tuple("77", std::make_tuple("5",7,8,9,1,10)),
                std::make_tuple("6", std::make_tuple("5",7,8,9,1,10))});

        tree d({std::make_tuple("99", std::make_tuple("6",2,3,4,5,6))});

        c.concat(a); // a ������ �������� 8 ���������, � c 0
        CHECK(c.count() == 9);
        CHECK(a.count() == 0);
        // ��������� ����� �������� �������� � ���������
        CHECK(strcmp(std::get<0>(find_max(c)).id,"77") == 0);
        CHECK(strcmp(std::get<0>(find_min(c)).id,"1")  == 0);

        d.concat(c);
        // �������� ���������� ��������� � ��������
        CHECK(d.count() == 10);
        CHECK(a.count() == 0);
        CHECK(c.count() == 0);
        // �������� ���� ������������� � ������������ �������� � ������ d
        CHECK(strcmp(std::get<0>(find_max(d)).id,"99") == 0);
        CHECK(strcmp(std::get<0>(find_min(d)).id,"1")  == 0);
    }
    SECTION("������ PriceList")
    {
        SECTION("���������� �������� � PriceList")
        {
             PriceList a(date_template, "mvideo");
             // ��������� ��������
             a.add("1", std::make_tuple("6",2,3,4,5,6)); // &&
             a.add("3", std::make_tuple("8",1,2,3,4,5)); // &&
             CHECK(count(a) == 2);
             computer c = std::make_tuple("8",1,1,1,1,1);
             a.add("4", c); // &
             CHECK(count(a) == 3);
        }
        SECTION("��������� �������� � PriceList")
        {
             PriceList a(date_template, "mvideo");
             // ��������� ��������
             a.add("1", std::make_tuple("6",2,3,4,5,6)); // &&
             a.add("3", std::make_tuple("8",1,2,3,4,5)); // &&
             CHECK(count(a) == 2);
             computer c = std::make_tuple("99",1,1,1,1,1);
             CHECK(a.change("3", c)); // ������� ������� � ������ 3
             CHECK_FALSE(a.change("4", c)); // ���������� �������� �������������� �������
        }
        SECTION("�������� ������ � ������")
        {
            PriceList a(date_template, "mvideo");
             // ��������� ��������
             a.add("1", std::make_tuple("6",2,3,4,5,6)); // &&
             a.add("3", std::make_tuple("8",1,2,3,4,5)); // &&
             CHECK(count(a) == 2);
             computer c = std::make_tuple("99",1,1,1,1,1);
             CHECK(a.remove("3")); // ������ ������� � ������ 3
             CHECK_FALSE(a.remove("4")); // ���������� ������� �������������� �������
        }
    }
    SECTION("����������")
    {
        CHECK_THROWS([&](){tree a({std::make_tuple("7", std::make_tuple("6",2,3,4,5,6)),
                                   std::make_tuple("7", std::make_tuple("3",3,3,3,3,3))});}()); // �������� � ���������� ������ ���������
        CHECK_THROWS([&](){tree a(std::make_tuple("7", std::make_tuple("6",2,3,4,5,6))); a.insert("7",std::make_tuple("6",2,3,4,5,6)); }()); // ��������� ������� � ����� �� Id
        tree ABC;
        CHECK_THROWS([&](){tree::data_t tmp =  find_max(ABC);}); // ����� � ������ ������
        CHECK_THROWS([&](){tree::data_t tmp =  find_min(ABC);}); // ����� � ������ ������
        CHECK_THROWS([&](){tree::data_t tmp =  ABC.find("1");}); // ����� � ������ ������
        CHECK_THROWS([&](){ABC.concat(ABC);}); // ������� � ����� �����
    }
}
