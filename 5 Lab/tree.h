#ifndef TREE_H
#define TREE_H

#include <exception>    // ��� ��������� ����������
#include <stack>        // ��� ���������� ������������ ������ ���������� ������������ ����
#include <fstream>      // ��� ������ � ����
#include <sstream>

#include "component.h"

class tree
{
  public:
  // �������� ��������������� ��� ����
  using key_t   = uint64_t;
  using value_t = computer;
  using data_t  = std::tuple<ID, value_t>;
  using size_type = size_t;
  private:
    // ���� ������
    struct node_t
    {
        data_t element;
        // ��������� �� ���������� � ��������� �������
        node_t *left    = nullptr;
        node_t *right   = nullptr;
        // �����������
        node_t(const data_t &tmp) : element(tmp) {};
        ~node_t()
        {
            left = right = nullptr;
        }
    };
    // �������� ������
    node_t *root = nullptr; // ������ ������
    size_type recursive_height(const node_t *tmp) const noexcept; // ��� �������� ������ ������
    size_type count_value = 0; // ���������� ��������� � ������

    void check_for_unique_id(const data_t &value) const; // �������� �� ���� ������������� � ����������� id
    friend void reWriteRight(node_t * current, node_t * prev_current);
  public:
    // ��������� void, ��� ������������ ��� ���������� ��� ������ ���������� � ����������
    tree(void) noexcept {};
    virtual ~tree(void);
    tree(const data_t& d) noexcept;
    tree(const std::initializer_list<data_t> &t);
    tree(const tree& other);
    tree(tree && other) noexcept;                // -- ����������� �������� --
    tree& operator=(tree&& other) noexcept;     // -- �������� ����������� --
    tree& operator=(const tree& other);
// ���������� ------------------
    bool empty() const noexcept;
    size_type count() const noexcept;              // -- ����������� ��������� --
    size_type height() const noexcept;             // -- ������ ������ -- (����� ������������ ������ ��������)
// ����� -----------------------
    data_t& find(const ID& key) const;
// ����� �������� � ����������� ������
    friend data_t find_min(const tree &A);
// ����� �������� � ������������ ������
    friend data_t find_max(const tree &A);

// ������������ ���������� --
// �������� --
    void insert(const ID& key, const value_t& v);
    void insert(const data_t& d);
// �������� --
    bool replace(const ID& key, const value_t& v);
// ������� --
    bool erase (const ID& key);              // ������� ���������
    void clear ();                              // ������� ���
// ����� --
    void swap (tree &t) noexcept;        // �������� � �������� �������
// ����������� ��������
    void concat(tree &t);
    // ����������� ������
    friend void PreOrderRecurse(std::ofstream &write, node_t * val)noexcept;
    friend void InOrderRecurse(std::ofstream &write, node_t * val)noexcept;
    friend void PostOrderRecurse(std::ofstream &write, node_t * val)noexcept;
    // ������������ ������
    friend void PreOrderCycle(const std::string &fileName, const tree &A)noexcept;
    friend void InOrderCycle(const std::string &fileName, const tree &A)noexcept;
    friend void PostOrderCycle(const std::string &fileName, const tree &A)noexcept;
    // ������� � ������� ������ ��������� �� ������


    friend void WritePreOrderRecurse(const std::string &fileName, const tree &A) noexcept;
    friend void WriteInOrderRecurse(const std::string &fileName, const tree &A) noexcept;
    friend void WritePostOrderRecurse(const std::string &fileName, const tree &A) noexcept;
};


#endif // TREE_H
