#include "tree.h"

#define get(num, element) std::get<num>(element)

// ��� ��������� ����� �� �������
size_t getKey(const tree::data_t &element)
{
    return std::atoi(get(0,element).id);
}

std::string elementValue(const tree::data_t &element)
{
    std::stringstream A;
      A << (get(0, element).id)            << "\n"
      << std::get<0>(get(1, element))    << "\n"
      << std::get<1>(get(1, element))    << "\n"
      << std::get<2>(get(1, element))    << "\n"
      << std::get<3>(get(1, element))    << "\n"
      << std::get<4>(get(1, element))    << "\n"
      << std::get<5>(get(1, element))    << "\n\n";
    return A.str();
}

// �������� id �� ������������
void tree::check_for_unique_id(const data_t &value) const
{
    size_t value_key = getKey(value);
    if (root) // ���� ������ �� ������, �� ��������� ���� �� ����� ���� � ������
    {
        node_t * current = root;
        while (current)
            {
                if (value_key == getKey(current->element)) throw std::bad_typeid(); // ���� ����� ���������� ����
                else if(value_key < getKey(current->element)) current = current->left;
                else current = current->right;
            }
    }
}

// ���������� ��������� ������
tree::~tree(void)
{
   clear();
}

// ����������� � ���������� ������ l-value
tree::tree(const data_t& d) noexcept
{
    root = new node_t(d);
}

// ����������� ������� �������������
tree::tree(const std::initializer_list<data_t>& t)
{
    std::for_each(t.begin(), t.end(), [this](data_t a){ insert(a); }); // ������ ������� ������� insert
}

// ����������� �����������
tree::tree(const tree &other)
{
    // ������ �������������� ������� � �������������� �����
    std::stack<node_t *> stack; // ��� ������ ���������
    node_t * current = other.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            insert(current->element); // ���������� �������� � ������
            current = current->left;
        }
        current = stack.top()->right;
        stack.pop();
    }
}

// ���������� �����������
tree::tree(tree&& other) noexcept : root(other.root), count_value(other.count_value) // ������������� �� ���������
{
    other.count_value = 0; // ������� ����� ���������� ���������
    other.root = nullptr;  // ������ �������� ����� � ����� other
}

// ������������� �������� =
tree& tree::operator=(tree&& other) noexcept
{
    clear(); // ������ ������ ������
    while (other.count_value != 0)
        {
            insert(other.root->element);
            other.erase(get(0,other.root->element));
        }
    return *this;
}

tree& tree::operator=(const tree& other)
{
     clear(); // ������ ������ ������
     tree tmp(other); // ��������������� ��� ������ ������������
     swap(tmp); // �������� �������� ����� ��������� � ����������
     return *this;
}

// �������� ������ �� �������
bool tree::empty() const noexcept
{
    return (root == nullptr);
}

// ���������� ����� � ������
tree::size_type tree::count() const noexcept
{
    return count_value; // ������ ��������
}

// ����������� ���������� ������ ������
tree::size_type tree::recursive_height(const node_t *tmp) const noexcept
{
    return tmp == nullptr ? 0 : (1 + std::max(recursive_height(tmp->left), recursive_height(tmp->right)));
}

// ���������� ������ ������ �������� ��������� (����� ������������ ������ ��������)
tree::size_type tree::height() const noexcept
{
    return recursive_height(root);
}

tree::data_t& tree::find(const ID& key) const
{
    if (count_value == 0) throw std::length_error("������ ������!"); // ���� � ������ ��� ���������
    bool finded = false;
    size_t int_key = std::stoi(key.id);
    node_t * current = root;
    while (!finded && current) // ���� ������� ������, ���� ������ �������� ���
        {
            if (getKey(current->element) == int_key) finded = true; // ���� ����� �������
            if (getKey(current->element) > int_key) current = current->left;
            else current = current->right;
        }
    // ��������� ������� ������ �� ����� ������
    // ���� ������� �� ������, �� ������ �������� �����
    if (!finded) return root->element; //
    // ����� ���� ������� ������ ������ data_t
    return current->element;
}

// ������� � ������ �������� ����� � ���������� � ����������
void tree::insert(const ID& key, const value_t& v)
{
    node_t * new_node = new node_t(std::make_tuple(key, v)); // ����������� �������
    size_t int_key = std::atoi(key.id);
    // ���� ������ ������� � ������
    if (!root) {
        root = new_node; // ������� � root-�������
    } else {
    node_t * current = root;//, * prev_current = root; // �������� ��������� �� ������� �����
    check_for_unique_id(new_node->element); // ��������� ��� �� �������� � ���������� ������
    bool finded = false;
    while (!finded)
        {
            //prev_current = current; // ���������� ������� ����� �����
            if (getKey(current->element) > int_key) { // ���� ������ ���������������� ����
                    if (current->left) current = current->left; // ���� �����, ���� �� � ����
                    else finded = true;  // ���� �� ����� ������ ���� �� �������� nullptr
            }
            else if (getKey(current->element) < int_key) { // ���� ������ ���������������� ����
                    if (current->right) current = current->right; // ���� �����, ���� �� � ����
                    else finded = true; // ���� �� ����� ������ ���� �� �������� nullptr
            }
        }
    // ��������� � �������
    if (getKey(current->element) > int_key) current->left = new_node;
    else current->right = new_node;
    }
    count_value++; // ����������� ��������� ����� ������
}

// ������� � ������ �������� � ������
void tree::insert(const data_t& d)
{
    insert(get(0,d),std::get<1>(d));
}

bool tree::replace(const ID& key, const value_t& v)
{
    bool finded = false;
    size_t int_key = std::stoi(key.id);
    node_t * current = root;
    while (!finded && current) // ���� �� ������ �������, ��� ������ �� nullptr
        {
            if (getKey(current->element) == int_key) finded = true;
            else if (getKey(current->element) > int_key) current = current->left;
            else current = current->right;

        }
    if (!finded) return false; // ���� �������� � ����� ������ �� ����������
    std::get<1>(current->element) = v; // ������ ����� ��������
    return finded;
}

// friend ������� ��� �������� ��������� �� ������� � ������� ����, ������������ erase
void reWriteRight(tree::node_t * current, tree::node_t * prev_current)
{
    while (current->right) // ���� ���� ������� ������
    {
        prev_current = current; // �������� �������
        current->element = current->right->element;
        current = current->right;
    }
    prev_current->right = nullptr;
    delete current;
}

// �������� �������� �� ������
bool tree::erase(const ID& key)
{
    bool finded = false;
    size_t int_key = std::atoi(key.id);
    node_t * current = root, * prev_current;
    while (!finded && current) // ���� �� ������ �������, ��� ������ �� nullptr
    {
        if (getKey(current->element) == int_key)
            finded = true; // ���� ����� ������� � ������
        else   // ����� ���������� ������� � ��������� � ����������
        {
            prev_current = current;
            // ����� � ����� ��������� �������
            if (getKey(current->element) > int_key) current = current->left;
            else current = current->right;
        }
    }
    // �� ������ ����� ������� ��� ������, �������� ���������� ��� ��������
    // 1. �������� ����� - � ������ ������ delete + nullptr
    // 2. �������� ���� � ����� �������� ��������� - ��������� �������� ������� � ��������� �����, ������� �������
    // 3. �������� ���� � ����� ��������� ����������
    if (finded) { // ���� ������ �������
    if (current->left == current->right)  // ���� current - ���� -> �� � ���� left = right = nullptr, ��� �� root
    {
        // ����� �������, � ������� ����� ������� ���������� ����� ���������
        if (current == root)
        {
            delete current;
            root = nullptr;
        }
        else
        {
            if (prev_current->left == current) prev_current->left = nullptr;
            else prev_current->right = nullptr;
            delete current;// ������ ����������� �������
        }
    }
    else if (current->left && current->right)   // ���� ������� ��� �������� ��������
    {
        if (current != root) reWriteRight(current, prev_current); // ��������� ������ ����
        else // ���� ��������� ������� ��������, �� ���������� ����� ����� ������ ������� ����� � ��������� �������� � ������ � ������� �������, ��� �������� ���� �����
        {
            current = current->left;
            if (current->right)
            {
                while (current->right) // ���� � ������ �������� ���� ������
                {
                    prev_current = current;
                    current = current->right;
                }
                root->element = current->element;
                prev_current->right = nullptr;
            }
            else  // ���� ������� ���, �� ������� �������� ������ � ������� � ���, ������ ��� � ��������� ����� ������
            {
                root->element = root->left->element;
                root->left = root->left->left;
            }
            delete current;
        }
    }
    else     // ���� ���� ���� ������� �����, ���� ������
    {
        if (current->right)
        {
            if (current != root) reWriteRight(current, prev_current); // ���� ���� ������� ������, ����� ��� �������� �����, �� ��������� �������� ������
            else { root = root->right;
            delete current;}
        }
        else // ����� �������� ������ ���
        {
            if (current == root) { root = root->left; delete current; } // ���� ������� ��� � ��� �������� ������
            else { // ����� ������� �� ���
            while (!current->right && current->left) // ���� �� �������� ������� ������ � ���� ������� �����
            {
                prev_current = current;
                current->element = current->left->element;
                current = current->left;
            }
            if (current->right) reWriteRight(current, prev_current);
            else
            {
                prev_current->left = nullptr;
                delete current;
            }
            }
        }
    }
    --count_value; // �������� ���������� ���������
    }
    return finded;
}

// ������� ������
void tree::clear()
{
    if(root) while (root) erase(get(0,root->element)); // ���� ���������� ��������� �� ����� ����� 0, ������� ������

}

// ����� ��������
void tree::swap(tree& t) noexcept
{
    std::swap(root, t.root);
    std::swap(count_value, t.count_value);
}

// ����������� ��������, ������ ������ ���������� ������
void tree::concat(tree &t)
{
    if(t.root == root) throw std::logic_error("����������� �������� ���������� � ����� �����!");// ���� �� t != �������� ���������, �� ����������
    while (t.root) // ���� � t ���� �������� ����� �� ����� � �������
        {
            insert(t.root->element);
            t.erase(get(0,t.root->element));
        }
    // �� ������ this �� ������ ���������� + �������� t, � t ����
}

// ����� ������������ ��������
tree::data_t find_min(const tree &A)
{
    // ����������� ������ � �������� ������ ������ � ������ ����� ����
    if(A.count_value == 0) throw std::length_error("������ ������, ����������� ������� �����������!"); // ���� ��������� � ������ ���
    tree::node_t * current = A.root;
    while (current->left)  current = current->left; // ��������� �����, ���� ����� ���� �������
    return current->element; // ������ ��������� �������
}
// ����� �������� � ������������ ������
tree::data_t find_max(const tree &A)
{
    std::cout << "*"<<A.count_value <<std::endl;
// ����������� ������ � �������� ������ ������ � ������ ������ ����
    if(A.count_value == 0) throw std::length_error("������ ������, ������������ ������� �����������!"); // ���� ��������� � ������ ���
    tree::node_t * current = A.root;
    while (current->right)  current = current->right; // ��������� �����, ���� ����� ���� �������
    return current->element; // ������ ��������� �������

}

// ����� �������� ��������� ������ ������
void PreOrderRecurse(std::ofstream &write, tree::node_t * val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        write << elementValue(val->element);
        PreOrderRecurse(write, val->left);
        PreOrderRecurse(write, val->right);
        }
}

// ����� �������� ��������� ������ ������
void InOrderRecurse(std::ofstream &write, tree::node_t * val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        InOrderRecurse(write, val->left);
        write << elementValue(val->element);
        InOrderRecurse(write, val->right);
        }
}

// ����� �������� ��������� ������ ������
void PostOrderRecurse(std::ofstream &write, tree::node_t * val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        PostOrderRecurse(write, val->left);
        PostOrderRecurse(write, val->right);
        write << elementValue(val->element);
        }
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WritePreOrderRecurse(const std::string &filename, const tree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open()) PreOrderRecurse(toWrite, A.root);
    toWrite.close();
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WriteInOrderRecurse(const std::string &filename, const tree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open())  InOrderRecurse(toWrite, A.root);
    toWrite.close();
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WritePostOrderRecurse(const std::string &filename, const tree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open()) PostOrderRecurse(toWrite, A.root);
    toWrite.close();
}


 void PreOrderCycle(const std::string &filename, const tree &A) noexcept
 {
    if (A.count() != 0){
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){ // ���� ������� ������ ������

    std::stack<tree::node_t *> stack; // ��� ������ ���������
    tree::node_t * current = A.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            toWrite << elementValue(current->element);
            current = current->left;
        }
        current = stack.top()->right;
        stack.pop();
    }}
    toWrite.close();
 }}


 void InOrderCycle(const std::string &filename, const tree &A)noexcept
 {
    if (A.count() != 0){
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){
    std::stack<tree::node_t *> stack;
    tree::node_t * current = A.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            current = current->left;
        }
        toWrite << elementValue(stack.top()->element);
        current = stack.top()->right;
        stack.pop();
    }}}
 }

 void PostOrderCycle(const std::string &filename, const tree &A)noexcept
 {
    if (A.count() != 0){ // ���� ���� ��� ��������
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){ // ���� �������� ��������
    std::stack<tree::node_t *> stack, stackR; // ��� ������ ���������
    tree::node_t * current = A.root;
    stack.push(current);
    while (!stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        current = stack.top();
        stack.pop();
        stackR.push(current);
        if (current->left)  stack.push(current->left);
        if (current->right) stack.push(current->right);
    }
    while(!stackR.empty()) // ����, ������� ���������� � ������� ��������
        {
            toWrite << elementValue(stackR.top()->element); // ������ ������ � ����
            stackR.pop();
        }
      }
   }
}

