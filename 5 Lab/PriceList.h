    #ifndef PRICELIST_H
    #define PRICELIST_H

    #include "tree.h"

    class PriceList
    {
       // ���� �����-�����
       NAME firmName;
       DATE create_date;
       tree Container; // �����-��������� � ����������� � �����������
       CONTAIN models_in = 0; // ���������� ��������� �������

       public :
       // �����������
       PriceList (const DATE &c_date, const NAME &f_name) noexcept : firmName(f_name), create_date(c_date), Container() {}; // � ����������� : ��� � ���� ��������
       PriceList (const DATE &c_date, const NAME &f_name,const tree &Elements) noexcept : firmName(f_name), create_date(c_date), Container(Elements), models_in(Elements.count()) {}; // � ����������� : ���, ���� ��������, ������ ������
       // ����������� �����������
       PriceList(const PriceList &other);
       // ����������� �����������
       PriceList(PriceList && other);
       // �������� ������������� � r-value
       PriceList& operator=(PriceList &&other);
       // �������� ������������ � l-value
       PriceList& operator=(const PriceList &other);
       // ����������
       ~PriceList();

       // ������ ����������, ��������, ��������� ������
       // ���������� ������ l-value
       void add(const ID &key, const computer &to_insert) noexcept; // ������������ ������������� ����� ���������� � ������ tree
       // ���������� ������ r-value
       void add(const ID &key, computer && to_insert) noexcept;
       // ��������� ���������� ������� � ����������
       friend size_t count(const PriceList &A) noexcept;
       // �������� ������
       bool remove(const ID& to_delete) noexcept;

       // ��������� ������
       bool change(const ID& id_change, const computer &new_value);

       // ������ ������ � ���� � ��������� ���������
       friend void WriteInFile(const PriceList &pr_list, const std::string &fileName) noexcept;
    };

    #endif // PRICELIST_H
