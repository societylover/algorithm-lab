#ifndef AVLTREE_H
#define AVLTREE_H


#include "component.h"
#include <fstream>      // ��� ������ � ����

class AVLtree
{ public:
  using key_t   = uint64_t;
  using value_type = computer;
  using data_t  = std::tuple<key_t, value_type>;
  private:
    struct node_t
     {
         data_t value;
         node_t * left = nullptr, *right = nullptr;
         int _height = 0;
         node_t(const data_t &val) : value(val), _height(1){};
     };
    node_t * root = nullptr; // ������ ������
    void check_ID(const key_t &value) const;
    size_t _size = 0;
    void PreAllTreeBalance(node_t * val) noexcept;
    // ����������� ���������� ������ ������
    int recursive_height(const node_t *tmp) const noexcept; // ��� �������� ������ ������
    friend void reWriteRight(node_t * current, node_t * prev_current);
  public:
    AVLtree() = default;
    virtual ~AVLtree(void);
    AVLtree(const data_t& d);
    AVLtree(const std::initializer_list<data_t> &t);
    AVLtree(const AVLtree& other);
    AVLtree(AVLtree&& other) noexcept;                // -- ����������� �������� --
    AVLtree& operator=(AVLtree&& other) noexcept;     // -- �������� ����������� --
    AVLtree& operator=(const AVLtree& other);
// ���������� ------------------
    bool   empty() const noexcept;
    size_t count() const noexcept;                              // -- ����������� ��������� --
// ����������� ��� ������������
    int height(const node_t * val) const noexcept;           // -- ������ ��������� --
    int balance_factor(const node_t * val) const;               // ������ ������ ������ ��� ������������
    void fix_heigth_value(node_t * val) noexcept;                 // �������������� ������� ����� ������������
// ��������
    node_t *rightRotate(node_t * y) noexcept;
    node_t *leftRotate(node_t * x) noexcept;
// ������������
    node_t * balance(node_t * val) noexcept;
// ����� -----------------------
    node_t * find(const key_t& key) const;
    // ����� �������� � ����������� ������
    friend data_t find_min(const AVLtree &A);
// ����� �������� � ������������ ������
    friend data_t find_max(const AVLtree &A);
// ����������� ��������
    void concat(AVLtree &t);
// ������������ ���������� --
// �������� --
    bool insert(const key_t& key, const value_type& v);
    bool insert(const data_t& d);
// �������� --
    bool replace(const key_t& key, const value_type& v);
// ������� --
    bool erase (const key_t& key);              // ������� ���������
    void clear ();                              // ������� ���
// ����� --
    void swap (AVLtree &t) noexcept;        // �������� � �������� �������

    // ����������� ������
    friend void PreOrderRecurse(std::ofstream &write, node_t * val)noexcept;
    friend void InOrderRecurse(std::ofstream &write, node_t * val)noexcept;
    friend void PostOrderRecurse(std::ofstream &write, node_t * val)noexcept;
    // ������������ ������
    friend void PreOrderCycle(const std::string &fileName, const AVLtree &A)noexcept;
    friend void InOrderCycle(const std::string &fileName, const AVLtree &A)noexcept;
    friend void PostOrderCycle(const std::string &fileName, const AVLtree &A)noexcept;
    // ������� � ������� ������ ��������� �� ������
    friend void WritePreOrderRecurse(const std::string &fileName, const AVLtree &A) noexcept;
    friend void WriteInOrderRecurse(const std::string &fileName, const AVLtree &A) noexcept;
    friend void WritePostOrderRecurse(const std::string &fileName, const AVLtree &A) noexcept;
};

#endif // AVLTREE_H
