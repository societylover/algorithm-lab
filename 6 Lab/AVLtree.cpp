#include "AVLtree.h"

#include <stack>
#include <sstream>

#define KEY(VAL) std::get<0>(VAL)
#define get(num, element) std::get<num>(element)

std::string elementValue(const AVLtree::data_t &element)
{
    std::stringstream A;
      A << (KEY(element))            << "\n"
      << std::get<0>(get(1, element))    << "\n"
      << std::get<1>(get(1, element))    << "\n"
      << std::get<2>(get(1, element))    << "\n"
      << std::get<3>(get(1, element))    << "\n"
      << std::get<4>(get(1, element))    << "\n"
      << std::get<5>(get(1, element))    << "\n\n";
    return A.str();
}


void AVLtree::check_ID(const key_t &value) const
{
    if (root) // ���� ������ �� ������, �� ��������� ���� �� ����� ���� � ������
    {
        node_t * current = root;
        while (current)
            {
                if      (value == std::get<0>(current->value)) throw std::bad_typeid(); // ���� ����� ���������� ����
                else if (value < std::get<0>(current->value)) current = current->left;
                else current = current->right;
            }
    }
}

AVLtree::~AVLtree()
{
    clear();
}

AVLtree::AVLtree(const data_t& d)
{
    root = new node_t(d);
}

AVLtree::AVLtree(const std::initializer_list<data_t>& t)
{
    std::for_each(t.begin(), t.end(), [this](data_t a){ insert(a); });
}

AVLtree::AVLtree(const AVLtree& other)
{
    // ������ �������������� ������� � �������������� �����
    std::stack<node_t *> stack; // ��� ������ ���������
    node_t * current = other.root;

    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {

        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            insert(current->value); // ���������� �������� � ������
            current = current->left;
        }
        current = stack.top()->right;
        stack.pop();
    }
}

AVLtree::AVLtree(AVLtree&& other) noexcept : root(other.root)
{
    other.root = nullptr;
}

AVLtree& AVLtree::operator=(AVLtree&& other) noexcept
{
    clear(); // ������ ������ ������
    while (other.root)
    {
        insert(other.root->value);
        other.erase(KEY(other.root->value));
    }
    return *this;
}

AVLtree& AVLtree::operator=(const AVLtree& other)
{
    if (&other != this)
    {
        clear();
        AVLtree tmp(other); // ��������������� ��� ������ ������������
        swap(tmp); // �������� �������� ����� ��������� � ����������
    }
    return *this;
}

AVLtree::node_t * AVLtree::find(const key_t& key) const
{
    if (!root) throw std::logic_error("������ ������!");
    bool finded = false;
    node_t * start = root;
    while (start && !finded)
    {
        if (KEY(start->value) == key) finded = true;
        else if (KEY(start->value) > key) start = start->left;
        else start = start->right;
    }
    if (finded) return start;
    return root;
}

bool AVLtree::empty() const noexcept
{
    return root == nullptr;
}

int AVLtree::recursive_height(const node_t *tmp) const noexcept
{
    return tmp == nullptr ? 0 : (1 + std::max(recursive_height(tmp->left), recursive_height(tmp->right)));
}

size_t AVLtree::count() const noexcept
{
    return _size;
}



bool AVLtree::insert(const key_t& key, const value_type& v)
{
    check_ID(key); // �������� �� ������������

    node_t * new_node = new node_t(std::make_tuple(key, v));
    new_node->left = new_node->right = nullptr;
    if (root == nullptr) { root = new_node; PreAllTreeBalance(root); _size++; return true; }
    else {
    node_t * current = root; // �������� ��������� �� ������� �����
    bool finded = false;
    while (!finded)
        {
            if (KEY(current->value) > key) { // ���� ������ ���������������� ����
                    if (current->left) current = current->left; // ���� �����, ���� �� � ����
                    else finded = true;  // ���� �� ����� ������ ���� �� �������� nullptr
            }
            else if (KEY(current->value) < key) { // ���� ������ ���������������� ����
                    if (current->right) current = current->right; // ���� �����, ���� �� � ����
                    else finded = true; // ���� �� ����� ������ ���� �� �������� nullptr
            }
        }
    // ��������� � �������
    if (KEY(current->value) > key) current->left = new_node;
    else current->right = new_node;
    new_node = balance(new_node); // �������� ������������ ����� �������
    _size++;
    return true;
    }
    return false;
}

bool AVLtree::insert(const data_t& d)
{
    return insert(KEY(d), std::get<1>(d));
}

bool AVLtree::replace(const key_t& key, const value_type& v)
{
    if (empty()) return false;
    node_t * finded = find(key);
    if (KEY(finded->value) == key)
    {
        finded->value = std::make_tuple(key, v);
        return true;
    }
    return false;
}

// friend ������� ��� �������� ��������� �� ������� � ������� ����, ������������ erase
void reWriteRight(AVLtree::node_t * current, AVLtree::node_t * prev_current)
{
    while (current->right) // ���� ���� ������� ������
    {
        prev_current = current; // �������� �������
        current->value = current->right->value;
        current = current->right;
    }
    prev_current->right = nullptr;
    delete current;
}

void AVLtree::PreAllTreeBalance(AVLtree::node_t * val) noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        val = balance(val);
        PreAllTreeBalance(val->left);
        PreAllTreeBalance(val->right);
        }
}

bool AVLtree::erase(const key_t& key)
{
    if (root == nullptr) return false;
    bool finded = false;

    node_t * current = root, * prev_current;
    while (current && !finded) // ���� �� ������ �������, ��� ������ �� nullptr
    {
        if (KEY(current->value) == key)
            finded = true; // ���� ����� ������� � ������
        else   // ����� ���������� ������� � ��������� � ����������
        {
            prev_current = current;
            // ����� � ����� ��������� �������
            if (KEY(current->value) > key) current = current->left;
            else current = current->right;
        }
    }
    // �� ������ ����� ������� ��� ������, �������� ���������� ��� ��������
    // 1. �������� ����� - � ������ ������ delete + nullptr
    // 2. �������� ���� � ����� �������� ��������� - ��������� �������� ������� � ��������� �����, ������� �������
    // 3. �������� ���� � ����� ��������� ����������
    if (finded)
    {
    if (current->left == current->right)  // ���� current - ���� -> �� � ���� left = right = nullptr, ��� �� root
    {
        // ����� �������, � ������� ����� ������� ���������� ����� ���������
        if (current == root)
        {
            delete root;
            root = nullptr;
        }
        else
        {
            if (prev_current->left == current) prev_current->left = nullptr;
            else prev_current->right = nullptr;
            delete current;// ������ ����������� �������
        }
    }
    else if (current->left && current->right)   // ���� ������� ��� �������� ��������
    {
        if (current != root) reWriteRight(current, prev_current);  // ��������� ������ ����
        else // ���� ��������� ������� ��������, �� ���������� ����� ����� ������ ������� ����� � ��������� �������� � ������ � ������� �������, ��� �������� ���� �����
        {
            current = current->left;
            if (current->right)
            {
                while (current->right) // ���� � ������ �������� ���� ������
                {
                    prev_current = current;
                    current = current->right;
                }
                root->value = current->value;
                prev_current->right = nullptr;
            }
            else  // ���� ������� ���, �� ������� �������� ������ � ������� � ���, ������ ��� � ��������� ����� ������
            {
                root->value = root->left->value;
                root->left = root->left->left;
            }
            delete current;
        }
    }
    else     // ���� ���� ���� ������� �����, ���� ������
    {
        if (current->right)
        {
            if (current != root) reWriteRight(current, prev_current); // ���� ���� ������� ������, ����� ��� �������� �����, �� ��������� �������� ������
            else { root = root->right;
            delete current;}
        }
        else // ����� �������� ������ ���
        {
            if (current == root) { root = root->left; delete current; } // ���� ������� ��� � ��� �������� ������
            else { // ����� ������� �� ���
            while (!current->right && current->left) // ���� �� �������� ������� ������ � ���� ������� �����
            {
                prev_current = current;
                current->value = current->left->value;
                current = current->left;
            }
            if (current->right) reWriteRight(current, prev_current);
            else
            {
                prev_current->left = nullptr;
                delete current;
            }
            }
        }
    }
    PreAllTreeBalance(root);
    _size--;
    return true;
    }
    return false;
}


// �������
void AVLtree::clear()
{
    if(root != nullptr) while (root != nullptr) erase(KEY(root->value));  // ���� ���������� ��������� �� ����� ����� 0, ������� ������
}

int AVLtree::height(const node_t * val) const noexcept
{
    return ((val != nullptr) ? val->_height : 0);
}

int AVLtree::balance_factor(const node_t * val) const
{
    if (val == nullptr) throw std::logic_error("������ ��������� ������ ������ � �������!");
    return height(val->right) - height(val->left); // -1 0 � 1 - ������, -2 � 2 - �����, ������� ������� ������� � ������ � ������ � ������� �������� - ���� ������ ������������
}

void AVLtree::fix_heigth_value(node_t * val) noexcept
{
    int height_left = (val->left != nullptr) ? val->left->_height : 0, height_right = (val->right != nullptr) ? val->right->_height : 0;
    val->_height = (height_left > height_right ?  height_left : height_right ) + 1;
}

// ������ �������
AVLtree::node_t *AVLtree::rightRotate(node_t * y) noexcept
{
    // ����:
    /*  (y)
     (q)
       (q_r)
    */
    /* �����:
       (q)
         (y)
     (q_r)
    */
    node_t * q  = y->left;
	y->left = q->right;
	q->right = y;
	fix_heigth_value(y);
	fix_heigth_value(q);
	return q;
}


// ����� �������
AVLtree::node_t *AVLtree::leftRotate(node_t * x) noexcept
{
    // ����:
    /*  (x)
            (p)
       (q_r)
    */
    /* �����:
          (p)
       (x)
         (q_r)
    */
    node_t* p = x->right;
	x->right = p->left;
	p->left = x;
	fix_heigth_value(x);
	fix_heigth_value(p);
	return p;
}


AVLtree::node_t * AVLtree::balance(node_t * val) noexcept
{
    // ������������ ������ � ��� ������, ���� ������ ������ ������ (� ������ -2 ��� 2)
    if (val != nullptr){
    fix_heigth_value(val);
	if( balance_factor(val) == 2)
	{
		if (balance_factor(val->right) < 0) val->right = rightRotate(val->right);
		return leftRotate(val);
	}
	if(balance_factor(val)== -2)
	{
		if(balance_factor(val->left) > 0) val->left = leftRotate(val->left);
		return rightRotate(val);
	}}

	// ���� ������� ���������� (1 -1 ��� 0), �� ������������ �� �����
	return val;
}

// ����� �������� ��������
void AVLtree::swap(AVLtree& t) noexcept
{
    std::swap(root, t.root);
}

// ����������� ��������, ������ ������ ���������� ������
void AVLtree::concat(AVLtree &t)
{
    if (t.root == root) throw std::logic_error("����������� �������� ���������� � ����� �����!");// ���� �� t != �������� ���������, �� ����������
    while (t.root != nullptr) // ���� � t ���� �������� ����� �� ����� � �������
        {
            insert(t.root->value);
            t.erase(get(0,t.root->value));
        }
    // �� ������ this �� ������ ���������� + �������� t, � t ����
}

// ����� ������������ ��������
AVLtree::data_t find_min(const AVLtree &A)
{
    // ����������� ������ � �������� ������ ������ � ������ ����� ����
    if(A.root == nullptr) throw std::length_error("������ ������, ����������� ������� �����������!"); // ���� ��������� � ������ ���
    AVLtree::node_t * current = A.root;
    while (current->left)  current = current->left; // ��������� �����, ���� ����� ���� �������
    return current->value; // ������ ��������� �������
}
// ����� �������� � ������������ ������
AVLtree::data_t find_max(const AVLtree &A)
{
// ����������� ������ � �������� ������ ������ � ������ ������ ����
    if(A.root == nullptr) throw std::length_error("������ ������, ������������ ������� �����������!"); // ���� ��������� � ������ ���
    AVLtree::node_t * current = A.root;
    while (current->right)  current = current->right; // ��������� �����, ���� ����� ���� �������
    return current->value; // ������ ��������� �������
}

// ����� �������� ��������� ������ ������
void PreOrderRecurse(std::ofstream &write, AVLtree::node_t * val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        write << elementValue(val->value);
        PreOrderRecurse(write, val->left);
        PreOrderRecurse(write, val->right);
        }
}

// ����� �������� ��������� ������ ������
void InOrderRecurse(std::ofstream &write, AVLtree::node_t * val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        InOrderRecurse(write, val->left);
        write << elementValue(val->value);
        InOrderRecurse(write, val->right);
        }
}

// ����� �������� ��������� ������ ������
void PostOrderRecurse(std::ofstream &write, AVLtree::node_t * val)noexcept // ���������� ����������� ����� ������
{
    if (val)
        {
        PostOrderRecurse(write, val->left);
        PostOrderRecurse(write, val->right);
        write << elementValue(val->value);
        }
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WritePreOrderRecurse(const std::string &filename, const AVLtree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open()) PreOrderRecurse(toWrite, A.root);
    toWrite.close();
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WriteInOrderRecurse(const std::string &filename, const AVLtree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open())  InOrderRecurse(toWrite, A.root);
    toWrite.close();
}

// ��� ������� ������ ��������� �������� ������, �.�. ����� private root
void WritePostOrderRecurse(const std::string &filename, const AVLtree &A) noexcept
{
    std::ofstream toWrite(filename);
    if (toWrite.is_open()) PostOrderRecurse(toWrite, A.root);
    toWrite.close();
}


 void PreOrderCycle(const std::string &filename, const AVLtree &A) noexcept
 {
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){ // ���� ������� ������ ������
    std::stack<AVLtree::node_t *> stack; // ��� ������ ���������
    AVLtree::node_t * current = A.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            toWrite << elementValue(current->value);
            current = current->left;
        }
        current = stack.top()->right;
        stack.pop();
    }}
    toWrite.close();
 }


 void InOrderCycle(const std::string &filename, const AVLtree &A)noexcept
 {
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){
    std::stack<AVLtree::node_t *> stack;
    AVLtree::node_t * current = A.root;
    while (current != nullptr || !stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        while (current != nullptr) // ���� ��������� �������
        {
            stack.push(current);
            current = current->left;
        }
        toWrite << elementValue(stack.top()->value);
        current = stack.top()->right;
        stack.pop();
    }}
 }

 void PostOrderCycle(const std::string &filename, const AVLtree &A)noexcept
 {
    std::ofstream toWrite(filename);
    if (toWrite.is_open()){ // ���� �������� ��������
    std::stack<AVLtree::node_t *> stack, stackR; // ��� ������ ���������
    AVLtree::node_t * current = A.root;
    stack.push(current);
    while (!stack.empty()) // ���� �� ������ �� ���������� �������� ��� ���� �� ����
    {
        current = stack.top();
        stack.pop();
        stackR.push(current);
        if (current->left)  stack.push(current->left);
        if (current->right) stack.push(current->right);
    }
    while(!stackR.empty()) // ����, ������� ���������� � ������� ��������
        {
            toWrite << elementValue(stackR.top()->value); // ������ ������ � ����
            stackR.pop();
        }
      }
}


