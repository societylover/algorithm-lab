#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include "PriceList.h"

int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "Russian"); 

    int result = Catch::Session().run( argc, argv );	// --   Catch --
    system("pause");			// --    --
    return result;
}
